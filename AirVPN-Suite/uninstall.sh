#!/bin/sh

#
# AirVPN suite uninstallation script
#
# Version 1.1 - 22 January 2021
#
# Written by ProMIND
#

get_bin_path()
{
    binpath="/bin /sbin /usr/bin /usr/sbin /usr/local/bin /usr/local/sbin"

    local cmdpath=$2
    eval $cmdpath=$1

    for cpath in $binpath; do
        tbin="${cpath}/${1}"

        if [ -f ${tbin} ]; then
            eval $cmdpath=$tbin
        fi
    done
}

get_dbus_path()
{
    tpath="/etc/dbus-1/system.d /usr/local/etc/dbus-1/system.d /usr/share/dbus-1/system.d /usr/local/share/dbus-1/system.d"

    for dpath in $tpath; do
        tconf="${dpath}/org.airvpn.server.conf"

        if [ -f ${tconf} ] && [ "${dbuspath}" = "" ]; then
            dbuspath=$dpath
        fi
    done
}

get_bin_path userdel userdel_bin
get_bin_path groupdel groupdel_bin

dbuspath=""

get_dbus_path

airvpn_user=airvpn
airvpn_group=airvpn

echo
echo "AirVPN suite uninstallation script"
echo

if [ `id -u` != "0" ]; then
    echo "You need root privilegs to uninstall AirVPN suite"
    echo

    exit 1
fi

read -p "Do you want to completely uninstall AirVPN Suite? [y/n] " yn

if [ "${yn}" = "n" ]; then
    echo
    echo "Uninstallation aborted"

    exit 1
fi

INIT_TYPE="unknown"

echo

if [ `cat < /proc/1/comm` = "systemd" ]; then
    INIT_TYPE=systemd
    echo "System is using systemd"
elif [ `cat < /proc/1/comm` = "init" ]; then
    INIT_TYPE=init
    echo "System is using SysV-style init"
else
    echo "This system is not running either systemd or SysV-style init"
    echo "Installation aborted"
    echo

    exit 1
fi

if [ "${dbuspath}" = "" ]; then
    echo
    echo "ERROR: D-Bus is not properly configured or not available"
    echo "Uninstallation aborted"
    echo

    exit 1
else
    echo
    echo "D-Bus directory is ${dbuspath}"
    echo
fi

if [ "${INIT_TYPE}" = "systemd" ]; then
    # systemd

    if [ -f "/etc/systemd/system/bluetit.service" ]; then
        echo "Stopping bluetit.service"
        
        systemctl stop bluetit.service
        
        echo "Disabling bluetit.service"
        
        systemctl disable bluetit.service

        echo "Removing systemd bluetit.service unit"

        rm /etc/systemd/system/bluetit.service

        systemctl daemon-reload
    fi
else
    # SysV-style init

    if [ -f "/etc/init.d/bluetit" ]; then
        echo "Stopping bluetit daemon"
        
        /etc/init.d/bluetit stop

        which chkconfig  > /dev/null 2>&1

        if [ $? -eq 0 ]; then
            chkconfig --del bluetit
        else
            rm /etc/rc0.d/K60bluetit
            rm /etc/rc1.d/K60bluetit
            rm /etc/rc2.d/K60bluetit
            rm /etc/rc3.d/S90bluetit
            rm /etc/rc4.d/K60bluetit
            rm /etc/rc5.d/S90bluetit
            rm /etc/rc6.d/K60bluetit
        fi

        echo "Removing SysV-style bluetit init script"

        rm /etc/init.d/bluetit
    fi
fi

echo "Removing D-Bus configuration files"

if [ -f "${dbuspath}/org.airvpn.client.conf" ]; then
    rm $dbuspath/org.airvpn.client.conf
fi

if [ -f "${dbuspath}/org.airvpn.server.conf" ]; then
    rm $dbuspath/org.airvpn.server.conf
fi

which hummingbird  > /dev/null 2>&1

if [ $? -eq 0 ]; then
    echo "Removing hummingbird"
    rm `which hummingbird`
fi

which goldcrest  > /dev/null 2>&1

if [ $? -eq 0 ]; then
    echo "Removing goldcrest"
    rm `which goldcrest`
fi

if [ -f "/sbin/bluetit" ]; then
    echo "Removing bluetit"
    rm /sbin/bluetit
fi

if [ -d "/etc/airvpn" ]; then
    echo "Removing bluetit configuration files"
    rm /etc/airvpn/*

    echo "Removing /etc/airvpn directory"
    rmdir /etc/airvpn
fi

id $airvpn_user > /dev/null 2>&1

if [ $? -eq 0 ]; then
    echo

    read -p "Do you want to delete ${airvpn_user} user and its home directory? [y/n] " yn

    if [ "${yn}" = "y" ]; then
        $userdel_bin -r $airvpn_user
    fi
fi

grep -q $airvpn_group /etc/group

if [ $? -eq 0 ]; then
    echo

    read -p "Do you want to delete ${airvpn_group} group? [y/n] " yn

    if [ "${yn}" = "y" ]; then
        $groupdel_bin $airvpn_group
    fi
fi

echo

if [ "${INIT_TYPE}" = "systemd" ]; then
    systemctl reload dbus.service
else
    /etc/init.d/dbus reload
fi

echo "Done."
