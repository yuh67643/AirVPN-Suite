Changelog for Bluetit

Version 1.2.1 - 9 December 2022

- [ProMIND] production release


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.1 RC 1 - 30 November 2022

- [ProMIND] updated all dependencies and libraries
- [ProMIND] package is now realeased both for OpenSSL 3.0 and OpenSSL 1.1.x (legacy)
- [ProMIND] airvpn_server_save() now correctly returns a proper error in case an invalid user key has been provided
- [ProMIND] added RC option forbidquickhomecountry
- [ProMIND] DBUS method BT_METHOD_NETWORK_LOCK_STATUS now returns the description of firewall backend in use


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 - 22 March 2022

- [ProMIND] production release


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 RC 3 - 17 March 2022

- [ProMIND] do not check for supported ciphers in OpenVPN config file in case eval.cipher is empty
- [ProMIND] establish_openvpn_connection() returns false in case of client's event error or fatal error
- [ProMIND] connection and connection stats threads are now stopped by dedicated functions stop_connection_thread() and void stop_connection_stats_thread() respectively
- [ProMIND] improved error management at connection time


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 RC 2 - 8 March 2022

- [ProMIND] Added list_data_ciphers dbus method
- [ProMIND] Added list_pushed_dns dbus method
- [ProMIND] Check and validate requested data cipher according to VpnClient's supported ciphers
- [ProMIND] Shows server information summary at the end of connection process via VpnClient connected event
- [ProMIND] Normalized (extended) bool values for options allowuaf, compress and network-lock


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 RC 1 - 15 February 2022

- [ProMIND] Same as Beta 1


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.2.0 Beta 1 - 7 February 2022

- [ProMIND] White and black lists are now properly checked when connecting to an AirVPN server or country
- [ProMIND] In case there are white lists defined, quick connection will ignore the connection scheme priority
- [ProMIND] Added "africa" and "oceania" to continent/country connection process
- [ProMIND] Added SSL library version to startup log
- [ProMIND] Removed ipv6 option and replaced with allowuaf option (Allow Unused Address Families) in order to comply to the new OpenVPN3 specifications
- [ProMIND] Added DBus method ssl_library_version
- [ProMIND] btcommon.hpp: added normalized client options and descriptions
- [ProMIND] add_airvpn_bootstrap_to_network_lock(): added support for AirVPN IPv6 bootstrap servers


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 - 4 June 2021

- [ProMIND] Client option "network-lock" is now forbidden in case persistent network lock is enabled
- [ProMIND] Avoid network lock initialization in case persistent network lock is enabled and client is requiring a OpenVPN connection from profile
- [ProMIND] --air-list option now accepts "all" for sub options --air-server and --air-country
- [ProMIND] AirVPN Manifest update suspended in case Bluetit is in a dirty status
- [ProMIND] Changed systemd unit in order to prevent the obnoxious SIGKILL signal inappropriately sent before stop timeout completion and for no logical or practical reason when Bluetit is properly and neatly terminating in response to a legal and expected SIGTERM


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 RC 4 - 14 May 2021

- [ProMIND] Added directives airipv6 and air6to4 in bluetit.rc
- [ProMIND] In case it is requested a network recovery, VpnClient object is now initialized with NetFilter::Mode::OFF
- [ProMIND] In case the requested network lock method is not available, connection is not started
- [ProMIND] In case system location cannot be determined through ipleak.net, country is now properly set to empty, latitude and longitude to 0.
- [ProMIND] Persistent network lock is enabled only in case Bluetit status is clean
- [ProMIND] AirVPN boot connection is started only in case Bluetit status is clean
- [ProMIND] DNS backup files are now properly evaluated when determining dirty status
- [ProMIND] Added D-Bus commands "reconnect_connection" and "session_reconnect"


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 RC 3 - 16 April 2021

- [ProMIND] Release Candidate 3


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 RC 2 - 13 April 2021

- [ProMIND] Release Candidate 2


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 RC 1 - 7 April 2021

- [ProMIND] Release Candidate 1


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 Beta 2 - 2 April 2021

- [ProMIND] Gateway and gateway interface check at startup. Bluetit won't proceed until both gateway and gateway interface are properly set up by the system
- [ProMIND] Increased volume and rate data sizes for 32 bit architectures
- [ProMIND] Added aircpher directive to bluetit.rc
- [ProMIND] Added maxconnretries directive to bluetit.rc


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.1.0 Beta 1 - 11 March 2021

- [ProMIND] Updated base classes
- [ProMIND] connection_stats_updater(): now uses server.getEffectiveBandWidth() for AIRVPN_SERVER_BANDWIDTH
- [ProMIND] added bool shutdownInProgress to control bluetit exit procedure and avoid signal flooding
- [ProMIND] system location is detected at boot time and eventually propagated to all AirVPN users
- [ProMIND] Network lock and filter is now enabled and activated before AirVPN login procedure
- [ProMIND] Added dbus methods "enable_network_lock", "disable_network_lock" and "network_lock_status"
- [ProMIND] Renamed bluetit.rc directive "airconnectonboot" to "airconnectatboot"
- [ProMIND] Added bluetit.rc directive "networklockpersist"


*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


Version 1.0.0 - 7 January 2021

- [ProMIND] Production release
