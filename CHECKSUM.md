# AirVPN Suite for Linux

#### AirVPN's free and open source OpenVPN 3 suite based on AirVPN's OpenVPN 3 library fork

### Version 1.2.1 - Release date 9 December 2022


## Note on Checksum Files

AirVPN Suite is an open source project and, as such, its source code can be
downloaded, forked and modified by anyone who wants to create a derivative
project or build it on his or her computer. This also means the source code can
be tampered or modified in a malicious way, therefore creating a binary version
of the suite which may act harmfully, destroy or steal your data, redirecting
your network traffic and data while pretending to be the "real" AirVPN Suite
client genuinely developed and supported by AirVPN.

For this reason, we cannot guarantee forked, modified and custom compiled
versions of AirVPN Suite to be compliant to our specifications, development and
coding guidelines and style, including our security standards. These projects,
of course, may also be better and more efficient than our release, however we
cannot guarantee or provide help for the job of others.

You are therefore strongly advised to check and verify the checksum codes found
in the `.sha512` files to exactly correspond to the ones below, that is, the
checksum we have computed from the sources and distribution files directly
compiled and built by AirVPN. This will make you sure about the origin and
authenticity of the suite. Please note the files contained in the distribution
tarballs are created from the very source code available in the master branch of
the [official AirVPN Suite's repository](https://gitlab.com/AirVPN/AirVPN-Suite).


### Checksum codes for Version 1.2.1

The checksum codes contained in files `AirVPN-Suite-<arch>-1.2.1.<archive>.sha512`
must correspond to the codes below in order to prove they are genuinely created
and distributed by AirVPN.


## Linux x86_64

`AirVPN-Suite-x86_64-1.2.1.tar.gz`:
be3f89fe87aabb4bdf77dd1744fc865c7d2faedb0bfbcaf1d4af9215159eeb62b136c4bfeb74b1a940febe2dfbed985df508ee580babc090e671a58a48f84e93


## Linux x86_64 legacy

`AirVPN-Suite-x86_64-legacy-1.2.1.tar.gz`:
b31265b687e6be25b2a9cd6f4ac9cb7a73b5e2184aa17af7e58001d4919d4447ad7f8b242923bd81f3b9fa8018ea79f5147df37ed070def1447579dcc20aca4d


## Linux ARM32 legacy

`AirVPN-Suite-armv7l-legacy-1.2.1.tar.gz`:
4aa7f7fab89cc02a8e7f1aa90faebbbf916d094c6ff54715fecb42ce73411c7d6a71ff91ef764b95faa5cff5fa599f7772b2724f0b32a1a9e35746b5245f028a


## Linux ARM64

`AirVPN-Suite-aarch64-1.2.1.tar.gz`:
c0c496f0391d1102a947bfafb73b93ee136cb52fef08b1f1321277ce18399bb37882efe556f272e8071673cf1b4f7243badf94755914d7767b3a032573c0b99e


## Linux ARM64 legacy

`AirVPN-Suite-aarch64-legacy-1.2.1.tar.gz`:
4147acc92e4efef3f96b2ea4f9e4a68c7890a0f4d7ecb19225920e5549374d228496687f267e6eb2b2f9ef556a08a4c45fd551461e37bd578752daab0ea93b94
