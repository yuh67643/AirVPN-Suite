/*
 * bluetit.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <sstream>
#include <vector>
#include <thread>
#include <chrono>
#include <future>
#include <regex>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>

#define OPENVPN_USE_LOG_BASE_SIMPLE

#include "include/vpnclient.hpp"

#include "include/bluetit.hpp"

#include "include/localnetwork.hpp"
#include "include/netfilter.hpp"
#include "include/logger.hpp"
#include "include/semaphore.hpp"
#include "include/airvpnmanifest.hpp"
#include "include/airvpnserver.hpp"
#include "include/airvpnserverprovider.hpp"
#include "include/airvpnuser.hpp"

std::thread *airVpnManifestUpdaterThread = nullptr;
std::thread *connectionThread = nullptr;
std::thread *connectionStatsThread = nullptr;

Semaphore *manifestSemaphore = nullptr;

std::promise<void> manifestUpdaterSignalExit;
std::future<void> manifestUpdaterFutureObject;

std::promise<void> *connectionSignalExit = nullptr;
std::future<void> connectionFutureObject;

std::promise<void> *connectionStatsSignalExit = nullptr;
std::future<void> connectionStatsFutureObject;

ClientAPI::Status openVpnConnectionStatus;

std::map<std::string, std::string> connectionStats;
std::map<std::string, std::string> bluetitLocation;

VpnClient *vpnClient = nullptr;
ClientAPI::Config *clientConfig = nullptr;
Logger *logger = nullptr;
int status = BLUETIT_STATUS_UNKNOWN;
bool shutdownInProgress = false;

bool airVpnEnabled = false;
bool airVpnUserValid = false;
bool airVpnInfo = false;
bool airVpnList = false;
bool airVpnKeyList = false;
bool airVpnSave = false;
bool airVpnAutoQuickMode = true;
bool forbidQuickHomeCountry = true;
std::string airVpnSaveFileName = "", customAirVpnKey = "";

int airVpnManifestUpdateInterval = DEFAULT_MANIFEST_UPDATE_INTERVAL;

std::vector<ConnectionScheme> connectionSchemeList;

std::string airVpnServerPattern, airVpnCountryPattern, airVpnUsername, airVpnPassword;
std::string bluetitCountry, airVpnTlsMode, airVpnKeyName;
std::string airVpnClientWhiteServerList, airVpnClientBlackServerList;
std::string airVpnClientWhiteCountryList, airVpnClientBlackCountryList;
std::string airVpnConnectAtBoot, airVpnBootConnectionServer, airVpnBootConnectionCountry;
std::string airVpnBootConnectionUserName, airVpnBootConnectionPassword, airVpnBootConnectionKey;
std::string airVpnBootConnectionPort, airVpnBootConnectionProto, airVpnBootConnectionCipher;
bool airVpnBootConnectionIPv6 = false, airVpnBootConnection6to4 = false, airVpnOverrideQuickConnection = false;
std::string connectionInformation;
bool airVpnIPv6 = DEFAULT_IPV6_MODE, airVpn6to4 = DEFAULT_IPV6_MODE, iPv6Enabled = false;

int maxConnectionRetries = DEFAULT_MAX_CONNECTION_RETRIES;

DBusConnector *dbusConnector = nullptr;
DBusResponse dbusResponse;
std::vector<std::string> dbusItems;
std::ostringstream osstring;

AirVPNManifest *airVpnManifest = nullptr;
AirVPNUser *airVpnUser = nullptr;
LocalNetwork *localNetwork = nullptr;
NetFilter *netFilter = nullptr;
RCParser *rcParser = nullptr;
OptionParser *optionParser = nullptr;

#if defined(OPENVPN_PLATFORM_LINUX)

DNSManager *dnsManager = nullptr;

#endif

// VPN Client settings

std::string vpn_response;
std::string vpn_dynamicChallengeCookie;
std::string vpn_proto;
std::string vpn_allowUnusedAddrFamilies;
std::string vpn_server;
std::string vpn_port;
std::string vpn_cipher_alg = "";
std::string vpn_gui_version = BLUETIT_FULL_NAME;
int vpn_timeout = 0;
std::string vpn_compress;
std::string vpn_privateKeyPassword;
std::string vpn_tlsVersionMinOverride;
std::string vpn_tlsCertProfileOverride;
std::string vpn_proxyHost;
std::string vpn_proxyPort;
std::string vpn_proxyUsername;
std::string vpn_proxyPassword;
std::string vpn_peer_info;
std::string vpn_gremlin;
unsigned int vpn_tcp_queue_limit = TCP_QUEUE_LIMIT_DEFAULT;
bool vpn_ncp_disable = false;
NetFilter::Mode vpn_network_lock_mode = NetFilter::Mode::AUTO;
NetFilter::Mode persistent_network_lock_mode = NetFilter::Mode::OFF;
NetFilter::Mode policy_network_lock_mode = NetFilter::Mode::AUTO;
bool vpn_ignore_dns_push = false;
bool vpn_self_test = false;
bool vpn_cachePassword = false;
bool vpn_disableClientCert = false;
bool vpn_proxyAllowCleartextAuth = false;
int vpn_defaultKeyDirection = -1;
int vpn_sslDebugLevel = 0;
bool vpn_autologinSessions = false;
bool vpn_retryOnAuthFailed = false;
bool vpn_tunPersist = true;
bool vpn_altProxy = false;
bool vpn_dco = false;
std::string vpn_epki_cert_fn;
std::string vpn_epki_ca_fn;
std::string vpn_epki_key_fn;
std::string remote_override_cmd;
std::string vpn_profile = "";

void signal_handler(int signum)
{
    std::string signalDescription;

    if(shutdownInProgress == true)
        return;

    switch(signum)
    {
        case SIGTERM:
        case SIGINT:
        case SIGPIPE:
        case SIGHUP:
        {
            shutdownInProgress = true;

            if(signum == SIGTERM)
                signalDescription = "SIGTERM";
            else if(signum == SIGINT)
                signalDescription = "SIGINT";
            else if(signum == SIGPIPE)
                signalDescription = "SIGPIPE";
            else
                signalDescription = "SIGHUP";

            osstring.str("");

            osstring << "Received " << signalDescription << " signal. Terminating " << BLUETIT_SHORT_NAME << ".";

            logger->systemLog(osstring.str());

            cleanup_and_exit(EXIT_SUCCESS);
        }
        break;

        case SIGUSR1:
        {
            osstring.str("");

            osstring << "Received SIGUSR1 signal. ";

            if(vpn_tunPersist == false)
            {
                osstring << "TUN persistence is not enabled. Pause/resume is ignored.";
            }
            else if(status == BLUETIT_STATUS_CONNECTED)
            {
                osstring << "Pausing VPN connection.";

                pause_openvpn_connection();
            }
            else if(status == BLUETIT_STATUS_PAUSED)
            {
                osstring << "Resuming VPN connection.";

                resume_openvpn_connection();
            }
            else
                osstring << "VPN connection is not paused or connected. Signal ignored.";

            logger->systemLog(osstring.str());
        }
        break;

        case SIGUSR2:
        {
            osstring.str("");
            
            osstring << "Received SIGUSR2 signal. ";

            if(vpn_tunPersist == false)
            {
                osstring << "TUN persistence is not enabled. Reconnection is ignored.";
            }
            else
            {
                osstring << "Reconnecting VPN.";

                reconnect_openvpn();
            }

            logger->systemLog(osstring.str());
        }
        break;

        default:
        {
            osstring.str("");

            osstring << "Received unhandled signal " << signum;

            logger->systemLog(osstring.str());
        }
        break;
    }
}

void create_daemon()
{
    pid_t pid;
    int fd, fp_lock;
    char str[10];

    pid = fork();

    if(pid < 0)
    {
        logger->systemLog("Cannot start daemon");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(pid > 0)
        exit(EXIT_SUCCESS);

    if(setsid() < 0)
    {
        logger->systemLog("Cannot start a new session for daemon. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    // signal(SIGCHLD, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);

    signal(SIGHUP, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGPIPE, signal_handler);
    signal(SIGUSR1, signal_handler);
    signal(SIGUSR2, signal_handler);

    pid = fork();

    if(pid < 0)
    {
        logger->systemLog("Cannot start daemon");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(pid > 0)
        exit(EXIT_SUCCESS);

    umask(0);

    if(chdir(BLUETIT_RESOURCE_DIRECTORY) != 0)
    {
        logger->systemLog("Cannot start daemon (Cannot change to resource directory)");

        cleanup_and_exit(EXIT_FAILURE);
    }

    for(fd = sysconf(_SC_OPEN_MAX); fd >= 0; fd--)
        close(fd);

    fp_lock = open(BLUETIT_LOCK_FILE, O_RDWR|O_CREAT, 0640);

    if(fp_lock < 0)
    {
        logger->systemLog("Cannot create lock file");

        cleanup_and_exit(EXIT_FAILURE);
    }

    sprintf(str, "%d\n", getpid());

    if(write(fp_lock, str, strlen(str)) <= 0)
    {
        close(fp_lock);

        logger->systemLog("Cannot start daemon (Cannot write pid to lock file)");

        cleanup_and_exit(EXIT_FAILURE);
    }

    close(fp_lock);

    osstring.str("");

    osstring << BLUETIT_SHORT_NAME << " daemon started with PID " << str;

    logger->systemLog(osstring.str());
}

bool check_if_root()
{
    uid_t uid, euid;
    bool retval = false;

    uid=getuid();
    euid=geteuid();

    if(uid != 0 || uid != euid)
        retval = false;
    else
        retval = true;

    return retval;
}

void global_log(const std::string &s)
{
    if(logger != nullptr)
        logger->queueLog(s);
}

void global_syslog(const std::string &s)
{
    if(logger != nullptr)
        logger->systemLog(s);
}

void reset_settings()
{
    vpn_response = "";
    vpn_dynamicChallengeCookie = "";
    vpn_proto = DEFAULT_PROTO;
    vpn_allowUnusedAddrFamilies = "";
    vpn_server = "";
    vpn_port = DEFAULT_PORT;
    vpn_cipher_alg = "";
    vpn_gui_version = BLUETIT_FULL_NAME;
    vpn_timeout = 0;
    vpn_compress = "";
    vpn_privateKeyPassword = "";
    vpn_tlsVersionMinOverride = "";
    vpn_tlsCertProfileOverride = "";
    vpn_proxyHost = "";
    vpn_proxyPort = "";
    vpn_proxyUsername = "";
    vpn_proxyPassword = "";
    vpn_peer_info = "";
    vpn_gremlin = "";
    vpn_tcp_queue_limit = TCP_QUEUE_LIMIT_DEFAULT;
    vpn_ncp_disable = false;
    vpn_network_lock_mode = policy_network_lock_mode;
    vpn_ignore_dns_push = false;
    vpn_self_test = false;
    vpn_cachePassword = false;
    vpn_disableClientCert = false;
    vpn_proxyAllowCleartextAuth = false;
    vpn_defaultKeyDirection = -1;
    vpn_sslDebugLevel = 0;
    vpn_autologinSessions = false;
    vpn_retryOnAuthFailed = false;
    vpn_tunPersist = true;
    vpn_altProxy = false;
    vpn_dco = false;
    vpn_epki_cert_fn = "";
    vpn_epki_ca_fn = "";
    vpn_epki_key_fn = "";
    remote_override_cmd = "";
    vpn_profile = "";

    airVpnInfo = false;
    airVpnList = false;
    airVpnKeyList = false;
    airVpnSave = false;
    airVpnAutoQuickMode = true;

    airVpnServerPattern = "";
    airVpnCountryPattern = "";
    airVpnUsername = "";
    airVpnPassword = "";
    airVpnTlsMode = DEFAULT_TLS_MODE;
    airVpnIPv6 = DEFAULT_IPV6_MODE;
    airVpn6to4 = DEFAULT_6TO4_MODE;
    airVpnKeyName = "";
    customAirVpnKey = "";

    airVpnClientWhiteServerList = "";
    airVpnClientBlackServerList = "";
    airVpnClientWhiteCountryList = "";
    airVpnClientBlackCountryList = "";

    connectionInformation = "";

    AirVPNTools::setBootServerIPv6Mode(airVpnIPv6);

    if(clientConfig != nullptr)
        set_openvpn_client_config();
}

bool enable_network_lock(int mode)
{
    bool retval = false;

    if(netFilter == nullptr)
        return false;

    if(netFilter->isNetworkLockAvailable() == false)
    {
        logger->queueLog("ERROR: Network filter and lock are not available");

        return false;
    }

    if(netFilter->isNetworkLockEnabled() == true)
    {
        logger->queueLog("ERROR: Network filter and lock are already enabled");

        return false;
    }

    if(status == BLUETIT_STATUS_CONNECTED)
    {
        logger->queueLog("ERROR: Network filter and lock cannot be enabled. Bluetit is connected to VPN.");

        return false;
    }

    if(netFilter->init())
    {
        logger->queueLog("Network filter successfully initialized");

        netFilter->setup(localNetwork->getLoopbackInterface());

        if(netFilter->commitRules() == true)
        {
            osstring.str("");

            if(mode == NETLOCKMODE_PERSISTENT)
                osstring << "Persistent";
            else
                osstring << "Session";
            
            osstring << " network filter and lock successfully enabled";
                
            logger->queueLog(osstring.str());
            
            retval = true;
        }
        else
        {
            logger->queueLog("ERROR: Cannot enable network filter and lock");
            
            retval = false;
        }
    }
    else
    {
        logger->queueLog("ERROR: Cannot initialize network filter");
        
        retval = false;
    }

    return retval;
}

bool disable_network_lock()
{
    bool retval = false;

    if(netFilter == nullptr)
        return false;

    if(netFilter->isNetworkLockAvailable() == false)
    {
        logger->queueLog("ERROR: Network filter and lock are not available");

        return false;
    }

    if(netFilter->isNetworkLockEnabled() == false)
    {
        logger->queueLog("ERROR: Network filter and lock is not enabled");

        return false;
    }

    if(status == BLUETIT_STATUS_CONNECTED)
    {
        logger->queueLog("ERROR: Network filter and lock cannot be disabled. Bluetit is connected to VPN.");

        return false;
    }

    if(netFilter->restore())
    {
        logger->queueLog("Session network filter and lock are now disabled");
        
        retval = true;
    }
    else
    {
        logger->queueLog("ERROR: Cannot restore network filter");

        retval = false;
    }

    if(persistent_network_lock_mode != NetFilter::Mode::OFF)
    {
        logger->systemLog("Enabling persistent network filter and lock");
        
        manifestSemaphore->forbid("Persistent network filter and lock enabled");

        enable_network_lock(NETLOCKMODE_PERSISTENT);

        logger->flushLog();
    }

    return retval;
}

bool add_airvpn_bootstrap_to_network_lock()
{
    std::vector<std::string> bootstrapServer;
    NetFilter::IP ipMode;
    bool retval = true;

    if(airVpnEnabled == false)
    {
        logger->queueLog("ERROR: AirVPN services and connection are disabled.");
        
        return false;
    }

    bootstrapServer = airVpnManifest->getBootStrapServerUrlList();

    if(bootstrapServer.size() > 0)
    {
        for(std::string server : bootstrapServer)
        {
            server = std::regex_replace(server, std::regex("http:\\/\\/"), "");
            server = std::regex_replace(server, std::regex("https:\\/\\/"), "");
            server = std::regex_replace(server, std::regex("\\["), "");
            server = std::regex_replace(server, std::regex("\\]"), "");

            if(server.find(":") != std::string::npos)
            {
                server += "/128";
                ipMode = NetFilter::IP::v6;
            }
            else
            {
                server += "/32";
                ipMode = NetFilter::IP::v4;
            }

            if(ipMode == NetFilter::IP::v4 || (ipMode == NetFilter::IP::v6 && localNetwork->isIPv6Enabled() == true))
                retval &= netFilter->commitAllowRule(ipMode, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, server, 0);
        }
        
        if(retval == true)
            logger->queueLog("AirVPN bootstrap servers are now allowed to pass through the network filter");
        else
            logger->queueLog("ERROR: Cannot allow AirVPN bootstrap servers to pass through network filter");
    }
    else
    {
        logger->queueLog("ERROR: There are no AirVPN bootstrap servers defined in manifest");
        
        retval = false;
    }
    
    return retval;
}

bool remove_airvpn_bootstrap_from_network_lock()
{
    std::vector<std::string> bootstrapServer;
    bool retval = false;

    if(airVpnEnabled == false)
    {
        logger->queueLog("ERROR: AirVPN services and connection are disabled.");
        
        return false;
    }

    bootstrapServer = airVpnManifest->getBootStrapServerUrlList();

    if(bootstrapServer.size() > 0)
    {
        for(std::string server : bootstrapServer)
        {
            server += "/32";

            netFilter->commitRemoveAllowRule(NetFilter::IP::v4, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, server, 0);
        }
        
        logger->queueLog("AirVPN bootstrap servers removed from the network filter");

        retval = true;
    }
    else
    {
        logger->queueLog("ERROR: There are no AirVPN bootstrap servers defined in manifest");
        
        retval = false;
    }
    
    return retval;
}

std::string check_rc_value(const std::string &description, const std::string &directive, const std::string &value)
{
    std::string result = BT_OK;
    std::ostringstream oserror;

    if(rcParser->getDirective(directive) != nullptr)
    {
        if(rcParser->isValueAllowed(directive, value) == false)
        {
            oserror.str("");

            oserror << "ERROR: " << description << " '" << value << "' is not allowed by " << BLUETIT_SHORT_NAME << " policy.";

            logger->systemLog(oserror.str());

            result = oserror.str();
        }
    }

    return result;
}

std::string denied_rc_value(const std::string &description, const std::string &directive, const std::string &value)
{
    std::string result = BT_OK;
    std::ostringstream oserror;

    if(rcParser->getDirective(directive) != nullptr)
    {
        if(rcParser->isValueAllowed(directive, value) == true)
        {
            oserror.str("");

            oserror << "ERROR: " << description << " '" << value << "' is not allowed by " << BLUETIT_SHORT_NAME << " policy.";

            logger->systemLog(oserror.str());

            result = oserror.str();
        }
    }

    return result;
}

std::string set_openvpn_client_config()
{
    if(clientConfig == nullptr)
    {
        logger->systemLog("set_openvpn_client_config(): ClientAPI::Config is null. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    clientConfig->guiVersion = vpn_gui_version;

    clientConfig->serverOverride = vpn_server;
    clientConfig->portOverride = vpn_port;
    clientConfig->protoOverride = vpn_proto;
    clientConfig->cipherOverrideAlgorithm = vpn_cipher_alg;
    clientConfig->connTimeout = vpn_timeout;
    clientConfig->compressionMode = vpn_compress;
    clientConfig->allowUnusedAddrFamilies = vpn_allowUnusedAddrFamilies;
    clientConfig->tcpQueueLimit = vpn_tcp_queue_limit;
    clientConfig->disableNCP = vpn_ncp_disable;
    clientConfig->privateKeyPassword = vpn_privateKeyPassword;
    clientConfig->tlsVersionMinOverride = vpn_tlsVersionMinOverride;
    clientConfig->tlsCertProfileOverride = vpn_tlsCertProfileOverride;
    clientConfig->disableClientCert = vpn_disableClientCert;
    clientConfig->proxyHost = vpn_proxyHost;
    clientConfig->proxyPort = vpn_proxyPort;
    clientConfig->proxyUsername = vpn_proxyUsername;
    clientConfig->proxyPassword = vpn_proxyPassword;
    clientConfig->proxyAllowCleartextAuth = vpn_proxyAllowCleartextAuth;
    clientConfig->altProxy = vpn_altProxy;
    clientConfig->dco = vpn_dco;
    clientConfig->defaultKeyDirection = vpn_defaultKeyDirection;
    clientConfig->sslDebugLevel = vpn_sslDebugLevel;
    clientConfig->googleDnsFallback = false;
    clientConfig->autologinSessions = vpn_autologinSessions;
    clientConfig->retryOnAuthFailed = vpn_retryOnAuthFailed;
    clientConfig->tunPersist = vpn_tunPersist;
    clientConfig->gremlinConfig = vpn_gremlin;
    clientConfig->info = true;
    clientConfig->wintun = false;

    return BT_OK;
}

std::string set_openvpn_profile(const std::string &profile)
{
    std::string result = BT_ERROR;

    if(clientConfig == nullptr)
    {
        logger->systemLog("set_openvpn_profile(): ClientAPI::Config is null. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(profile == "")
    {
        result = BT_ERROR;
        result += ": profile is empty";
    }
    else
    {
        vpn_profile = profile;

        clientConfig->content = profile;

        result = BT_OK;
    }

    return result;
}

void start_boot_airvpn_connection()
{
    std::ostringstream oserror;

    if(airVpnConnectAtBoot == "off")
        return;

    if(status != BLUETIT_STATUS_READY)
    {
        oserror.str("");
        
        oserror << "ERROR: Cannot start AirVPN boot connection. " << bluetit_status_description();

        logger->systemLog(oserror.str());

        return;
    }

    manifestSemaphore->forbid("AirVPN boot connection initialization in progress");

    reset_settings();

    if(airVpnOverrideQuickConnection == true)
        airVpnAutoQuickMode = false;

    if(airVpnBootConnectionUserName.empty())
    {
        oserror.str("");
        
        oserror << "ERROR: Cannot start AirVPN boot connection. AirVPN username not provided in " << BLUETIT_RC_FILE;

        logger->systemLog(oserror.str());

        return;
    }

    if(airVpnBootConnectionPassword.empty())
    {
        oserror.str("");
        
        oserror << "ERROR: Cannot start AirVPN boot connection. AirVPN password not provided in " << BLUETIT_RC_FILE;

        logger->systemLog(oserror.str());

        return;
    }

    airVpnUsername = airVpnBootConnectionUserName;
    airVpnPassword = airVpnBootConnectionPassword;
    airVpnKeyName = airVpnBootConnectionKey;
    vpn_proto = airVpnBootConnectionProto;
    vpn_port = airVpnBootConnectionPort;
    airVpnIPv6 = airVpnBootConnectionIPv6;
    airVpn6to4 = airVpnBootConnection6to4;

    AirVPNTools::setBootServerIPv6Mode(airVpnIPv6);

    if(airVpnBootConnectionCipher != "")
        vpn_cipher_alg = airVpnBootConnectionCipher;

    set_openvpn_client_config();

    if(airVpnConnectAtBoot == "quick")
    {
        airVpnServerPattern = "";
        airVpnCountryPattern = "";
    }
    else if(airVpnConnectAtBoot == "server")
    {
        if(airVpnBootConnectionServer.empty())
        {
            oserror.str("");
            
            oserror << "ERROR: Cannot start AirVPN boot connection. AirVPN server not provided in " << BLUETIT_RC_FILE;

            logger->systemLog(oserror.str());

            return;
        }

        airVpnServerPattern = airVpnBootConnectionServer;
        airVpnCountryPattern = "";
    }
    else if(airVpnConnectAtBoot == "country")
    {
        if(airVpnBootConnectionCountry.empty())
        {
            oserror.str("");
            
            oserror << "ERROR: Cannot start AirVPN boot connection. AirVPN country not provided in " << BLUETIT_RC_FILE;

            logger->systemLog(oserror.str());

            return;
        }

        airVpnServerPattern = "";
        airVpnCountryPattern = airVpnBootConnectionCountry;
    }
    else
    {
        oserror.str("");
        
        oserror << "ERROR: Cannot start AirVPN boot connection. Unknown mode \"" << airVpnConnectAtBoot << "\"";

        logger->systemLog(oserror.str());

        return;
    }

    logger->systemLog("Starting AirVPN boot connection");

    if(airVpnBootConnectionIPv6 == true)
        logger->systemLog("IPv6 is enabled");

    if(airVpnBootConnection6to4 == true)
        logger->systemLog("IPv6 over IPv4 is enabled");

    connectionThread = new std::thread(start_airvpn_connection);

    if(connectionThread == nullptr)
    {
        oserror.str("");
        
        oserror << "ERROR: Cannot create a connection thread. Terminating " << BLUETIT_SHORT_NAME;
        
        logger->systemLog(oserror.str());
        
        cleanup_and_exit(EXIT_FAILURE);
    }
}

void start_airvpn_connection()
{
    std::string result, serverProfile, countryName = "";
    std::ostringstream oserror;
    bool connect_country, timeout, pattern_allowed = false;
    int wait_seconds = 0, sleep_seconds = 1;
    RCParser::Directive *directive;

    if(status != BLUETIT_STATUS_READY)
    {
        oserror.str("");
        
        oserror << "ERROR: Cannot start AirVPN connection. " << bluetit_status_description();

        OPENVPN_LOG(oserror.str());

        terminate_client_session();

        return;
    }

    try
    {
        netFilter->setMode(vpn_network_lock_mode);
    }
    catch(NetFilterException &e)
    {
        osstring.str("");

        osstring << "Network Lock Error: " << e.what();

        OPENVPN_LOG(osstring.str());

        terminate_client_session();

        return;
    }

    if(netFilter->isNetworkLockAvailable())
    {
        if(netFilter->isNetworkLockEnabled() == false)
            enable_network_lock(NETLOCKMODE_SESSION);
        else if(persistent_network_lock_mode != NetFilter::Mode::OFF)
            OPENVPN_LOG("Persistent Network Lock and Filter is enabled");

        add_airvpn_bootstrap_to_network_lock();
    }

    manifestSemaphore->permit();

    if(AirVPNManifest::getManifestType() == AirVPNManifest::Type::NOT_SET || AirVPNManifest::getManifestType() == AirVPNManifest::Type::PROCESSING)
    {
        oserror.str("");

        oserror << "Waiting for a valid AirVPN Manifest to be available";

        OPENVPN_LOG(oserror.str());

        timeout = false;
        wait_seconds = 0;

        do
        {
            sleep(sleep_seconds);
            
            wait_seconds += sleep_seconds;
            
            if(wait_seconds >= WAIT_AIRVPN_MANIFEST_TIMEOUT)
                timeout = true;
        }
        while((AirVPNManifest::getManifestType() == AirVPNManifest::Type::NOT_SET || AirVPNManifest::getManifestType() == AirVPNManifest::Type::PROCESSING) && timeout == false);
        
        if(timeout)
        {
            result = BT_ERROR;

            result += ": Cannot start AirVPN Connection. AirVPN Manifest not available.";

            OPENVPN_LOG(result);

            terminate_client_session();

            return;
        }
    }

    if(airvpn_user_login() == false)
    {
        result = BT_ERROR;

        result += ": AirVPN login failed for user ";
        result += dbusConnector->stringToLocale(airVpnUsername);

        OPENVPN_LOG(result);

        terminate_client_session();

        return;
    }

    if(airVpnServerPattern.empty() && airVpnCountryPattern.empty())
    {
        connectionSignalExit = new std::promise<void>();
        
        connectionFutureObject = connectionSignalExit->get_future();

        start_airvpn_quick_connection(std::move(connectionFutureObject));

        return;
    }

    if(airVpnServerPattern.empty() == false)
    {
        pattern_allowed = true;

        directive = rcParser->getDirective(RC_DIRECTIVE_AIRWHITESERVERLIST);
    
        if(directive != nullptr)
        {
            if(rcParser->isValueInList(directive, airVpnServerPattern) == false)
                pattern_allowed = false;
        }

        directive = rcParser->getDirective(RC_DIRECTIVE_AIRBLACKSERVERLIST);
    
        if(directive != nullptr)
        {
            if(rcParser->isValueInList(directive, airVpnServerPattern) == true)
                pattern_allowed = false;
        }

        if(pattern_allowed == false)
        {
            osstring.str("");
            
            osstring << "ERROR: Server \"" << airVpnServerPattern << "\" is not allowed by " << BLUETIT_SHORT_NAME << " policy.";

            OPENVPN_LOG(osstring.str());

            terminate_client_session();

            return;
        }
    }

    if(airVpnCountryPattern.empty() == false)
    {
        pattern_allowed = true;

        directive = rcParser->getDirective(RC_DIRECTIVE_AIRWHITECOUNTRYLIST);
    
        if(directive != nullptr)
        {
            if(rcParser->isValueInList(directive, airVpnCountryPattern) == false)
                pattern_allowed = false;
        }

        directive = rcParser->getDirective(RC_DIRECTIVE_AIRBLACKCOUNTRYLIST);
    
        if(directive != nullptr)
        {
            if(rcParser->isValueInList(directive, airVpnCountryPattern) == true)
                pattern_allowed = false;
        }

        if(pattern_allowed == false)
        {
            osstring.str("");
            
            osstring << "ERROR: Country \"" << airVpnCountryPattern << "\" is not allowed by " << BLUETIT_SHORT_NAME << " policy.";

            OPENVPN_LOG(osstring.str());

            terminate_client_session();

            return;
        }
    }

    if(airVpnKeyName.empty())
        airVpnKeyName = airVpnUser->getFirstProfileName();
    else
    {
        if(airVpnUser->getUserKey(airVpnKeyName).name == "")
        {
            result = "ERROR: Key \"" + dbusConnector->stringToLocale(airVpnKeyName) + "\" does not exist for user " + dbusConnector->stringToLocale(airVpnUser->getUserName());

            OPENVPN_LOG(result);

            terminate_client_session();

            return;
        }
    }

    if(airVpnKeyName.empty())
    {
        result = "ERROR: User " + dbusConnector->stringToLocale(airVpnUser->getUserName()) + " has no defined keys";

        OPENVPN_LOG(result);

        terminate_client_session();

        return;
    }

    OPENVPN_LOG("Selected user key: " + airVpnKeyName);

    if(!airVpnServerPattern.empty())
        connect_country = false;
    else
        connect_country = true;

    serverProfile = airvpn_create_profile(connect_country, 0);
    
    if(serverProfile.empty())
    {
        if(connect_country == true)
            result = "ERROR: Country " + dbusConnector->stringToLocale(airVpnCountryPattern) + " does not exist";
        else
            result = "ERROR: AirVPN server " + dbusConnector->stringToLocale(airVpnServerPattern) + " does not exist";

        OPENVPN_LOG(result);

        terminate_client_session();

        return;
    }
    else
    {
        result = set_openvpn_profile(serverProfile);
        
        if(result != BT_OK)
        {
            OPENVPN_LOG(result);

            terminate_client_session();

            return;
        }
    }
    
    osstring.str("");

    if(connect_country == false)
    {
        AirVPNServer server = airVpnManifest->getServerByName(airVpnServerPattern);

        if(server.getName() == "")
        {
            result = "ERROR: AirVPN server " + dbusConnector->stringToLocale(airVpnServerPattern) + " does not exist";

            OPENVPN_LOG(result);

            terminate_client_session();

            return;
        }

        osstring.str("");

        osstring << "Starting connection to AirVPN server " + dbusConnector->stringToLocale(server.getName());

        connectionInformation = "Connected to AirVPN server " + dbusConnector->stringToLocale(server.getName());
        
        if(server.getLocation() != "")
        {
            osstring << ", " << server.getLocation();

            connectionInformation += ", " + server.getLocation();
        }

        if(server.getCountryName() != "")
        {
            osstring << " (" << server.getCountryName() << ")";

            connectionInformation += " (" + server.getCountryName() + ")";
        }
    }
    else
    {
        if(AirVPNTools::iequals(airVpnCountryPattern, "earth") ||
           AirVPNTools::iequals(airVpnCountryPattern, "europe") ||
           AirVPNTools::iequals(airVpnCountryPattern, "africa") ||
           AirVPNTools::iequals(airVpnCountryPattern, "asia") ||
           AirVPNTools::iequals(airVpnCountryPattern, "america") ||
           AirVPNTools::iequals(airVpnCountryPattern, "oceania"))
        {
            countryName = airVpnCountryPattern;
        }
        else if(CountryContinent::getCountryName(airVpnCountryPattern) != "")
        {
            countryName = CountryContinent::getCountryName(airVpnCountryPattern);
        }
        else
        {
            if(CountryContinent::getCountryCode(airVpnCountryPattern) != "")
                countryName = airVpnCountryPattern;
        }

        if(countryName != "")
            countryName[0] = toupper(countryName[0]);
        else
        {
            result = "ERROR: Country " + dbusConnector->stringToLocale(airVpnCountryPattern) + " does not exist";

            OPENVPN_LOG(result);

            terminate_client_session();

            return;
        }

        osstring.str("");

        osstring << "Starting connection to currently best AirVPN server in " + dbusConnector->stringToLocale(countryName);

        connectionInformation = "Connected to currently best AirVPN server in " + dbusConnector->stringToLocale(countryName);
    }

    if(osstring.str() != "")
        OPENVPN_LOG(osstring.str());

    start_openvpn_connection(maxConnectionRetries);
    
    return;
}

void start_airvpn_quick_connection(const std::future<void> &future)
{
    std::string result, serverProfile, line;
    std::vector<AirVPNServer> airVPNServerList;
    std::vector<std::string> list, item;
    RCParser::Directive *directive;
    int currentConnectionSchemeIndex, ipEntry, ipEntryOffset;
    bool endOfConnectionScheme;

    AirVPNServerProvider *airVPNServerProvider = new AirVPNServerProvider(airVpnUser, BLUETIT_RESOURCE_DIRECTORY);

    if(airVPNServerProvider == nullptr)
    {
        logger->systemLog("Cannot create AirVPNServerProvider object. Exiting.");

        if(netFilter->isNetworkLockEnabled())
            disable_network_lock();

        cleanup_and_exit(EXIT_FAILURE);
    }

    airVPNServerProvider->setUserIP(airVpnUser->getUserIP());
    airVPNServerProvider->setUserCountry(airVpnUser->getUserCountry());

    list.clear();

    if(airVpnClientWhiteServerList != "")
    {
        item = AirVPNTools::split(airVpnClientWhiteServerList, ",");

        for(size_t i = 0; i < item.size(); ++i)
            list.push_back(AirVPNTools::toLower(item[i]));
    }
    else if(rcParser->getDirective(RC_DIRECTIVE_AIRWHITESERVERLIST) != nullptr)
    {
        directive = rcParser->getDirective(RC_DIRECTIVE_AIRWHITESERVERLIST);

        for(size_t i = 0; i < directive->value.size(); ++i)
            list.push_back(AirVPNTools::toLower(directive->value[i]));
    }

    airVPNServerProvider->setServerWhitelist(list);

    list.clear();

    if(airVpnClientBlackServerList != "")
    {
        item = AirVPNTools::split(airVpnClientBlackServerList, ",");

        for(size_t i = 0; i < item.size(); ++i)
            list.push_back(AirVPNTools::toLower(item[i]));
    }
    
    if(rcParser->getDirective(RC_DIRECTIVE_AIRBLACKSERVERLIST) != nullptr)
    {
        directive = rcParser->getDirective(RC_DIRECTIVE_AIRBLACKSERVERLIST);

        for(size_t i = 0; i < directive->value.size(); ++i)
            list.push_back(AirVPNTools::toLower(directive->value[i]));
    }

    airVPNServerProvider->setServerBlacklist(list);

    list.clear();

    if(airVpnClientWhiteCountryList != "")
    {
        item = AirVPNTools::split(airVpnClientWhiteCountryList, ",");

        for(size_t i = 0; i < item.size(); ++i)
            list.push_back(AirVPNTools::toUpper(item[i]));
    }
    else if(rcParser->getDirective(RC_DIRECTIVE_AIRWHITECOUNTRYLIST) != nullptr)
    {
        directive = rcParser->getDirective(RC_DIRECTIVE_AIRWHITECOUNTRYLIST);

        for(size_t i = 0; i < directive->value.size(); ++i)
            list.push_back(AirVPNTools::toUpper(directive->value[i]));
    }

    airVPNServerProvider->setCountryWhitelist(list);

    list.clear();

    if(airVpnClientBlackCountryList != "")
    {
        item = AirVPNTools::split(airVpnClientBlackCountryList, ",");

        for(size_t i = 0; i < item.size(); ++i)
            list.push_back(AirVPNTools::toUpper(item[i]));
    }
    
    if(rcParser->getDirective(RC_DIRECTIVE_AIRBLACKCOUNTRYLIST) != nullptr)
    {
        directive = rcParser->getDirective(RC_DIRECTIVE_AIRBLACKCOUNTRYLIST);

        for(size_t i = 0; i < directive->value.size(); ++i)
            list.push_back(AirVPNTools::toUpper(directive->value[i]));
    }

    airVPNServerProvider->setCountryBlacklist(list);

    if(airVpnTlsMode == TLS_MODE_CRYPT)
    {
        airVPNServerProvider->setTlsMode(AirVPNServerProvider::TLSMode::TLS_CRYPT);
        
        ipEntry = 3;
    }
    else
    {
        airVPNServerProvider->setTlsMode(AirVPNServerProvider::TLSMode::TLS_AUTH);
        
        ipEntry = 1;
    }

    airVPNServerProvider->setSupportIPv4(true);
    
    if(airVpnIPv6 == true || airVpn6to4 == true)
        airVPNServerProvider->setSupportIPv6(true);
    else
        airVPNServerProvider->setSupportIPv6(false);
    
    airVPNServerList = airVPNServerProvider->getFilteredServerList(forbidQuickHomeCountry);

    delete airVPNServerProvider;

    if(airVpnKeyName.empty())
        airVpnKeyName = airVpnUser->getFirstProfileName();
    else
    {
        if(airVpnUser->getUserKey(airVpnKeyName).name == "")
        {
            result = "ERROR: Key \"" + dbusConnector->stringToLocale(airVpnKeyName) + "\" does not exist for user " + dbusConnector->stringToLocale(airVpnUser->getUserName());

            OPENVPN_LOG(result);

            terminate_client_session();

            return;
        }
    }
    
    if(airVpnKeyName.empty())
    {
        result = "ERROR: User " + dbusConnector->stringToLocale(airVpnUser->getUserName()) + " has no defined keys";

        OPENVPN_LOG(result);

        terminate_client_session();

        return;
    }

    OPENVPN_LOG("Selected user key: " + airVpnKeyName);

    if(airVpnAutoQuickMode == true)
    {
        OPENVPN_LOG("Auto quick connection mode enabled");

        osstring.str("");

        osstring << "Loading connection schemes from " << CONNECTION_SEQUENCE_FILE;

        logger->systemLog(osstring.str());

        std::ifstream connectionSchemeFile(CONNECTION_SEQUENCE_FILE);

        connectionSchemeList.clear();

        if(connectionSchemeFile.is_open())
        {
            while(std::getline(connectionSchemeFile, line))
            {
                item = AirVPNTools::split(line, ",");

                if(item.size() == 3)
                {
                    ConnectionScheme cs;
                    
                    cs.protocol = AirVPNTools::trim(AirVPNTools::toLower(item[0]));
                    
                    try
                    {
                        cs.port = std::stoi(item[1]);
                    }
                    catch(std::exception &e)
                    {
                        cs.port = 443;
                    }

                    try
                    {
                        cs.entry = std::stoi(item[2]);
                    }
                    catch(std::exception &e)
                    {
                        cs.entry = 0;
                    }


                    connectionSchemeList.push_back(cs);
                }
            }

            connectionSchemeFile.close();
        }
        else
        {
            OPENVPN_LOG("Cannot open connection scheme file. Auto quick connection mode disabled.");
            
            airVpnAutoQuickMode = false;
        }
    }
    else
        OPENVPN_LOG("Auto quick connection scheme is disabled. Using custom/default connection options.");

    for(AirVPNServer server : airVPNServerList)
    {
        airVpnServerPattern = server.getName();

        endOfConnectionScheme = false;
        currentConnectionSchemeIndex = 0;

        while(endOfConnectionScheme == false)
        {
            osstring.str("");

            osstring << "Starting quick connection to AirVPN server " << dbusConnector->stringToLocale(airVpnServerPattern);

            connectionInformation = "Connected to AirVPN server " + dbusConnector->stringToLocale(airVpnServerPattern);
            
            if(server.getLocation() != "")
            {
                osstring << ", " << dbusConnector->stringToLocale(server.getLocation());
                
                connectionInformation += ", " + dbusConnector->stringToLocale(server.getLocation());
            }

            if(server.getCountryName() != "")
            {
                osstring << " (" << dbusConnector->stringToLocale(server.getCountryName()) << ")";
                
                connectionInformation += " (" + dbusConnector->stringToLocale(server.getCountryName()) + ")";
            }

            OPENVPN_LOG(osstring.str());

            if(airVpnAutoQuickMode == true)
            {
                ConnectionScheme cs = connectionSchemeList[currentConnectionSchemeIndex];
                
                vpn_port = std::to_string(cs.port);
                vpn_proto = cs.protocol;
                ipEntryOffset = cs.entry;

                currentConnectionSchemeIndex++;
                
                if(currentConnectionSchemeIndex == connectionSchemeList.size())
                    endOfConnectionScheme = true;
            }
            else
            {
                ipEntryOffset = 0;

                endOfConnectionScheme = true;
            }

            osstring.str("");

            osstring << "Trying protocol " << AirVPNTools::toUpper(vpn_proto);
            osstring << ", port " << vpn_port << ", IP entry " << ipEntry + ipEntryOffset;

            OPENVPN_LOG(osstring.str());

            serverProfile = airvpn_create_profile(false, ipEntryOffset);
            
            if(serverProfile.empty())
            {
                OPENVPN_LOG("ERROR: failed to create OpenVPN profile");

                terminate_client_session();

                return;
            }
            else
            {
                result = set_openvpn_profile(serverProfile);
                
                if(result != BT_OK)
                {
                    OPENVPN_LOG(result);

                    terminate_client_session();

                    return;
                }
            }

            start_openvpn_connection(1);

            if(openVpnConnectionStatus.error == false)
            {
                terminate_client_session();

                return;
            }
            
            if(future.wait_for(std::chrono::seconds(5)) != std::future_status::timeout)
            {
                logger->systemLog("Terminating quick connection thread: stop signal received");

                return;
            }
        }
    }

    OPENVPN_LOG("ERROR: Reached end of AirVPN server list. No suitable server found.");

    terminate_client_session();
}

void start_openvpn_connection(int max_connection_retries)
{
    bool done = false, connection_success = false;
    int i;

    if(status != BLUETIT_STATUS_READY)
    {
        osstring.str("");
        
        osstring << "ERROR: Cannot start OpenVPN connection. " << bluetit_status_description();

        OPENVPN_LOG(osstring.str());

        terminate_client_session();

        return;
    }

    OPENVPN_LOG("Starting VPN Connection");

    for(i = 0; i < max_connection_retries && done == false; i++)
    {
        if(i > 0)
        {
            osstring.str("");

            osstring << "Retrying VPN Connection (" << i << "/" << max_connection_retries << ")";

            OPENVPN_LOG(osstring.str());
        }

        connection_success = establish_openvpn_connection();

        if(connection_success == false)
        {
            status = BLUETIT_STATUS_READY;

            OPENVPN_LOG("Waiting for pending threads to finish");

            stop_connection_stats_thread();

            terminate_client_session();

            if(vpnClient != nullptr)
                delete vpnClient;

            vpnClient = nullptr;
        }

        if(openVpnConnectionStatus.error == false || connection_success == false)
            done = true;
    }
}

bool establish_openvpn_connection()
{
    std::string result = BT_ERROR;
    RCParser::Directive *rcDirective;
    ClientAPI::Status connect_status;

    if(status != BLUETIT_STATUS_READY)
    {
        osstring.str("");
        
        osstring << "ERROR: Cannot start OpenVPN connection. " << bluetit_status_description();

        OPENVPN_LOG(osstring.str());

        terminate_client_session();

        return false;
    }

    if(vpnClient != nullptr)
    {
        result = BT_ERROR;

        result += ": VPN is already connected and running. Start request aborted.";

        OPENVPN_LOG(result);

        terminate_client_session();

        return false;
    }

    if(vpn_profile == "")
    {
        result = BT_ERROR;

        result += ": no openvpn profile provided. Start request aborted.";

        OPENVPN_LOG(result);

        terminate_client_session();

        return false;
    }

    try
    {
        vpnClient = new VpnClient(netFilter, BLUETIT_RESOURCE_DIRECTORY, SYSTEM_DNS_BACKUP_FILE, RESOLVDOTCONF_BACKUP);
    }
    catch(NetFilterException &e)
    {
        osstring.str("");

        osstring << "NetFilterException: " << e.what();

        logger->systemLog(osstring.str());

        if(netFilter->isNetworkLockEnabled())
            disable_network_lock();

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(vpnClient != nullptr)
    {
        logger->systemLog("OpenVPN3 client successfully created and initialized.");
        
        vpnClient->setConnectionInformation(connectionInformation);
    }
    else
    {
        logger->systemLog("Cannot create OpenVPN3 client. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    vpnClient->subscribeEvent(ClientEvent::Type::CONNECTED, vpn_client_connected_event_callback);

    if(clientConfig == nullptr)
    {
        logger->systemLog("establish_openvpn_connection(): clientConfig is null. Exiting.");

        if(netFilter->isNetworkLockEnabled())
            disable_network_lock();

        cleanup_and_exit(EXIT_FAILURE);
    }

    result = BT_OK;

    try
    {
        if(!vpn_epki_cert_fn.empty())
            clientConfig->externalPkiAlias = "epki"; // dummy string

        PeerInfo::Set::parse_csv(vpn_peer_info, clientConfig->peerInfo);

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_TUNPERSIST);

        if(rcDirective != nullptr)
        {
            osstring.str("");
    
            osstring << "TUN persistence is ";

            if(rcParser->isDirectiveEnabled(rcDirective) == true)
            {
                clientConfig->tunPersist = true;

                osstring << "enabled";
            }
            else
            {
                clientConfig->tunPersist = false;

                osstring << "disabled";
            }

            osstring << " by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        if(clientConfig->tunPersist == false)
            OPENVPN_LOG("WARNING: TUN persistence is disabled. Pausing and resuming VPN connection is not allowed.");
        else
            OPENVPN_LOG("TUN persistence is enabled.");
            
        rcDirective = rcParser->getDirective(RC_DIRECTIVE_TCPQUEUELIMIT);

        if(rcDirective != nullptr)
        {
            try
            {
                clientConfig->tcpQueueLimit = std::stoi(rcDirective->value[0]);
            }
            catch(std::exception &e)
            {
                clientConfig->tcpQueueLimit = TCP_QUEUE_LIMIT_DEFAULT;
            }

            osstring.str("");

            osstring << "TCP queue limit set to " << rcDirective->value[0] << " by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_NCPDISABLE);

        if(rcDirective != nullptr)
        {
            osstring.str("");

            osstring << "Negotiable Crypto Parameters (NCP) is ";

            if(rcParser->isDirectiveEnabled(rcDirective) == true)
            {
                clientConfig->disableNCP = true;

                osstring << "disabled";
            }
            else
            {
                clientConfig->disableNCP = false;

                osstring << "enabled";
            }

            osstring << " by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_TIMEOUT);

        if(rcDirective != nullptr)
        {
            try
            {
                clientConfig->connTimeout = std::stoi(rcDirective->value[0]);
            }
            catch(std::exception &e)
            {
                clientConfig->connTimeout = 0;
            }

            osstring.str("");

            osstring << "Connection timeout set to " << rcDirective->value[0] << " by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_COMPRESS);

        if(rcDirective != nullptr)
        {
            vpn_compress = AirVPNTools::normalizeBoolValue(rcDirective->value[0], "yes", "no");

            clientConfig->compressionMode = vpn_compress;

            osstring.str("");

            osstring << "Compression mode set to '" << rcDirective->value[0] << "' by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_TLSVERSIONMIN);

        if(rcDirective != nullptr)
        {
            vpn_tlsVersionMinOverride = rcDirective->value[0];

            clientConfig->tlsVersionMinOverride = vpn_tlsVersionMinOverride;

            osstring.str("");

            osstring << "TLS minumum version set to '" << rcDirective->value[0] << "' by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_PROXYHOST);

        if(rcDirective != nullptr)
        {
            vpn_proxyHost = rcDirective->value[0];

            clientConfig->proxyHost = vpn_proxyHost;

            osstring.str("");

            osstring << "Proxy host set to '" << rcDirective->value[0] << "' by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_PROXYPORT);

        if(rcDirective != nullptr)
        {
            vpn_proxyPort = rcDirective->value[0];

            clientConfig->proxyPort = vpn_proxyPort;

            osstring.str("");

            osstring << "Proxy port set to '" << rcDirective->value[0] << "' by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_PROXYUSERNAME);

        if(rcDirective != nullptr)
        {
            vpn_proxyUsername = rcDirective->value[0];

            clientConfig->proxyUsername = vpn_proxyUsername;

            osstring.str("");

            osstring << "Proxy user name set by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_PROXYPASSWORD);

        if(rcDirective != nullptr)
        {
            vpn_proxyPassword = rcDirective->value[0];

            clientConfig->proxyPassword = vpn_proxyPassword;

            osstring.str("");

            osstring << "Proxy password set by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_PROXYBASIC);

        if(rcDirective != nullptr)
        {
            osstring.str("");

            osstring << "Proxy HTTP basic auth is";

            if(rcParser->isDirectiveEnabled(rcDirective) == true)
            {
                clientConfig->proxyAllowCleartextAuth = true;

                osstring << "enabled";
            }
            else
            {
                clientConfig->proxyAllowCleartextAuth = false;

                osstring << "disabled";
            }

            osstring << " by " << BLUETIT_SHORT_NAME << " policy";

            OPENVPN_LOG(osstring.str());
        }

        // allow -s server override to reference a friendly name
        // in the config.
        //   setenv SERVER <HOST>/<FRIENDLY_NAME>

        const ClientAPI::EvalConfig eval = vpnClient->eval_config(*clientConfig);

        if(eval.error)
        {
            result = BT_ERROR;

            result += " eval config error: ";
            result += eval.message;

            OPENVPN_LOG(result);

            terminate_client_session();

            delete vpnClient;

            vpnClient = nullptr;

            return false;
        }
        else if(eval.autologin == false)
        {
            result = BT_ERROR;

            result += ": profiles with credentials are not supported";

            OPENVPN_LOG(result);

            terminate_client_session();

            delete vpnClient;

            vpnClient = nullptr;

            return false;
        }
        else
        {
            if(!clientConfig->serverOverride.empty())
            {
                for(auto &se : eval.serverList)
                {
                    if(clientConfig->serverOverride == se.friendlyName)
                    {
                        clientConfig->serverOverride = se.server;

                        break;
                    }
                }
            }

            logger->systemLog("Successfully set OpenVPN3 client configuration");

            if(!vpn_epki_cert_fn.empty())
            {
                vpnClient->epki_cert = read_text_utf8(vpn_epki_cert_fn);

                if(!vpn_epki_ca_fn.empty())
                    vpnClient->epki_ca = read_text_utf8(vpn_epki_ca_fn);

#if defined(USE_MBEDTLS)

                if(!vpn_epki_key_fn.empty())
                {
                    const std::string epki_key_txt = read_text_utf8(vpn_epki_key_fn);

                    vpnClient->epki_ctx.parse(epki_key_txt, "EPKI", vpn_privateKeyPassword);
                }
                else
                    OPENVPN_THROW_EXCEPTION("--epki-key must be specified");

#endif
            }

            result = BT_OK;

            for(size_t i = 0; i < eval.remoteList.size() && result == BT_OK; ++i)
            {
                const ClientAPI::RemoteEntry& re = eval.remoteList[i];

                result = check_rc_value("remote server", RC_DIRECTIVE_REMOTE, re.server);
            }

            if(clientConfig->protoOverride == "" && result == BT_OK)
                result = check_rc_value("protocol", RC_DIRECTIVE_PROTO, (eval.remoteProto == "tcp-client") ? "tcp" : eval.remoteProto);

            if(clientConfig->portOverride == "" && result == BT_OK)
                result = check_rc_value("port", RC_DIRECTIVE_PORT, eval.remotePort);

            if(clientConfig->cipherOverrideAlgorithm == "" && result == BT_OK)
                result = check_rc_value("cipher algorithm", RC_DIRECTIVE_CIPHER, eval.cipher);

            if(eval.cipher != "" && vpnClient->isDataCipherSupported(eval.cipher) == false && result == BT_OK)
            {
                osstring.str("");

                osstring << "ERROR: cipher " << eval.cipher << " is not supported. Please use --list-data-ciphers to list supported data ciphers.";
                
                result = osstring.str();

                OPENVPN_LOG(result);
            }

            if(result != BT_OK)
            {
                terminate_client_session();

                delete vpnClient;

                vpnClient = nullptr;

                return false;
            }

            if(rcParser->getDirective(RC_DIRECTIVE_NETWORKLOCK) != nullptr || rcParser->getDirective(RC_DIRECTIVE_NETWORKLOCKPERSIST) != nullptr)
            {
                vpn_network_lock_mode = policy_network_lock_mode;

                osstring.str("");

                osstring << "Network lock set to '" << netFilter->getModeDescription(policy_network_lock_mode) << "' by " << BLUETIT_SHORT_NAME << " policy";

                OPENVPN_LOG(osstring.str());
            }

            rcDirective = rcParser->getDirective(RC_DIRECTIVE_IGNOREDNSPUSH);

            if(rcDirective != nullptr)
            {
                osstring.str("");

                osstring << "Ignore DNS push is ";

                if(rcParser->isDirectiveEnabled(rcDirective) == true)
                {
                    vpn_ignore_dns_push = true;

                    osstring << "enabled";
                }
                else
                {
                    vpn_ignore_dns_push = false;

                    osstring << "disabled";
                }

                osstring << " by " << BLUETIT_SHORT_NAME << " policy";

                OPENVPN_LOG(osstring.str());
            }

#ifdef OPENVPN_REMOTE_OVERRIDE
            vpnClient->set_remote_override_cmd(remote_override_cmd);
#endif

            // start VPN connection

            vpnClient->setConfig(*clientConfig);

            vpnClient->setEvalConfig(eval);

            vpnClient->setNetworkLockMode(vpn_network_lock_mode);

            vpnClient->ignoreDnsPush(vpn_ignore_dns_push);

            logger->systemLog("Starting OpenVPN3 connection thread");

            status = BLUETIT_STATUS_CONNECTED;

            connectionStatsSignalExit = new std::promise<void>();
    
            connectionStatsFutureObject = connectionStatsSignalExit->get_future();

            connectionStatsThread = new std::thread(connection_stats_updater, std::move(connectionStatsFutureObject));

            if(connectionStatsThread == nullptr)
            {
                osstring.str("");
                
                osstring << "ERROR: Cannot create the connection statistics thread. Terminating " << BLUETIT_SHORT_NAME;
                
                OPENVPN_LOG(osstring.str());

                cleanup_and_exit(EXIT_FAILURE);
            }

            connect_status = vpnClient->connect();

            if(connect_status.error)
            {
                osstring.str("");

                osstring << "OpenVPN3 CONNECT ERROR: ";

                if(!connect_status.status.empty())
                    osstring << connect_status.status << ": ";

                osstring << connect_status.message;

                OPENVPN_LOG(osstring.str());

                terminate_client_session();

                status = BLUETIT_STATUS_READY;
            }

            logger->systemLog("OpenVPN3 connection thread finished");

            status = BLUETIT_STATUS_READY;

            openVpnConnectionStatus = connect_status;
        }
    }
    catch(const std::exception& e)
    {
        osstring.str("");

        osstring << "OpenVPN3 Connect thread exception: " << e.what();

        logger->systemLog(osstring.str());

        cleanup_and_exit(EXIT_FAILURE);
    }

    return !(vpnClient->eventError() || vpnClient->eventFatalError());
}

std::string stop_openvpn_connection()
{
    std::string result = BT_OK;

    if(vpnClient == nullptr || status != BLUETIT_STATUS_CONNECTED)
    {
        osstring.str("");

        osstring << "ERROR: " << BLUETIT_SHORT_NAME << " is not connected to VPN";

        return osstring.str();
    }

    logger->systemLog("Stopping OpenVPN3 connection thread");

    if(vpnClient)
        vpnClient->stop();

    // wait for connection statistics threads to exit

    stop_connection_stats_thread();

    // wait for connection threads to exit

    stop_connection_thread();

    send_event(BT_EVENT_DISCONNECTED, "");
        
    status = BLUETIT_STATUS_READY;

    delete vpnClient;

    vpnClient = nullptr;

    reset_settings();

    if(persistent_network_lock_mode != NetFilter::Mode::OFF)
    {
        logger->systemLog("Enabling persistent Network Filter and Lock");
        
        manifestSemaphore->forbid("Persistent Network Filter and Lock enabled");

        enable_network_lock(NETLOCKMODE_PERSISTENT);

        logger->flushLog();
    }

    return result;
}

void stop_connection_thread()
{
    if(connectionThread != nullptr)
    {
        if(connectionSignalExit != nullptr)
        {
            try
            {
                connectionSignalExit->set_value();
            }
            catch(std::future_error& e)
            {
                osstring.str("");
                
                osstring << "ERROR: Connection Thread (" << e.code() << "): " << e.what()
;
                OPENVPN_LOG(osstring.str());
            }
        }

        if(connectionThread->joinable())
            connectionThread->join();

        delete connectionThread;

        connectionThread = nullptr;

        if(connectionSignalExit != nullptr)
        {
            delete connectionSignalExit;

            connectionSignalExit = nullptr;
        }
    }
}

void stop_connection_stats_thread()
{
    if(connectionStatsThread != nullptr)
    {
        if(connectionStatsSignalExit != nullptr)
        {
            try
            {
                connectionStatsSignalExit->set_value();
            }
            catch(std::future_error& e)
            {
                osstring.str("");
                
                osstring << "ERROR: Connection Statistics Thread (" << e.code() << "): " << e.what();

                OPENVPN_LOG(osstring.str());
            }
        }

        if(connectionStatsThread->joinable())
            connectionStatsThread->join();

        delete connectionStatsThread;

        connectionStatsThread = nullptr;

        if(connectionStatsSignalExit != nullptr)
        {
            delete connectionStatsSignalExit;

            connectionStatsSignalExit = nullptr;
        }
    }
}

std::string reconnect_openvpn()
{
    std::string result = BT_ERROR;

    if(status == BLUETIT_STATUS_CONNECTED)
    {
        logger->systemLog("Reconnecting VPN server");

        if(vpnClient != nullptr)
        {
            vpnClient->reconnect(0);

            result = BT_OK;
        }
        else
        {
            result = BT_ERROR;

            result += ": reconnect_openvpn() - vpnClient is null.";

            logger->systemLog(result);
        }
    }
    else if(status == BLUETIT_STATUS_READY)
    {
        logger->systemLog("VPN is not connected. Trying to start a new connection.");

        result = BT_OK;

        start_openvpn_connection(maxConnectionRetries);
    }
    else
    {
        result = BT_ERROR;

        result += ": ";
        result += BLUETIT_SHORT_NAME;
        result += " is not connected to a VPN or is not ready. Reconnect command aborted.";

        logger->systemLog(result);
    }

    return result;
}

std::string pause_openvpn_connection()
{
    std::string result = BT_ERROR;
    
    if(clientConfig->tunPersist == false)
    {
        result = BT_ERROR;
        
        result += ": TUN persistence is disabled. Pausing the VPN connection is not allowed.";
    }
    else if(status == BLUETIT_STATUS_CONNECTED)
    {
        logger->systemLog("Pausing VPN connection");

        if(vpnClient != nullptr)
        {
            vpnClient->pause("Received pause signal");

            status = BLUETIT_STATUS_PAUSED;

            result = BT_OK;

            send_event(BT_EVENT_PAUSE, "");
        }
        else
        {
            result = BT_ERROR;

            result += ": pause_openvpn_connection() - vpnClient is null.";

            logger->systemLog(result);
        }
    }
    else
    {
        result = BT_ERROR;

        result += ": ";
        result += BLUETIT_SHORT_NAME;
        result += " is not connected to a VPN. Pause command aborted.";

        logger->systemLog(result);
    }

    return result;
}

std::string resume_openvpn_connection()
{
    std::string result = BT_ERROR;

    if(clientConfig->tunPersist == false)
    {
        result = BT_ERROR;
        
        result += ": TUN persistence is disabled. Resuming the VPN connection is not allowed.";
    }
    else if(status == BLUETIT_STATUS_PAUSED)
    {
        logger->systemLog("Resuming VPN connection");

        if(vpnClient != nullptr)
        {
            vpnClient->resume();

            status = BLUETIT_STATUS_CONNECTED;

            result = BT_OK;

            send_event(BT_EVENT_RESUME, "");
        }
        else
        {
            result = BT_ERROR;

            result += ": resume_openvpn_connection() - vpnClient is null.";

            logger->systemLog(result);
        }
    }
    else
    {
        result = BT_ERROR;

        result += ": VPN connection is not paused. Resume command aborted.";

        logger->systemLog(result);
    }

    return result;
}

void connection_stats_updater(const std::future<void> &future)
{
    std::map<std::string, std::string> openvpnStats;
    std::map<std::string, std::string>::iterator it, itStats;
    std::string openvpnStatKeys[] = {
                                     "bytes_in", "bytes_out",
                                     "packets_in", "packets_out",
                                     "tun_bytes_in", "tun_bytes_out",
                                     "tun_packets_in", "tun_packets_out"
                                    };

    const int init_wait_seconds = 5;
    int serverStatInterval = 0, intervalCounter = 0;
    long long connectionTime, rate = 0, maxRateIn = 0, maxRateOut = 0;
    AirVPNServer server = AirVPNServer("");
    
    logger->systemLog("Connection statistics updater thread started");

    connectionStats.clear();

    sleep(init_wait_seconds);

    connectionTime = init_wait_seconds;

    serverStatInterval = airVpnManifestUpdateInterval * 60;

    openvpnStats = vpnClient->get_connection_stats();

    for(std::pair<std::string, std::string> row : openvpnStats)
        connectionStats.insert(std::make_pair(row.first, row.second));

    it = openvpnStats.find("server_ip");
    
    if(it != openvpnStats.end())
    {
        server = AirVPNManifest::getServerByIP(it->second);
        
        connectionStats.insert(std::make_pair("airvpn_server_name", server.getName()));
        connectionStats.insert(std::make_pair("airvpn_server_location", server.getLocation()));
        connectionStats.insert(std::make_pair("airvpn_server_region", server.getRegion()));
        connectionStats.insert(std::make_pair("airvpn_server_country", server.getCountryName()));
        connectionStats.insert(std::make_pair("airvpn_server_country_code", server.getCountryCode()));
        connectionStats.insert(std::make_pair("airvpn_server_continent", server.getContinent()));
        connectionStats.insert(std::make_pair("airvpn_server_bandwidth", std::to_string(server.getEffectiveBandWidth())));
        connectionStats.insert(std::make_pair("airvpn_server_max_bandwidth", std::to_string(server.getMaxBandWidth())));
        connectionStats.insert(std::make_pair("airvpn_server_users", std::to_string(server.getUsers())));
        connectionStats.insert(std::make_pair("airvpn_server_warning_open", server.getWarningOpen()));
        connectionStats.insert(std::make_pair("airvpn_server_warning_closed", server.getWarningClosed()));
        connectionStats.insert(std::make_pair("airvpn_server_load", std::to_string(server.getLoad())));
        connectionStats.insert(std::make_pair("airvpn_server_tls_ciphers", server.getTlsCipherNames()));
        connectionStats.insert(std::make_pair("airvpn_server_tls_suite_ciphers", server.getTlsSuiteCipherNames()));
        connectionStats.insert(std::make_pair("airvpn_server_data_ciphers", server.getDataCipherNames()));
        connectionStats.insert(std::make_pair("airvpn_server_score", std::to_string(server.getScore())));
    }

    connectionStats.insert(std::make_pair("status", ""));
    connectionStats.insert(std::make_pair("connection_time", std::to_string(connectionTime)));

    it = openvpnStats.find("BYTES_IN");
            
    if(it != openvpnStats.end())
    {
        connectionStats.insert(std::make_pair("rate_in", it->second));
        connectionStats.insert(std::make_pair("bytes_in", it->second));
    }
    else
    {
        connectionStats.insert(std::make_pair("rate_in", "0"));
        connectionStats.insert(std::make_pair("bytes_in", "0"));
    }

    it = openvpnStats.find("BYTES_OUT");
            
    if(it != openvpnStats.end())
    {
        connectionStats.insert(std::make_pair("rate_out", it->second));
        connectionStats.insert(std::make_pair("bytes_out", it->second));
    }
    else
    {
        connectionStats.insert(std::make_pair("rate_out", "0"));
        connectionStats.insert(std::make_pair("bytes_out", "0"));
    }

    connectionStats.insert(std::make_pair("max_rate_in", "0"));
    connectionStats.insert(std::make_pair("max_rate_out", "0"));

    connectionStats.insert(std::make_pair("tun_bytes_in", "0"));
    connectionStats.insert(std::make_pair("tun_bytes_out", "0"));
    connectionStats.insert(std::make_pair("tun_packets_in", "0"));
    connectionStats.insert(std::make_pair("tun_packets_out", "0"));
    
    do
    {
        it = connectionStats.find("status");
        
        if(it != connectionStats.end())
        {
            switch(status)
            {
                case BLUETIT_STATUS_READY:
                {
                    it->second = "READY";
                }
                break;

                case BLUETIT_STATUS_CONNECTED:
                {
                    it->second = "CONNECTED";
                }
                break;

                case BLUETIT_STATUS_PAUSED:
                {
                    it->second = "PAUSED";
                }
                break;

                case BLUETIT_STATUS_DIRTY_EXIT:
                {
                    it->second = "DIRTY_EXIT";
                }
                break;

                case BLUETIT_STATUS_RESOURCE_DIRECTORY_ERROR:
                {
                    it->second = "RESOURCE_DIRECTORY_ERROR";
                }
                break;

                case BLUETIT_STATUS_INIT_ERROR:
                {
                    it->second = "INIT_ERROR";
                }
                break;

                case BLUETIT_STATUS_LOCK_ERROR:
                {
                    it->second = "LOCK_ERROR";
                }
                break;

                case BLUETIT_STATUS_UNKNOWN:
                {
                    it->second = "UNKNOWN";
                }
                break;

                default:
                {
                    it->second = "UNKNOWN";
                }
                break;
            }
        }

        if(status == BLUETIT_STATUS_CONNECTED)
        {
            connectionTime += CONNECTION_STATS_INTERVAL;

            openvpnStats = vpnClient->get_connection_stats();

            it = openvpnStats.find("bytes_in");
            itStats = connectionStats.find("bytes_in");
        
            if(it != openvpnStats.end() && itStats != connectionStats.end())
            {
                try
                {
                    rate = (std::stoll(it->second) - std::stoll(itStats->second)) / CONNECTION_STATS_INTERVAL;
                }
                catch(std::exception &e)
                {
                    rate = 0;
                }

                it = connectionStats.find("rate_in");
                
                if(it != connectionStats.end())
                    it->second = std::to_string(rate);
                
                if(rate > maxRateIn)
                {
                    it = connectionStats.find("max_rate_in");
                    
                    if(it != connectionStats.end())
                        it->second = std::to_string(rate);
                    
                    maxRateIn = rate;
                }
            }
            
            it = openvpnStats.find("bytes_out");
            itStats = connectionStats.find("bytes_out");
        
            if(it != openvpnStats.end() && itStats != connectionStats.end())
            {
                try
                {
                    rate = (std::stoll(it->second) - std::stoll(itStats->second)) / CONNECTION_STATS_INTERVAL;
                }
                catch(std::exception &e)
                {
                    rate = 0;
                }

                it = connectionStats.find("rate_out");
                
                if(it != connectionStats.end())
                    it->second = std::to_string(rate);
                
                if(rate > maxRateOut)
                {
                    it = connectionStats.find("max_rate_out");
                    
                    if(it != connectionStats.end())
                        it->second = std::to_string(rate);
                    
                    maxRateOut = rate;
                }
            }

            for(std::string key : openvpnStatKeys)
            {
                it = openvpnStats.find(key);
                itStats = connectionStats.find(key);
            
                if(it != openvpnStats.end() && itStats != connectionStats.end())
                    itStats->second = it->second;
            }
            
            it = connectionStats.find("connection_time");
            
            if(it != connectionStats.end())
                it->second = std::to_string(connectionTime);
            
            intervalCounter += CONNECTION_STATS_INTERVAL;
            
            if(intervalCounter > serverStatInterval)
            {
                it = openvpnStats.find("server_ip");
    
                if(it != openvpnStats.end())
                {
                    server = AirVPNManifest::getServerByIP(it->second);

                    itStats = connectionStats.find("airvpn_server_bandwidth");

                    if(itStats != connectionStats.end())
                        itStats->second = std::to_string(server.getEffectiveBandWidth());

                    itStats = connectionStats.find("airvpn_server_users");

                    if(itStats != connectionStats.end())
                        itStats->second = std::to_string(server.getUsers());

                    itStats = connectionStats.find("airvpn_server_load");

                    if(itStats != connectionStats.end())
                        itStats->second = std::to_string(server.getLoad());

                    itStats = connectionStats.find("airvpn_server_score");

                    if(itStats != connectionStats.end())
                        itStats->second = std::to_string(server.getScore());
                }

                intervalCounter = 0;
            }
        }
    }
    while(future.wait_for(std::chrono::seconds(CONNECTION_STATS_INTERVAL)) == std::future_status::timeout);

    connectionStats.clear();
    
    logger->systemLog("Connection statistics updater thread finished");
}

void connection_stats(DBusResponse &response)
{
    DBusResponse::Item item;
    DBusResponse::ItemIterator it;

    response.clear();

    if(status == BLUETIT_STATUS_CONNECTED)
    {
        response.setResponse(BT_OK);

        for(std::pair<std::string, std::string> row : connectionStats)
            response.addToItem(item, row.first, row.second);

        response.add(item);
    }
    else
    {
        osstring.str("");

        osstring << BLUETIT_SHORT_NAME << " is not connected";

        response.setResponse(osstring.str());
    }
}

std::string set_bluetit_options(const std::vector<std::string> &options)
{
    std::ostringstream oserror;
    std::string result;
    OptionParser::Error optionError;
    OptionParser::Options parserOptions;

    if(options.empty())
        return "No options provided";

    if(optionParser == nullptr)
        return "Option parser is null";

    optionError = optionParser->parseOptions(options);

    if(optionError != OptionParser::Error::OK)
    {
        result = "ERROR: " + optionParser->getErrorDescription();

        logger->systemLog(result);

        return result;
    }

    parserOptions = optionParser->getInvalidOptions();

    if(parserOptions.size() > 0)
    {
        osstring.str("");

        osstring << "Option " << parserOptions[0]->longName << " (" << parserOptions[0]->shortName << "): " << parserOptions[0]->error;

        logger->systemLog(osstring.str());
        
        return osstring.str();
    }

    parserOptions = optionParser->getOptions();
    
    if(parserOptions.empty())
        return BT_OK;

    for(OptionParser::Option *option : parserOptions)
    {
        osstring.str("");

        osstring << BT_METHOD_SET_OPTIONS << ": " << option->longName << " (" << option->shortName << ")";

        if(option->value != "")
        {
            osstring << " -> ";

            if(option->longName != BT_CLIENT_OPTION_AIR_PASSWORD && option->shortName != BT_CLIENT_OPTION_AIR_PASSWORD_SHORT &&
               option->longName != BT_CLIENT_OPTION_PK_PASSWORD && option->shortName != BT_CLIENT_OPTION_PK_PASSWORD_SHORT &&
               option->longName != BT_CLIENT_OPTION_PROXY_PASSWORD && option->shortName != BT_CLIENT_OPTION_PROXY_PASSWORD_SHORT &&
               option->longName != BT_CLIENT_OPTION_CACHE_PASSWORD && option->shortName != BT_CLIENT_OPTION_CACHE_PASSWORD_SHORT)
            {
                osstring << option->value;
            }
            else
                osstring << "************";
        }

        logger->logMethod(osstring.str());

        if(option->longName == BT_CLIENT_OPTION_SSL_DEBUG)
        {
            try
            {
                vpn_sslDebugLevel = std::stoi(option->value);
            }
            catch(std::exception &e)
            {
                vpn_sslDebugLevel = 0;
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_EPKI_CERT)
        {
            vpn_epki_cert_fn = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_EPKI_CA)
        {
            vpn_epki_ca_fn = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_EPKI_KEY)
        {
            vpn_epki_key_fn = option->value;
        }
#ifdef OPENVPN_REMOTE_OVERRIDE
        else if(option->longName == BT_CLIENT_OPTION_REMOTE_OVERRIDE)
        {
            vpn_remote_override_cmd = option->value;
        }
#endif
        else if(option->longName == BT_CLIENT_OPTION_RECOVER_NETWORK)
        {
            result = "ERROR: --";
            result += BT_CLIENT_OPTION_RECOVER_NETWORK;
            result += " option must be used alone";

            logger->systemLog(result);

            return result;
        }
        else if(option->longName == BT_CLIENT_OPTION_DISCONNECT)
        {
            result = "ERROR: --";
            result += BT_CLIENT_OPTION_DISCONNECT;
            result += " option must be used alone";

            logger->systemLog(result);

            return result;
        }
        else if(option->longName == BT_CLIENT_OPTION_PAUSE)
        {
            result = "ERROR: --";
            result += BT_CLIENT_OPTION_PAUSE;
            result += " option must be used alone";

            logger->systemLog(result);

            return result;
        }
        else if(option->longName == BT_CLIENT_OPTION_RESUME)
        {
            result = "ERROR: --";
            result += BT_CLIENT_OPTION_RESUME;
            result += " option must be used alone";

            logger->systemLog(result);

            return result;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_CONNECT)
        {
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_SERVER)
        {
            airVpnServerPattern = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_COUNTRY)
        {
            airVpnCountryPattern = AirVPNTools::toLower(option->value);
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_TLS_MODE)
        {
            if(option->value != "auto" && option->value != "auth" && option->value != "crypt")
            {
                result = "ERROR: --";
                result += BT_CLIENT_OPTION_AIR_TLS_MODE;
                result += " can be auto, auth, crypt";

                logger->systemLog(result);

                return result;
            }

            if(option->value == "auth")
                airVpnTlsMode = TLS_MODE_AUTH;
            else
                airVpnTlsMode = TLS_MODE_CRYPT;
            
            airVpnAutoQuickMode = false;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_IPV6)
        {
            if(RCParser::isValidBool(option->value) == false)
            {
                std::string opt = "--";
                opt += BT_CLIENT_OPTION_AIR_IPV6;

                result = "ERROR: " + OptionParser::allowedBoolValueMessage(opt);

                logger->systemLog(result);

                return result;
            }

            if(iPv6Enabled == false && OptionParser::isBoolEnabled(option->value) == true)
            {
                result = "ERROR: IPv6 is not available in this system";

                logger->systemLog(result);

                return result;
            }

            if(OptionParser::isBoolEnabled(option->value) == true)
                airVpnIPv6 = true;
            else
                airVpnIPv6 = false;
            
            AirVPNTools::setBootServerIPv6Mode(airVpnIPv6);
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_USER)
        {
            airVpnUsername = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_PASSWORD)
        {
            airVpnPassword = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_KEY)
        {
            airVpnKeyName = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_KEY_LIST)
        {
            airVpnKeyList = true;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_KEY_LOAD)
        {
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_SAVE)
        {
            airVpnSave = true;

            airVpnSaveFileName = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_INFO)
        {
            airVpnInfo = true;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_LIST)
        {
            airVpnList = true;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST)
        {
            std::vector<std::string> item;
            std::string server;

            airVpnClientWhiteServerList = option->value;

            item = AirVPNTools::split(airVpnClientWhiteServerList, ",");

            result = BT_OK;
            
            for(size_t i = 0; i < item.size() && result == BT_OK; ++i)
            {
                server = item[i];

                result = check_rc_value("server", RC_DIRECTIVE_AIRWHITESERVERLIST, server);
            }

            result = BT_OK;
            
            for(size_t i = 0; i < item.size() && result == BT_OK; ++i)
            {
                server = item[i];

                result = denied_rc_value("server", RC_DIRECTIVE_AIRWHITESERVERLIST, server);
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST)
        {
            airVpnClientBlackServerList = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST)
        {
            std::vector<std::string> item;
            std::string server;

            airVpnClientWhiteCountryList = option->value;

            item = AirVPNTools::split(airVpnClientWhiteCountryList, ",");

            result = BT_OK;
            
            for(size_t i = 0; i < item.size() && result == BT_OK; ++i)
            {
                server = item[i];

                result = check_rc_value("country", RC_DIRECTIVE_AIRWHITECOUNTRYLIST, server);
            }

            result = BT_OK;
            
            for(size_t i = 0; i < item.size() && result == BT_OK; ++i)
            {
                server = item[i];

                result = denied_rc_value("country", RC_DIRECTIVE_AIRWHITECOUNTRYLIST, server);
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST)
        {
            airVpnClientBlackCountryList = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_AIR_6TO4)
        {
            if(RCParser::isValidBool(option->value) == false)
            {
                std::string opt = "--";
                opt += BT_CLIENT_OPTION_AIR_6TO4;

                result = "ERROR: " + OptionParser::allowedBoolValueMessage(opt);

                logger->systemLog(result);

                return result;
            }

            if(iPv6Enabled == false && OptionParser::isBoolEnabled(option->value) == true)
            {
                result = "ERROR: IPv6 is not available in this system";

                logger->systemLog(result);

                return result;
            }

            if(OptionParser::isBoolEnabled(option->value) == true)
                airVpn6to4 = true;
            else
                airVpn6to4 = false;
        }
        else if(option->longName == BT_CLIENT_OPTION_CIPHER)
        {
            vpn_cipher_alg = option->value;

            if(VpnClient::isDataCipherSupported(option->value) == false)
            {
                oserror.str("");

                oserror << "ERROR: cipher " << option->value << " is not supported. Please use --list-data-ciphers to list supported data ciphers.";

                return oserror.str();
            }

            result = check_rc_value("cipher algorithm", RC_DIRECTIVE_CIPHER, vpn_cipher_alg);

            if(result != BT_OK)
                return result;
        }
        else if(option->longName == BT_CLIENT_OPTION_TCP_QUEUE_LIMIT)
        {
            if(rcParser->getDirective(RC_DIRECTIVE_TCPQUEUELIMIT) == nullptr)
            {
                try
                {
                    vpn_tcp_queue_limit = std::stoi(option->value);
                }
                catch(std::exception &e)
                {
                    vpn_tcp_queue_limit = TCP_QUEUE_LIMIT_DEFAULT;
                }

                if(vpn_tcp_queue_limit < 0 || vpn_tcp_queue_limit > 65535)
                {
                    result = "ERROR: --";
                    result += BT_CLIENT_OPTION_TCP_QUEUE_LIMIT;
                    result += " must be from 1 to 65535";

                    logger->systemLog(result);

                    return result;
                }
            }
            else
            {
                oserror.str("");

                oserror << "ERROR: TCP queue limit cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_NCP_DISABLE)
        {
            vpn_ncp_disable = true;

            result = check_rc_value("Disable Negotiable Crypto Parameters (NCP)", RC_DIRECTIVE_NCPDISABLE, "true");

            if(result != BT_OK)
                return result;
        }
        else if(option->longName == BT_CLIENT_OPTION_NETWORK_LOCK)
        {
            if(persistent_network_lock_mode == NetFilter::Mode::OFF)
            {
                std::string value = AirVPNTools::normalizeBoolValue(option->value, "on", "off");

                if(value == "on")
                    vpn_network_lock_mode = NetFilter::Mode::AUTO;
                else if(value == "iptables")
                    vpn_network_lock_mode = NetFilter::Mode::IPTABLES;
                else if(value == "nftables")
                    vpn_network_lock_mode = NetFilter::Mode::NFTABLES;
                else if(value == "pf")
                    vpn_network_lock_mode = NetFilter::Mode::PF;
                else if(value == "off")
                    vpn_network_lock_mode = NetFilter::Mode::OFF;
                else
                {
                    result = "ERROR: --";
                    result += BT_CLIENT_OPTION_NETWORK_LOCK;
                    result += " option must be on, iptables, nftables, pf or off";

                    logger->systemLog(result);

                    return result;
                }

                result = check_rc_value("Network Lock", RC_DIRECTIVE_NETWORKLOCKPERSIST, value);
                
                if(result == BT_OK)
                    result = check_rc_value("Network Lock", RC_DIRECTIVE_NETWORKLOCK, value);

                if(result != BT_OK)
                    return result;
            }
            else
            {
                result = "ERROR: Persistent network lock enabled in ";
                result += BLUETIT_SHORT_NAME;
                result += ". Option --network-lock cannot be used.";

                logger->systemLog(result);

                return result;
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_GUI_VERSION)
        {
            vpn_gui_version = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_IGNORE_DNS_PUSH)
        {
            vpn_ignore_dns_push = true;

            result = check_rc_value("Ignore DNS push", RC_DIRECTIVE_IGNOREDNSPUSH, "true");

            if(result != BT_OK)
                return result;
        }
        else if(option->longName == BT_CLIENT_OPTION_CACHE_PASSWORD)
        {
            vpn_cachePassword = true;
        }
        else if(option->longName == BT_CLIENT_OPTION_NO_CERT)
        {
            vpn_disableClientCert = true;
        }
        else if(option->longName == BT_CLIENT_OPTION_RESPONSE)
        {
            vpn_response = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_PROTO)
        {
            vpn_proto = option->value;

            result = check_rc_value("protocol", RC_DIRECTIVE_PROTO, vpn_proto);

            if(result != BT_OK)
            {
                return result;
            }

            airVpnAutoQuickMode = false;
        }
        else if(option->longName == BT_CLIENT_OPTION_ALLOWUAF)
        {
            vpn_allowUnusedAddrFamilies = AirVPNTools::normalizeBoolValue(option->value, "yes", "no");
        }
        else if(option->longName == BT_CLIENT_OPTION_SERVER)
        {
            vpn_server = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_PORT)
        {
            int iport;

            vpn_port = option->value;

            try
            {
                iport = std::stoi(vpn_port);
            }
            catch(std::exception &e)
            {
                result = "ERROR: illegal value for --";
                result += BT_CLIENT_OPTION_PORT;

                return result;
            }

            if(iport < 1 || iport > 65535)
            {
                result = "ERROR: --";
                result += BT_CLIENT_OPTION_PORT;
                result += " must be an integer value from 1 to 65535";

                return result;
            }

            result = check_rc_value("port", RC_DIRECTIVE_PORT, vpn_port);

            if(result != BT_OK)
            {
                return result;
            }

            airVpnAutoQuickMode = false;
        }
        else if(option->longName == BT_CLIENT_OPTION_TIMEOUT)
        {
            try
            {
                vpn_timeout = std::stoi(option->value);
            }
            catch(std::exception &e)
            {
                vpn_timeout = 0;
            }

            if(rcParser->getDirective(RC_DIRECTIVE_TIMEOUT) != nullptr)
            {
                oserror.str("");

                oserror << "ERROR: Connection timeout cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_COMPRESS)
        {
            if(rcParser->getDirective(RC_DIRECTIVE_COMPRESS) != nullptr)
            {
                oserror.str("");

                oserror << "ERROR: Compression mode cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }

            vpn_compress = AirVPNTools::normalizeBoolValue(option->value, "yes", "no");
        }
        else if(option->longName == BT_CLIENT_OPTION_PK_PASSWORD)
        {
            vpn_privateKeyPassword = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_TVM_OVERRIDE)
        {
            vpn_tlsVersionMinOverride = option->value;

            if(rcParser->getDirective(RC_DIRECTIVE_TLSVERSIONMIN) != nullptr)
            {
                oserror.str("");

                oserror << "ERROR: TLS minimun version cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_TCPROF_OVERRIDE)
        {
            vpn_tlsCertProfileOverride = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_PROXY_HOST)
        {
            vpn_proxyHost = option->value;

            if(rcParser->getDirective(RC_DIRECTIVE_PROXYHOST) != nullptr)
            {
                oserror.str("");

                oserror << "ERROR: Proxy host cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_PROXY_PORT)
        {
            vpn_proxyPort = option->value;

            if(rcParser->getDirective(RC_DIRECTIVE_PROXYPORT) != nullptr)
            {
                oserror.str("");

                oserror << "ERROR: Proxy port cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_PROXY_USERNAME)
        {
            vpn_proxyUsername = option->value;

            if(rcParser->getDirective(RC_DIRECTIVE_PROXYUSERNAME) != nullptr)
            {
                oserror.str("");

                oserror << "ERROR: Proxy user name cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_PROXY_PASSWORD)
        {
            vpn_proxyPassword = option->value;

            if(rcParser->getDirective(RC_DIRECTIVE_PROXYPASSWORD) != nullptr)
            {
                oserror.str("");

                oserror << "ERROR: Proxy password cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_PROXY_BASIC)
        {
            vpn_proxyAllowCleartextAuth = true;

            if(rcParser->getDirective(RC_DIRECTIVE_PROXYBASIC) != nullptr)
            {
                oserror.str("");

                oserror << "ERROR: Proxy HTTP basic auth cannot be set due to " << BLUETIT_SHORT_NAME << " policy.";

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_ALT_PROXY)
        {
            vpn_altProxy = true;
        }
#ifdef ENABLE_DCO

        else if(option->longName == BT_CLIENT_OPTION_DCO)
        {
            vpn_dco = true;
        }

#endif
        else if(option->longName == BT_CLIENT_OPTION_AUTO_SESS)
        {
            vpn_autologinSessions = true;
        }
        else if(option->longName == BT_CLIENT_OPTION_AUTH_RETRY)
        {
            vpn_retryOnAuthFailed = true;
        }
        else if(option->longName == BT_CLIENT_OPTION_PERSIST_TUN)
        {
            vpn_tunPersist = true;

            result = check_rc_value("persistent TUN interface", RC_DIRECTIVE_TUNPERSIST, "true");

            if(result != BT_OK)
                return result;
        }
        else if(option->longName == BT_CLIENT_OPTION_DEF_KEYDIR)
        {
            const std::string arg = option->value;

            if(arg == "bi" || arg == "bidirectional")
                vpn_defaultKeyDirection = -1;
            else if(arg == "0")
                vpn_defaultKeyDirection = 0;
            else if(arg == "1")
                vpn_defaultKeyDirection = 1;
            else
            {
                oserror.str("");

                oserror << "ERROR: --" << BT_CLIENT_OPTION_DEF_KEYDIR << " bad default key-direction: " << arg;

                logger->systemLog(oserror.str());

                return oserror.str();
            }
        }
        else if(option->longName == BT_CLIENT_OPTION_DC)
        {
            vpn_dynamicChallengeCookie = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_PEER_INFO)
        {
            vpn_peer_info = option->value;
        }
        else if(option->longName == BT_CLIENT_OPTION_GREMLIN)
        {
            vpn_gremlin = option->value;
        }
    }

    result = set_openvpn_client_config();

    return result;
}

VpnClient::EventCallback vpn_client_connected_event_callback(VpnClient::EventData eventData)
{
    AirVPNServer server = airVpnManifest->getServerByIP(eventData.connectionInfo.serverIp);
    std::vector<VpnClient::IPEntry> pushedDns;

    osstring.str("");

    if(server.getName().empty() == false)
    {
        osstring << "Connected to AirVPN server " << dbusConnector->stringToLocale(server.getName());
        
        if(server.getLocation() != "")
            osstring << ", " << server.getLocation();

        if(server.getCountryName() != "")
            osstring << " (" << server.getCountryName() << ")";
    }
    else
        osstring << "Connected to VPN server with";
    
    osstring << " IP address " << eventData.connectionInfo.serverIp;

    osstring << " - Port " << eventData.connectionInfo.serverPort;

    osstring << " - Protocol " << eventData.connectionInfo.serverProto;

    osstring << " - Cipher " << eventData.connectionInfo.cipher;

    OPENVPN_LOG(osstring.str());
    
    if(vpnClient != nullptr)
    {
        pushedDns = vpnClient->getPushedDns();

        if(pushedDns.size() > 0)
        {
            osstring.str("");

            osstring << "Pushed DNS:";

            for(VpnClient::IPEntry dns : pushedDns)
            {
                osstring << " " << dns.address << " (";

                if(dns.ipv6)
                    osstring << "IPv6";
                else
                    osstring << "IPv4";

                osstring << ")";
            }

            OPENVPN_LOG(osstring.str());
        }
    }
}

int bluetit_status(void)
{
    DIR* resdir;
    bool dirtyExit = false;

    status = BLUETIT_STATUS_READY;

    // Check for resource directory

    resdir = opendir(BLUETIT_RESOURCE_DIRECTORY);

    if(resdir)
    {
        closedir(resdir);
    }
    else if(errno == ENOENT)
    {
        osstring.str("");

        osstring << "Creating resource directory " << BLUETIT_RESOURCE_DIRECTORY;

        logger->systemLog(osstring.str());

        if(mkdir(BLUETIT_RESOURCE_DIRECTORY, 0755) != 0)
        {
            osstring.str("");

            osstring << "Cannot create " << BLUETIT_RESOURCE_DIRECTORY << " (Error " << errno << " - " << strerror(errno);

            logger->systemLog(osstring.str());

            status = BLUETIT_STATUS_RESOURCE_DIRECTORY_ERROR;
        }
    }
    else
    {
        osstring.str("");

        osstring << "Cannot access resource directory" << BLUETIT_RESOURCE_DIRECTORY;

        logger->systemLog(osstring.str());

        status = BLUETIT_STATUS_RESOURCE_DIRECTORY_ERROR;
    }

    if(netFilter != nullptr)
    {
        if(netFilter->isNetworkLockEnabled() == false && netFilter->systemBackupExists() == true)
            dirtyExit = true;
        
        if(access(RESOLVDOTCONF_BACKUP, F_OK) == 0)
            dirtyExit = true;
        
        if(access(SYSTEM_DNS_BACKUP_FILE, F_OK) == 0)
            dirtyExit = true;
    }
    else
    {
        logger->systemLog("NetFilter is null");

        status = BLUETIT_STATUS_INIT_ERROR;
    }

#if defined(OPENVPN_PLATFORM_LINUX)

    if(dnsManager != nullptr)
    {
        if(dnsManager->resolvDotConfBackupExists())
            dirtyExit = true;
    }
    else
    {
        logger->systemLog("Cannot create DNSManager object. Exiting.");

        status = BLUETIT_STATUS_INIT_ERROR;
    }

#endif

    if(dirtyExit == true)
    {
        osstring.str("");

        osstring << BLUETIT_SHORT_NAME << " did not exit gracefully on its last run or has been killed.";

        logger->systemLog(osstring.str());

        osstring.str("");

        osstring << "Run recover network procedure or restore system settings saved in " << BLUETIT_RESOURCE_DIRECTORY;

        logger->systemLog(osstring.str());

        status = BLUETIT_STATUS_DIRTY_EXIT;
    }

    if(access(BLUETIT_LOCK_FILE, F_OK) != 0)
    {
        osstring.str("");

        osstring << "Lock file " << BLUETIT_LOCK_FILE << " not found. System has been probably tampered.";

        logger->systemLog(osstring.str());

        status = BLUETIT_STATUS_LOCK_ERROR;
    }

    return status;
}

std::string bluetit_status_description()
{
    std::string msg;

    switch(status)
    {
        case BLUETIT_STATUS_READY:
        {
            msg = BLUETIT_SHORT_NAME;
            msg += " is ready";
        }
        break;

        case BLUETIT_STATUS_CONNECTED:
        {
            msg = BLUETIT_SHORT_NAME;
            msg += " is connected to VPN";
        }
        break;

        case BLUETIT_STATUS_PAUSED:
        {
            msg = BLUETIT_SHORT_NAME;
            msg += ": VPN connection is paused.";
        }
        break;

        case BLUETIT_STATUS_DIRTY_EXIT:
        {
            msg = "It seems ";
            msg += BLUETIT_SHORT_NAME;
            msg += " did not exit gracefully or has been killed.\n";
            msg += "Your system may not be working properly and your network connection may not work\n";
            msg += "as expected. To recover your network settings, run this program again and use\n";
            msg += "the \"--recover-network\" option.";
        }
        break;

        case BLUETIT_STATUS_RESOURCE_DIRECTORY_ERROR:
        case BLUETIT_STATUS_INIT_ERROR:
        case BLUETIT_STATUS_LOCK_ERROR:
        {
            msg = BLUETIT_SHORT_NAME;
            msg += " error. Please see system log for details.";
        }
        break;

        case BLUETIT_STATUS_UNKNOWN:
        {
            msg = "Unknown ";
            msg += BLUETIT_SHORT_NAME;
            msg += " status. Please see system log for details.";
        }
        break;

        default:
        {
            msg = "Unknown status";
        }
        break;
    }
    
    return msg;
}

void cleanup_and_exit(int exit_code)
{
    if(status == BLUETIT_STATUS_CONNECTED)
    {
        stop_openvpn_connection();

        terminate_client_session();
    }

    if(airVpnManifestUpdaterThread != nullptr)
    {
        if(airVpnManifestUpdaterThread->joinable())
        {
            if(manifestSemaphore->isForbidden())
                manifestSemaphore->abortPendingJob();

            try
            {
                manifestUpdaterSignalExit.set_value();
            }
            catch(std::future_error& e)
            {
                osstring.str("");
                
                osstring << "ERROR: Manifest Updater thread (" << e.code() << "): " << e.what()
;
                OPENVPN_LOG(osstring.str());
            }

            if(airVpnManifestUpdaterThread->joinable())
                airVpnManifestUpdaterThread->join();

            delete airVpnManifestUpdaterThread;

            airVpnManifestUpdaterThread = nullptr;
        }
    }

    airvpn_user_logout();

    if(netFilter)
    {
        if(netFilter->isNetworkLockEnabled())
        {
            persistent_network_lock_mode = NetFilter::Mode::OFF;

            disable_network_lock();
        }
        
        delete netFilter;
    }

    if(access(BLUETIT_LOCK_FILE, F_OK) == 0)
        unlink(BLUETIT_LOCK_FILE);

    // remove system dns backup file

    if(access(SYSTEM_DNS_BACKUP_FILE, F_OK) != -1)
        unlink(SYSTEM_DNS_BACKUP_FILE);

    if(localNetwork)
        delete localNetwork;

    if(logger)
        delete logger;

    if(manifestSemaphore)
        delete manifestSemaphore;

    if(vpnClient)
        delete vpnClient;

    if(clientConfig)
        delete clientConfig;

    if(dbusConnector)
        delete dbusConnector;

    if(airVpnManifest)
        delete airVpnManifest;

    if(rcParser)
        delete rcParser;
    
    if(optionParser)
        delete optionParser;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(dnsManager != nullptr)
        delete dnsManager;

#endif
    
    exit(exit_code);
}

std::string recover_network()
{
    VpnClient *client = nullptr;

    std::string msg = "";

    if(status == BLUETIT_STATUS_READY || status == BLUETIT_STATUS_DIRTY_EXIT)
    {
        if(status == BLUETIT_STATUS_DIRTY_EXIT)
        {
            try
            {
                client = new VpnClient(NetFilter::Mode::OFF, BLUETIT_RESOURCE_DIRECTORY, SYSTEM_DNS_BACKUP_FILE, RESOLVDOTCONF_BACKUP);

                if(client != nullptr)
                {
                    if(client->restoreNetworkSettings())
                    {
                        msg = "Successfully restored DNS and network filter settings";

                        status = BLUETIT_STATUS_READY;
                    }
                    else
                        msg = "Failed to restore DNS and network filter settings. Please see syslog for details.";

                    delete client;

                    client = nullptr;
                }
                else
                {
                    logger->systemLog("Cannot create VpnClient object. Exiting.");

                    cleanup_and_exit(EXIT_FAILURE);
                }
            }
            catch(NetFilterException &e)
            {
                osstring.str("");

                osstring << BT_METHOD_RECOVER_NETWORK << "ERROR: " << e.what();

                logger->systemLog(osstring.str());

                msg = "Failed to restore DNS and network filter settings. Please see syslog for details.";

                if(client)
                    delete client;
            }

            if(persistent_network_lock_mode != NetFilter::Mode::OFF)
            {
                logger->systemLog("Enabling persistent network filter and lock");
                
                manifestSemaphore->forbid("Persistent network filter and lock enabled");

                enable_network_lock(NETLOCKMODE_PERSISTENT);

                logger->flushLog();
            }
            else
                manifestSemaphore->permit();
        }
        else
            msg = "System does not need a network recovery";
    }
    else
    {
        msg = "Operation refused. ";
        msg += BLUETIT_SHORT_NAME;
        msg += " is not accepting commands now.";
    }

    return msg;
}

void airvpn_server_info(const std::string &name, DBusResponse &response)
{
    std::string result, value;
    std::vector<int> cipher;
    int code;

    response.clear();

    if(name.empty())
        return;

    AirVPNServer airVpnServer = airVpnManifest->getServerByName(name);

    if(airVpnServer.getName() != "")
    {
        response.setResponse(BT_OK);

        DBusResponse::Item item;

        response.addToItem(item, "name", airVpnServer.getName());
        response.addToItem(item, "country_code", airVpnServer.getCountryCode());
        response.addToItem(item, "country", airVpnServer.getCountryName());
        response.addToItem(item, "location", airVpnServer.getLocation());
        response.addToItem(item, "bandwidth", std::to_string(airVpnServer.getBandWidth()));
        response.addToItem(item, "effective_bandwidth", std::to_string(airVpnServer.getEffectiveBandWidth()));
        response.addToItem(item, "max_bandwidth", std::to_string(airVpnServer.getMaxBandWidth()));
        response.addToItem(item, "users", std::to_string(airVpnServer.getUsers()));
        response.addToItem(item, "supports_ipv4", (airVpnServer.getSupportIPv4() ? "yes" : "no"));
        response.addToItem(item, "supports_ipv6", (airVpnServer.getSupportIPv6() ? "yes" : "no"));

        if(airVpnServer.getWarningOpen() != "")
            response.addToItem(item, "open_status", airVpnServer.getWarningOpen());

        if(airVpnServer.getWarningClosed() != "")
            response.addToItem(item, "close_status", airVpnServer.getWarningClosed());

        response.addToItem(item, "load", std::to_string(airVpnServer.getLoad()));

        cipher = airVpnServer.getTlsCiphers();

        if(cipher.size() > 0)
        {
            value = "";

            for(code = 0; code < cipher.size(); code ++)
            {
                if(code > 0)
                    value += ":";

                value += airVpnManifest->getTlsCipherDescription(cipher[code]);
            }

            response.addToItem(item, "tls_ciphers", value);
        }

        cipher = airVpnServer.getTlsSuiteCiphers();

        if(cipher.size() > 0)
        {
            value = "";

            for(code = 0; code < cipher.size(); code ++)
            {
                if(code > 0)
                    value += ":";

                value += airVpnManifest->getTlsSuiteCipherDescription(cipher[code]);
            }

            response.addToItem(item, "tls_suite_ciphers", value);
        }

        cipher = airVpnServer.getDataCiphers();

        if(cipher.size() > 0)
        {
            value = "";

            for(code = 0; code < cipher.size(); code ++)
            {
                if(code > 0)
                    value += ":";

                value += airVpnManifest->getDataCipherDescription(cipher[code]);
            }

            response.addToItem(item, "data_ciphers", value);
        }

        response.addToItem(item, "score", std::to_string(airVpnServer.getScore()));

        response.addToItem(item, "available", (airVpnServer.isAvailable() ? "yes" : "no"));

        response.add(item);
    }
    else
    {
        result = name;
        result += " - AirVPN server not found";

        response.setResponse(result);
    }
}

void airvpn_server_list(const std::string &pattern, DBusResponse &response)
{
    std::string result, value;
    std::vector<int> cipher;
    std::vector<std::string> serverList;
    int code;

    response.clear();

    if(pattern.empty())
        return;

    serverList = airVpnManifest->searchServer(pattern);

    if(serverList.size() > 0)
    {
        std::sort(serverList.begin(), serverList.end());

        response.setResponse(BT_OK);

        for(std::string name : serverList)
        {
            AirVPNServer airVpnServer = airVpnManifest->getServerByName(name);

            if(airVpnServer.getName() != "")
            {
                DBusResponse::Item item;

                response.addToItem(item, "name", airVpnServer.getName());
                response.addToItem(item, "country_code", airVpnServer.getCountryCode());
                response.addToItem(item, "country", airVpnServer.getCountryName());
                response.addToItem(item, "location", airVpnServer.getLocation());
                response.addToItem(item, "bandwidth", std::to_string(airVpnServer.getBandWidth()));
                response.addToItem(item, "effective_bandwidth", std::to_string(airVpnServer.getEffectiveBandWidth()));
                response.addToItem(item, "max_bandwidth", std::to_string(airVpnServer.getMaxBandWidth()));
                response.addToItem(item, "users", std::to_string(airVpnServer.getUsers()));
                response.addToItem(item, "supports_ipv4", (airVpnServer.getSupportIPv4() ? "yes" : "no"));
                response.addToItem(item, "supports_ipv6", (airVpnServer.getSupportIPv6() ? "yes" : "no"));

                if(airVpnServer.getWarningOpen() != "")
                    response.addToItem(item, "open_status", airVpnServer.getWarningOpen());

                if(airVpnServer.getWarningClosed() != "")
                    response.addToItem(item, "close_status", airVpnServer.getWarningClosed());

                response.addToItem(item, "load", std::to_string(airVpnServer.getLoad()));

                cipher = airVpnServer.getTlsCiphers();

                if(cipher.size() > 0)
                {
                    value = "";

                    for(code = 0; code < cipher.size(); code ++)
                    {
                        if(code > 0)
                            value += ":";

                        value += airVpnManifest->getTlsCipherDescription(cipher[code]);
                    }

                    response.addToItem(item, "tls_ciphers", value);
                }

                cipher = airVpnServer.getTlsSuiteCiphers();

                if(cipher.size() > 0)
                {
                    value = "";

                    for(code = 0; code < cipher.size(); code ++)
                    {
                        if(code > 0)
                            value += ":";

                        value += airVpnManifest->getTlsSuiteCipherDescription(cipher[code]);
                    }

                    response.addToItem(item, "tls_suite_ciphers", value);
                }

                cipher = airVpnServer.getDataCiphers();

                if(cipher.size() > 0)
                {
                    value = "";

                    for(code = 0; code < cipher.size(); code ++)
                    {
                        if(code > 0)
                            value += ":";

                        value += airVpnManifest->getDataCipherDescription(cipher[code]);
                    }

                    response.addToItem(item, "data_ciphers", value);
                }

                response.addToItem(item, "score", std::to_string(airVpnServer.getScore()));

                response.addToItem(item, "available", (airVpnServer.isAvailable() ? "yes" : "no"));

                response.add(item);
            }
        }
    }
    else
    {
        result = pattern;
        result += " - No AirVPN server found";

        response.setResponse(result);
    }
}

void airvpn_country_info(const std::string &name, DBusResponse &response)
{
    std::string result, code;

    response.clear();

    if(name.empty())
        return;

    if(CountryContinent::getCountryName(name) != "")
        code = name;
    else
        code = CountryContinent::getCountryCode(name);

    AirVPNManifest::CountryStats countryStats = airVpnManifest->getCountryStats(code);

    if(countryStats.countryISOCode != "")
    {
        response.setResponse(BT_OK);

        DBusResponse::Item item;

        response.addToItem(item, "country_iso_code", countryStats.countryISOCode);
        response.addToItem(item, "country_name", CountryContinent::getCountryName(countryStats.countryISOCode));
        response.addToItem(item, "servers", std::to_string(countryStats.servers));
        response.addToItem(item, "users", std::to_string(countryStats.users));
        response.addToItem(item, "bandwidth", std::to_string(countryStats.bandWidth));
        response.addToItem(item, "max_bandwidth", std::to_string(countryStats.maxBandWidth));

        response.add(item);
    }
    else
    {
        result = name;
        result += " - AirVPN country not found";

        response.setResponse(result);
    }
}

void airvpn_country_list(const std::string &pattern, DBusResponse &response)
{
    std::string result, code;
    std::vector<std::string> countryList;

    response.clear();

    if(pattern.empty())
        return;

    countryList = CountryContinent::searchCountry(pattern);

    if(countryList.size() > 0)
    {
        std::sort(countryList.begin(), countryList.end());

        response.setResponse(BT_OK);

        for(std::string code : countryList)
        {
            AirVPNManifest::CountryStats countryStats = airVpnManifest->getCountryStats(code);

            if(countryStats.countryISOCode != "")
            {
                DBusResponse::Item item;

                response.addToItem(item, "country_iso_code", countryStats.countryISOCode);
                response.addToItem(item, "country_name", CountryContinent::getCountryName(countryStats.countryISOCode));
                response.addToItem(item, "servers", std::to_string(countryStats.servers));
                response.addToItem(item, "users", std::to_string(countryStats.users));
                response.addToItem(item, "bandwidth", std::to_string(countryStats.bandWidth));
                response.addToItem(item, "max_bandwidth", std::to_string(countryStats.maxBandWidth));

                response.add(item);
            }
        }
    }
    else
    {
        result = pattern;
        result += " - No AirVPN country found";

        response.setResponse(result);
    }
}

void airvpn_key_list(const std::string &username, const std::string &password, DBusResponse &response)
{
    std::string result;
    std::vector<std::string> profiles;
    AirVPNUser *user = nullptr;

    profiles.clear();

    user = new AirVPNUser(username, password);
    
    if(user == nullptr)
    {
        logger->systemLog("Cannot create AirVPNUser object. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(user->getUserProfileType() == AirVPNUser::UserProfileType::NOT_SET)
    {
        result = "Login error";
        
        if(!user->getUserProfileErrorDescription().empty())
        {
            result += ": ";
            result += user->getUserProfileErrorDescription();
        }

        response.setResponse(result);
        
        delete user;

        return;
    }

    profiles = user->getUserKeyNames();

    if(profiles.size() > 0)
    {
        std::sort(profiles.begin(), profiles.end());

        response.setResponse(BT_OK);

        for(std::string name : profiles)
        {
            DBusResponse::Item item;

            response.addToItem(item, "key", name);

            response.add(item);
        }
    }
    else
    {
        result = "User " + username + " has no defined keys";

        response.setResponse(result);
    }

    delete user;
}

void airvpn_key_save(const std::string &username, const std::string &password, DBusResponse &response)
{
    std::string result;
    std::ostringstream profile;
    AirVPNUser *user = nullptr;
    AirVPNUser::UserKey userKey;

    user = new AirVPNUser(username, password);
    
    if(user == nullptr)
    {
        logger->systemLog("Cannot create AirVPNUser object. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(user->getUserProfileType() == AirVPNUser::UserProfileType::NOT_SET)
    {
        result = "Login error";
        
        if(!user->getUserProfileErrorDescription().empty())
        {
            result += ": ";
            result += user->getUserProfileErrorDescription();
        }

        response.setResponse(result);
        
        delete user;

        return;
    }

    userKey = user->getUserKey(airVpnKeyName);

    if(userKey.name == "")
    {
        result = "ERROR: Key \"" + dbusConnector->stringToLocale(airVpnKeyName) + "\" does not exist for user " + dbusConnector->stringToLocale(user->getUserName());

        response.setResponse(result);
        
        delete user;

        return;
    }

    profile.str("");
    profile << "#" << std::endl;
    profile << "# " << airVpnUsername << ", key " <<  airVpnKeyName << std::endl;
    profile << "#" << std::endl << std::endl;
    
    profile << "<ca>" << std::endl << user->getCertificateAuthorityCertificate() << std::endl << "</ca>" << std::endl;

    profile << "<cert>" << std::endl << userKey.certificate << std::endl << "</cert>" << std::endl;

    profile << "<key>" << std::endl << userKey.privateKey << std::endl << "</key>" << std::endl;

    if(airVpnTlsMode == "tls-auth")
        profile << "<tls-auth>" << std::endl << user->getTlsAuthKey() << std::endl << "</tls-auth>" << std::endl;
    else
        profile << "<tls-crypt>" << std::endl << user->getTlsCryptKey() << std::endl << "</tls-crypt>" << std::endl;
    
    response.setResponse(BT_OK);

    DBusResponse::Item item;

    response.addToItem(item, "type", "key");
    response.addToItem(item, "user", username);
    response.addToItem(item, "name", airVpnKeyName);
    response.addToItem(item, "file_name", airVpnSaveFileName);
    response.addToItem(item, "content", profile.str());

    response.add(item);

    delete user;
}

void airvpn_server_save(const std::string &username, const std::string &password, bool create_country, DBusResponse &response)
{
    std::string result, serverProfile;
    std::ostringstream profile;

    airvpn_user_logout();

    airVpnUser = new AirVPNUser(username, password);

    if(airVpnUser == nullptr)
    {
        logger->systemLog("Cannot create AirVPNUser object. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(airVpnUser->getUserProfileType() == AirVPNUser::UserProfileType::NOT_SET)
    {
        result = "Login error";
        
        if(!airVpnUser->getUserProfileErrorDescription().empty())
        {
            result += ": ";
            result += airVpnUser->getUserProfileErrorDescription();
        }

        response.setResponse(result);
        
        delete airVpnUser;

        airVpnUser = nullptr;

        return;
    }

    if(airVpnKeyName.empty())
        airVpnKeyName = airVpnUser->getFirstProfileName();
    else
    {
        if(airVpnUser->getUserKey(airVpnKeyName).name == "")
        {
            result = "ERROR: Key \"" + dbusConnector->stringToLocale(airVpnKeyName) + "\" does not exist for user " + dbusConnector->stringToLocale(airVpnUser->getUserName());

            response.setResponse(result);
            
            delete airVpnUser;

            airVpnUser = nullptr;

            return;
        }
    }

    if(airVpnKeyName.empty())
    {
        result = "ERROR: User " + dbusConnector->stringToLocale(airVpnUser->getUserName()) + " has no defined keys";

        response.setResponse(result);
        
        delete airVpnUser;

        airVpnUser = nullptr;

        return;
    }

    serverProfile = airvpn_create_profile(create_country, 0);
    
    if(serverProfile.empty())
    {
        if(create_country == true)
        {
            result = "AirVPN country ";
            result += airVpnCountryPattern;
        }
        else
        {
            result = "AirVPN server ";
            result += airVpnServerPattern;
        }
        
        result += " does not exist";

        response.setResponse(result);
        
        delete airVpnUser;
        
        airVpnUser = nullptr;

        return;
    }

    profile.str("");
    profile << "#" << std::endl;
    profile << "# " << airVpnUsername << ", ";
    
    if(customAirVpnKey.empty())
        profile << "key " <<  airVpnKeyName;
    else
        profile << "custom key";

    profile << std::endl;

    profile << "#" << std::endl;
    
    if(create_country == true)
        profile << "# OpenVPN profile for AirVPN country " << airVpnCountryPattern << std::endl;
    else
        profile << "# OpenVPN profile for AirVPN server " << airVpnServerPattern << std::endl;

    profile << "#" << std::endl << std::endl;
    
    profile << serverProfile << std::endl;
    
    response.setResponse(BT_OK);

    DBusResponse::Item item;

    if(create_country == true)
        response.addToItem(item, "type", "profile for country");
    else
        response.addToItem(item, "type", "profile for server");

    response.addToItem(item, "user", username);

    if(create_country == true)
        response.addToItem(item, "name", airVpnCountryPattern);
    else
        response.addToItem(item, "name", airVpnServerPattern);
        
    response.addToItem(item, "file_name", airVpnSaveFileName);
    response.addToItem(item, "content", profile.str());

    response.add(item);

    delete airVpnUser;
    
    airVpnUser = nullptr;
}

void airvpn_manifest_updater(int interval_minutes, const std::future<void> &future)
{
    int loop_counter, loop_time;
    AirVPNManifest::Error manifestError;

    logger->systemLog("AirVPN Manifest updater thread started");

    osstring.str("");

    osstring << "AirVPN Manifest update interval is " << interval_minutes << " minute";

    if(interval_minutes != 1)
        osstring << "s";

    logger->systemLog(osstring.str());

    loop_time = interval_minutes * 60;
    loop_counter = loop_time;

    do
    {
        if(loop_counter >= loop_time)
        {
            if(manifestSemaphore->isForbidden())
            {
                osstring.str("");

                osstring << "AirVPN Manifest update suspended: " << manifestSemaphore->getForbidReason();

                logger->systemLog(osstring.str());

                manifestSemaphore->wait();
            }

            if(manifestSemaphore->isPermitted())
            {
                logger->systemLog("Updating AirVPN Manifest");

                manifestError = airVpnManifest->loadManifest();

                if(manifestError == AirVPNManifest::Error::OK)
                {
                    osstring.str("");

                    osstring << "AirVPN Manifest successfully retrieved from ";

                    switch(airVpnManifest->getManifestType())
                    {
                        case AirVPNManifest::Type::STORED:
                        {
                            osstring << "local instance";
                        }
                        break;

                        case AirVPNManifest::Type::FROM_SERVER:
                        {
                            osstring << "server";
                        }
                        break;

                        default:
                        {
                            osstring << "unknown source";
                        }
                        break;
                    }

                    logger->systemLog(osstring.str());
                }
                else
                {
                    logger->systemLog(airVpnManifest->getErrorDescription());
                        logger->systemLog(osstring.str());
                }

                if(CountryContinent::getCountries() == 0)
                {
                    osstring.str("");

                    osstring << "WARNING: Country database is empty. Check " << BLUETIT_SHORT_NAME << " configuration files in " << BLUETIT_RESOURCE_DIRECTORY;

                    logger->systemLog(osstring.str());
                }

                if(CountryContinent::getContinents() == 0)
                {
                    osstring.str("");

                    osstring << "WARNING: Continent database is empty. Check " << BLUETIT_SHORT_NAME << " configuration files in " << BLUETIT_RESOURCE_DIRECTORY;

                    logger->systemLog(osstring.str());
                }

                loop_counter = 0;
            }
        }

        loop_counter += MANIFEST_THREAD_SLEEP_SECONDS;
    }
    while(future.wait_for(std::chrono::seconds(MANIFEST_THREAD_SLEEP_SECONDS)) == std::future_status::timeout);

    logger->systemLog("AirVPN Manifest updater thread finished");
}

bool airvpn_user_login()
{
    AirVPNUser::UserLocationStatus locationStatus = AirVPNUser::UserLocationStatus::UNKNOWN;
    std::map<std::string, std::string>::iterator it;
    std::string locationMessageError = "";
    double d;

    if(airVpnUsername.empty() || airVpnPassword.empty())
    {
        logger->systemLog("Invalid AirVPN user credentials");

        airVpnUserValid = false;

        return false;
    }

    airvpn_user_logout();

    OPENVPN_LOG("Logging in AirVPN user " + dbusConnector->stringToLocale(airVpnUsername));

    airVpnUser = new AirVPNUser(airVpnUsername, airVpnPassword);

    if(airVpnUser == nullptr)
    {
        logger->systemLog("Cannot create AirVPNUser object. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(airVpnUser->getUserProfileError() != AirVPNUser::Error::OK)
    {
        logger->systemLog("AirVPN login error: " + airVpnUser->getUserProfileErrorDescription());

        delete airVpnUser;
        
        airVpnUser = nullptr;
        
        return false;
    }

    if(bluetitCountry != "")
    {
        airVpnUser->setUserCountry(bluetitCountry);
        
        if(bluetitLocation.size() > 0)
        {
            it = bluetitLocation.find("status");

            if(it->second == "OK")
            {
                it = bluetitLocation.find("ip");
                airVpnUser->setUserIP(it->second);

                it = bluetitLocation.find("latitude");
                
                try
                {
                    d = std::stod(it->second);
                }
                catch(std::exception &e)
                {
                    d = 0.0;
                }

                airVpnUser->setUserLatitude(d);

                it = bluetitLocation.find("longitude");

                try
                {
                    d = std::stod(it->second);
                }
                catch(std::exception &e)
                {
                    d = 0.0;
                }

                airVpnUser->setUserLongitude(d);

                locationStatus = AirVPNUser::UserLocationStatus::SET;

                locationMessageError = "";
            }
            else
            {
                locationStatus = AirVPNUser::UserLocationStatus::UNKNOWN;

                locationMessageError = "Please use \"country\" directive in ";
                locationMessageError += BLUETIT_RC_FILE;
            }
        }
    }
    else
    {
        if(bluetitLocation.size() > 0)
        {
            it = bluetitLocation.find("status");

            if(it->second == "OK")
            {
                it = bluetitLocation.find("country");
                airVpnUser->setUserCountry(it->second);

                it = bluetitLocation.find("ip");
                airVpnUser->setUserIP(it->second);

                it = bluetitLocation.find("latitude");

                try
                {
                    d = std::stod(it->second);
                }
                catch(std::exception &e)
                {
                    d = 0.0;
                }

                airVpnUser->setUserLatitude(d);

                it = bluetitLocation.find("longitude");

                try
                {
                    d = std::stod(it->second);
                }
                catch(std::exception &e)
                {
                    d = 0.0;
                }

                airVpnUser->setUserLongitude(d);
                
                locationStatus = AirVPNUser::UserLocationStatus::SET;
            }
            else
            {
                locationStatus = AirVPNUser::UserLocationStatus::UNKNOWN;

                locationMessageError = "Please use \"country\" directive in ";
                locationMessageError += BLUETIT_RC_FILE;
            }
        }
        else
        {
            if(netFilter->isNetworkLockEnabled() == false)
            {
                OPENVPN_LOG("Requesting user IP and country to AirVPN ipleak.net via secure connection");

                locationStatus = airVpnUser->detectUserLocation();
                
                locationMessageError = airVpnUser->getUserLocationStatusError();
            }
            else
            {
                osstring.str("");
                
                osstring << "WARNING: Cannot determine system country via AirVPN's ipleak.net: Network Lock enabled. Please use \"country\" directive in " << BLUETIT_RC_FILE;

                OPENVPN_LOG(osstring.str());
            }
        }

        if(locationStatus == AirVPNUser::UserLocationStatus::SET)
        {
            OPENVPN_LOG("User IP: " + airVpnUser->getUserIP());

            OPENVPN_LOG("User country: " + dbusConnector->stringToLocale(airVpnUser->getUserCountry()));
        }
        else
        {
            osstring.str("");

            osstring << "ERROR: Cannot detect user location: " << locationMessageError;

            OPENVPN_LOG(osstring.str());
        }
    }

    OPENVPN_LOG("AirVPN user " + dbusConnector->stringToLocale(airVpnUser->getUserName()) + " successfully logged in");
    
    return true;
}

void airvpn_user_logout()
{
    if(airVpnUser != nullptr)
    {
        OPENVPN_LOG("Logging out AirVPN user " + dbusConnector->stringToLocale(airVpnUser->getUserName()));

        delete airVpnUser;

        airVpnUser = nullptr;

        airVpnUserValid = false;
    }
}

void terminate_client_session()
{
    if(status == BLUETIT_STATUS_CONNECTED)
        stop_openvpn_connection();

    airvpn_user_logout();

    if(netFilter != nullptr)
    {
        if(netFilter->isNetworkLockEnabled())
            disable_network_lock();
    }

    send_event(BT_EVENT_END_OF_SESSION, "");

    logger->flushLog();
}

void send_event(const std::string &event, const std::string &payload)
{
    if(dbusConnector == nullptr)
        return;

    dbusItems.clear();

    dbusItems.push_back(event);
    dbusItems.push_back(payload);

    dbusConnector->callMethod(BT_CLIENT_BUS_NAME, BT_CLIENT_OBJECT_PATH_NAME, BT_METHOD_EVENT, dbusItems);
}

std::string airvpn_create_profile(bool create_country_profile, int ipEntryOffset)
{
    int entry, port;
    std::string serverIp, serverProfile, countryCode;
    std::map<int, std::string> serverEntryIP;

    if(airVpnUser == nullptr)
        return "";

    if(create_country_profile == false)
    {
        AirVPNServer airVPNServer = airVpnManifest->getServerByName(airVpnServerPattern);
        
        if(airVPNServer.getName().empty())
            return "";

        if(airVpnIPv6 == true)
            serverEntryIP = airVPNServer.getEntryIPv6();
        else
            serverEntryIP = airVPNServer.getEntryIPv4();

        if(airVpnTlsMode == TLS_MODE_AUTH)
            entry = 0;
        else
            entry = 2;

        entry += ipEntryOffset;

        std::map<int, std::string>::iterator it = serverEntryIP.find(entry);

        if(it != serverEntryIP.end())
            serverIp = it->second;
        else
            return "";
    }
    else
    {
        if(AirVPNTools::iequals(airVpnCountryPattern, "earth") ||
           AirVPNTools::iequals(airVpnCountryPattern, "europe") ||
           AirVPNTools::iequals(airVpnCountryPattern, "africa") ||
           AirVPNTools::iequals(airVpnCountryPattern, "asia") ||
           AirVPNTools::iequals(airVpnCountryPattern, "america") ||
           AirVPNTools::iequals(airVpnCountryPattern, "oceania"))
        {
            countryCode = airVpnCountryPattern;
        }
        else if(CountryContinent::getCountryName(airVpnCountryPattern) != "")
        {
            countryCode = airVpnCountryPattern;
            airVpnCountryPattern = CountryContinent::getCountryName(airVpnCountryPattern);
        }
        else
            countryCode = CountryContinent::getCountryCode(airVpnCountryPattern);

        if(countryCode == "")
            return "";
        
        serverIp = countryCode;
    }

    if(vpn_cipher_alg.empty())
        vpn_cipher_alg = "SERVER";

    try
    {
        port = std::stoi(vpn_port);
    }
    catch(std::exception &e)
    {
        return "";
    }

    serverProfile = airVpnUser->getOpenVPNProfile(airVpnKeyName, serverIp, port, vpn_proto, airVpnTlsMode, vpn_cipher_alg, airVpnIPv6, airVpn6to4, create_country_profile, countryCode, customAirVpnKey);
    
    return serverProfile;
}

bool setup_rc_directives()
{
    bool result;

    if(rcParser == nullptr)
        return false;

    result = rcParser->addConfigDirective(RC_DIRECTIVE_BOOTSERVER, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_RSAEXPONENT, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_RSAMODULUS, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_MANIFESTUPDATEINTERVAL, RCParser::Type::INTEGER);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRCONNECTATBOOT, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_NETWORKLOCKPERSIST, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRUSERNAME, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRPASSWORD, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRKEY, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRSERVER, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRCOUNTRY, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRPROTO, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRPORT, RCParser::Type::INTEGER);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRCIPHER, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRIPV6, RCParser::Type::BOOL);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIR6TO4, RCParser::Type::BOOL);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRWHITESERVERLIST, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRBLACKSERVERLIST, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRWHITECOUNTRYLIST, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_AIRBLACKCOUNTRYLIST, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_FORBIDQUICKHOMECOUNTRY, RCParser::Type::BOOL);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_COUNTRY, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_REMOTE, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_PROTO, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_PORT, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_TUNPERSIST, RCParser::Type::BOOL);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_CIPHER, RCParser::Type::LIST);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_MAXCONNECTIONRETRIES, RCParser::Type::INTEGER);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_TCPQUEUELIMIT, RCParser::Type::INTEGER);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_NCPDISABLE, RCParser::Type::BOOL);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_NETWORKLOCK, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_IGNOREDNSPUSH, RCParser::Type::BOOL);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_TIMEOUT, RCParser::Type::INTEGER);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_COMPRESS, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_TLSVERSIONMIN, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_PROXYHOST, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_PROXYPORT, RCParser::Type::INTEGER);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_PROXYUSERNAME, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_PROXYPASSWORD, RCParser::Type::STRING);
    result &= rcParser->addConfigDirective(RC_DIRECTIVE_PROXYBASIC, RCParser::Type::BOOL);

    return result;
}

bool setup_options()
{
    bool result;

    if(optionParser == nullptr)
        return false;

    result = optionParser->addConfigOption(BT_CLIENT_OPTION_HELP_SHORT, BT_CLIENT_OPTION_HELP, OptionParser::Type::OPTION, BT_CLIENT_OPTION_HELP_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_VERSION_SHORT, BT_CLIENT_OPTION_VERSION, OptionParser::Type::OPTION, BT_CLIENT_OPTION_VERSION_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_BLUETIT_STATUS_SHORT, BT_CLIENT_OPTION_BLUETIT_STATUS, OptionParser::Type::OPTION, BT_CLIENT_OPTION_BLUETIT_STATUS_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_BLUETIT_STATS_SHORT, BT_CLIENT_OPTION_BLUETIT_STATS, OptionParser::Type::OPTION, BT_CLIENT_OPTION_BLUETIT_STATS_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_CONNECT_SHORT, BT_CLIENT_OPTION_AIR_CONNECT, OptionParser::Type::OPTION, BT_CLIENT_OPTION_AIR_CONNECT_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_SERVER_SHORT, BT_CLIENT_OPTION_AIR_SERVER, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_SERVER_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_COUNTRY_SHORT, BT_CLIENT_OPTION_AIR_COUNTRY, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_COUNTRY_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_TLS_MODE_SHORT, BT_CLIENT_OPTION_AIR_TLS_MODE, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_TLS_MODE_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_IPV6_SHORT, BT_CLIENT_OPTION_AIR_IPV6, OptionParser::Type::BOOL, BT_CLIENT_OPTION_AIR_IPV6_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_6TO4_SHORT, BT_CLIENT_OPTION_AIR_6TO4, OptionParser::Type::BOOL, BT_CLIENT_OPTION_AIR_6TO4_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_USER_SHORT, BT_CLIENT_OPTION_AIR_USER, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_USER_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_PASSWORD_SHORT, BT_CLIENT_OPTION_AIR_PASSWORD, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_PASSWORD_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_KEY_SHORT, BT_CLIENT_OPTION_AIR_KEY, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_KEY_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_KEY_LIST_SHORT, BT_CLIENT_OPTION_AIR_KEY_LIST, OptionParser::Type::OPTION, BT_CLIENT_OPTION_AIR_KEY_LIST_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_KEY_LOAD_SHORT, BT_CLIENT_OPTION_AIR_KEY_LOAD, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_KEY_LOAD_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_SAVE_SHORT, BT_CLIENT_OPTION_AIR_SAVE, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_SAVE_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_INFO_SHORT, BT_CLIENT_OPTION_AIR_INFO, OptionParser::Type::OPTION, BT_CLIENT_OPTION_AIR_INFO_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_LIST_SHORT, BT_CLIENT_OPTION_AIR_LIST, OptionParser::Type::OPTION, BT_CLIENT_OPTION_AIR_LIST_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST_SHORT, BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_WHITE_SERVER_LIST_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST_SHORT, BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_BLACK_SERVER_LIST_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST_SHORT, BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_WHITE_COUNTRY_LIST_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST_SHORT, BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST, OptionParser::Type::STRING, BT_CLIENT_OPTION_AIR_BLACK_COUNTRY_LIST_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_RESPONSE_SHORT, BT_CLIENT_OPTION_RESPONSE, OptionParser::Type::STRING, BT_CLIENT_OPTION_RESPONSE_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_DC_SHORT, BT_CLIENT_OPTION_DC, OptionParser::Type::STRING, BT_CLIENT_OPTION_DC_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_CIPHER_SHORT, BT_CLIENT_OPTION_CIPHER, OptionParser::Type::STRING, BT_CLIENT_OPTION_CIPHER_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_LIST_DATA_CIPHERS_SHORT, BT_CLIENT_OPTION_LIST_DATA_CIPHERS, OptionParser::Type::OPTION, BT_CLIENT_OPTION_LIST_DATA_CIPHERS_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_SERVER_SHORT, BT_CLIENT_OPTION_SERVER, OptionParser::Type::STRING, BT_CLIENT_OPTION_SERVER_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PROTO_SHORT, BT_CLIENT_OPTION_PROTO, OptionParser::Type::STRING, BT_CLIENT_OPTION_PROTO_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PORT_SHORT, BT_CLIENT_OPTION_PORT, OptionParser::Type::INTEGER, BT_CLIENT_OPTION_PORT_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_TCP_QUEUE_LIMIT_SHORT, BT_CLIENT_OPTION_TCP_QUEUE_LIMIT, OptionParser::Type::INTEGER, BT_CLIENT_OPTION_TCP_QUEUE_LIMIT_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_ALLOWUAF_SHORT, BT_CLIENT_OPTION_ALLOWUAF, OptionParser::Type::STRING, BT_CLIENT_OPTION_ALLOWUAF_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_NCP_DISABLE_SHORT, BT_CLIENT_OPTION_NCP_DISABLE, OptionParser::Type::OPTION, BT_CLIENT_OPTION_NCP_DISABLE_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_NETWORK_LOCK_SHORT, BT_CLIENT_OPTION_NETWORK_LOCK, OptionParser::Type::STRING, BT_CLIENT_OPTION_NETWORK_LOCK_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_IGNORE_DNS_PUSH_SHORT, BT_CLIENT_OPTION_IGNORE_DNS_PUSH, OptionParser::Type::OPTION, BT_CLIENT_OPTION_IGNORE_DNS_PUSH_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_TIMEOUT_SHORT, BT_CLIENT_OPTION_TIMEOUT, OptionParser::Type::INTEGER, BT_CLIENT_OPTION_TIMEOUT_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_COMPRESS_SHORT, BT_CLIENT_OPTION_COMPRESS, OptionParser::Type::STRING, BT_CLIENT_OPTION_COMPRESS_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PK_PASSWORD_SHORT, BT_CLIENT_OPTION_PK_PASSWORD, OptionParser::Type::STRING, BT_CLIENT_OPTION_PK_PASSWORD_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_TVM_OVERRIDE_SHORT, BT_CLIENT_OPTION_TVM_OVERRIDE, OptionParser::Type::STRING, BT_CLIENT_OPTION_TVM_OVERRIDE_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_TCPROF_OVERRIDE_SHORT, BT_CLIENT_OPTION_TCPROF_OVERRIDE, OptionParser::Type::STRING, BT_CLIENT_OPTION_TCPROF_OVERRIDE_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PROXY_HOST_SHORT, BT_CLIENT_OPTION_PROXY_HOST, OptionParser::Type::STRING, BT_CLIENT_OPTION_PROXY_HOST_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PROXY_PORT_SHORT, BT_CLIENT_OPTION_PROXY_PORT, OptionParser::Type::INTEGER, BT_CLIENT_OPTION_PROXY_PORT_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PROXY_USERNAME_SHORT, BT_CLIENT_OPTION_PROXY_USERNAME, OptionParser::Type::STRING, BT_CLIENT_OPTION_PROXY_USERNAME_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PROXY_PASSWORD_SHORT, BT_CLIENT_OPTION_PROXY_PASSWORD, OptionParser::Type::STRING, BT_CLIENT_OPTION_PROXY_PASSWORD_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PROXY_BASIC_SHORT, BT_CLIENT_OPTION_PROXY_BASIC, OptionParser::Type::OPTION, BT_CLIENT_OPTION_PROXY_BASIC_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_ALT_PROXY_SHORT, BT_CLIENT_OPTION_ALT_PROXY, OptionParser::Type::OPTION, BT_CLIENT_OPTION_ALT_PROXY_DESC);

#ifdef ENABLE_DCO

    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_DCO_SHORT, BT_CLIENT_OPTION_DCO, OptionParser::Type::OPTION, BT_CLIENT_OPTION_DCO_DESC);

#endif

    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_CACHE_PASSWORD_SHORT, BT_CLIENT_OPTION_CACHE_PASSWORD, OptionParser::Type::OPTION, BT_CLIENT_OPTION_CACHE_PASSWORD_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_NO_CERT_SHORT, BT_CLIENT_OPTION_NO_CERT, OptionParser::Type::OPTION, BT_CLIENT_OPTION_NO_CERT_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_DEF_KEYDIR_SHORT, BT_CLIENT_OPTION_DEF_KEYDIR, OptionParser::Type::STRING, BT_CLIENT_OPTION_DEF_KEYDIR_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_SSL_DEBUG_SHORT, BT_CLIENT_OPTION_SSL_DEBUG, OptionParser::Type::INTEGER, BT_CLIENT_OPTION_SSL_DEBUG_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AUTO_SESS_SHORT, BT_CLIENT_OPTION_AUTO_SESS, OptionParser::Type::OPTION, BT_CLIENT_OPTION_AUTO_SESS_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_AUTH_RETRY_SHORT, BT_CLIENT_OPTION_AUTH_RETRY, OptionParser::Type::OPTION, BT_CLIENT_OPTION_AUTH_RETRY_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PERSIST_TUN_SHORT, BT_CLIENT_OPTION_PERSIST_TUN, OptionParser::Type::OPTION, BT_CLIENT_OPTION_PERSIST_TUN_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PEER_INFO_SHORT, BT_CLIENT_OPTION_PEER_INFO, OptionParser::Type::STRING, BT_CLIENT_OPTION_PEER_INFO_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_GREMLIN_SHORT, BT_CLIENT_OPTION_GREMLIN, OptionParser::Type::STRING, BT_CLIENT_OPTION_GREMLIN_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_EPKI_CERT_SHORT, BT_CLIENT_OPTION_EPKI_CERT, OptionParser::Type::STRING, BT_CLIENT_OPTION_EPKI_CERT_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_EPKI_CA_SHORT, BT_CLIENT_OPTION_EPKI_CA, OptionParser::Type::STRING, BT_CLIENT_OPTION_EPKI_CA_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_EPKI_KEY_SHORT, BT_CLIENT_OPTION_EPKI_KEY, OptionParser::Type::STRING, BT_CLIENT_OPTION_EPKI_KEY_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_GUI_VERSION_SHORT, BT_CLIENT_OPTION_GUI_VERSION, OptionParser::Type::STRING, BT_CLIENT_OPTION_GUI_VERSION_DESC);

#ifdef OPENVPN_REMOTE_OVERRIDE

    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_REMOTE_OVERRIDE_SHORT, BT_CLIENT_OPTION_REMOTE_OVERRIDE, OptionParser::Type::STRING, BT_CLIENT_OPTION_REMOTE_OVERRIDE_DESC);

#endif

    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_RECOVER_NETWORK_SHORT, BT_CLIENT_OPTION_RECOVER_NETWORK, OptionParser::Type::OPTION, BT_CLIENT_OPTION_RECOVER_NETWORK_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_DISCONNECT_SHORT, BT_CLIENT_OPTION_DISCONNECT, OptionParser::Type::OPTION, BT_CLIENT_OPTION_DISCONNECT_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_PAUSE_SHORT, BT_CLIENT_OPTION_PAUSE, OptionParser::Type::OPTION, BT_CLIENT_OPTION_PAUSE_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_RESUME_SHORT, BT_CLIENT_OPTION_RESUME, OptionParser::Type::OPTION, BT_CLIENT_OPTION_RESUME_DESC);
    result &= optionParser->addConfigOption(BT_CLIENT_OPTION_RECONNECT_SHORT, BT_CLIENT_OPTION_RECONNECT, OptionParser::Type::OPTION, BT_CLIENT_OPTION_RECONNECT_DESC);

    return result;
}

int main(int argc, char **argv)
{
    DBusMessage *dbusMessage = nullptr;
    std::string message;
    RCParser::Directive *rcDirective = nullptr;
    int network_wait_seconds = 0;

    shutdownInProgress = false;

    logger = new Logger(BLUETIT_LOG_NAME);

    if(logger == nullptr)
    {
        syslog(LOG_DEBUG, "cannot create Logger object. Exiting.");

        exit(EXIT_FAILURE);
    }

    osstring.str("");

    osstring << "Starting " << BLUETIT_FULL_NAME << " - " << BLUETIT_RELEASE_DATE;

    logger->systemLog(osstring.str());

    osstring.str("");

    osstring << VpnClient::openVPNInfo();

    logger->systemLog(osstring.str());

    osstring.str("");

    osstring << VpnClient::openVPNCopyright();

    logger->systemLog(osstring.str());

    osstring.str("");

    osstring << "SSL Library: " << VpnClient::sslLibraryVersion();

    logger->systemLog(osstring.str());

    if(check_if_root() == false)
    {
        std::string msg = "You need to be root in order to run this program.";

        if(std::cout.good())
            std::cout << msg << std::endl << std::endl;

        logger->systemLog(msg);

        delete logger;

        exit(EXIT_FAILURE);
    }

    if(access(BLUETIT_LOCK_FILE, F_OK) == 0)
    {
        osstring.str("");

        osstring << BLUETIT_SHORT_NAME << " is already running or did not exit gracefully on its last run or has been killed. Exiting.";

        logger->systemLog(osstring.str());

        delete logger;

        exit(EXIT_FAILURE);
    }

    if(access(HUMMINGBIRD_LOCK_FILE, F_OK) == 0)
    {
        osstring.str("");

        osstring << "Hummingbird client is running or it did not exit gracefully. Exiting.";

        logger->systemLog(osstring.str());

        delete logger;

        exit(EXIT_FAILURE);
    }

    create_daemon();

    localNetwork = new LocalNetwork();

    if(localNetwork == nullptr)
    {
        logger->systemLog("Cannot create LocalNetwork object. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    LocalNetwork::Gateway gateway = localNetwork->getDefaultGatewayInterface();

    if(gateway.address == "" && gateway.interface == "")
    {
        osstring.str("");
        
        osstring << "Waiting for external network connection to be available";
        
        logger->systemLog(osstring.str());

        while(gateway.address == "" && gateway.interface == "")
        {
            network_wait_seconds += WAIT_NETWORK_INTERVAL;
            
            if((network_wait_seconds % WAIT_NETWORK_MESSAGE_INTERVAL) == 0)
                logger->systemLog(osstring.str());

            sleep(WAIT_NETWORK_INTERVAL);
            
            gateway = localNetwork->getDefaultGatewayInterface();
        }
    }

    osstring.str("");
    
    osstring << "External network is reachable via gateway " << gateway.address;
    osstring << " through interface " << gateway.interface;

    logger->systemLog(osstring.str());

    try
    {
        dbusConnector = new DBusConnector(BT_DBUS_INTERFACE_NAME, BT_SERVER_BUS_NAME);

        if(!dbusConnector)
        {
            logger->systemLog("Failed to create DBusConnector. Exiting.");

            cleanup_and_exit(EXIT_FAILURE);
        }
    }
    catch(DBusConnectorException &e)
    {
        osstring.str("");

        osstring << "DBusConnectorException: " << e.what();

        logger->systemLog(osstring.str());

        cleanup_and_exit(EXIT_FAILURE);
    }
    
    reset_settings();

#if defined(OPENVPN_PLATFORM_LINUX)

    dnsManager = new DNSManager(RESOLVDOTCONF_BACKUP);

    if(dnsManager == nullptr)
    {
        logger->systemLog("Cannot create DNSManager object. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

#endif

    clientConfig = new ClientAPI::Config();

    if(clientConfig == nullptr)
    {
        logger->systemLog("Cannot create OpenVPN ClientAPI::Config. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    set_openvpn_client_config();

    logger->systemLog("Successfully connected to D-Bus");

    if(access(BLUETIT_RC_FILE, F_OK) != 0)
    {
        osstring << "Creating run control file " << BLUETIT_RC_FILE << " from template";

        logger->systemLog(osstring.str());

        std::ofstream rcTemplate(BLUETIT_RC_FILE);

        rcTemplate << BLUETIT_RC_TEMPLATE;

        rcTemplate.close();
    }

    osstring.str("");

    osstring << "Reading run control directives from file " << BLUETIT_RC_FILE;

    logger->systemLog(osstring.str());

    rcParser = new RCParser(BLUETIT_RC_FILE);

    if(rcParser == nullptr)
    {
        logger->systemLog("Cannot create RCParser object. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(setup_rc_directives() == false)
    {
        logger->systemLog("Cannot set directives for RCParser. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    optionParser = new OptionParser();

    if(optionParser == nullptr)
    {
        logger->systemLog("Cannot create OptionParser object. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    if(setup_options() == false)
    {
        osstring.str("");
        
        osstring << "Cannot setup options for " << BLUETIT_SHORT_NAME << ". Exiting.";

        logger->systemLog(osstring.str());

        cleanup_and_exit(EXIT_FAILURE);
    }

    iPv6Enabled = localNetwork->isIPv6Enabled();
    
    osstring.str("");
    
    osstring << "IPv6 is ";
    
    if(iPv6Enabled == false)
        osstring << "not ";

    osstring << "available in this system";
        
    logger->systemLog(osstring.str());

    RCParser::Error rcError = rcParser->parseRCFile();

    if(rcError != RCParser::Error::OK)
    {
        switch(rcError)
        {
            case RCParser::Error::NO_DIRECTIVES_PROVIDED:
            {
                osstring.str("");

                osstring << "ERROR: No rc directives provided to RCParser. Exiting.";

                logger->systemLog(osstring.str());

                cleanup_and_exit(EXIT_FAILURE);
            }
            break;

            case RCParser::Error::CANNOT_OPEN_RC_FILE:
            {
                osstring.str("");

                osstring << BLUETIT_RC_FILE << " does not exist. " << BLUETIT_SHORT_NAME << " will use default values.";

                logger->systemLog(osstring.str());
            }
            break;

            case RCParser::Error::UNKNOWN_DIRECTIVE:
            {
                osstring.str("");

                osstring << "Error while parsing " << BLUETIT_RC_FILE << " file. ";

                osstring << rcParser->getErrorDescription() << ". Exiting.";

                logger->systemLog(osstring.str());

                cleanup_and_exit(EXIT_FAILURE);
            }
            break;

            case RCParser::Error::PARSE_ERROR:
            {
                osstring.str("");

                osstring << "Error while parsing " << BLUETIT_RC_FILE << " file. Exiting.";

                logger->systemLog(osstring.str());

                cleanup_and_exit(EXIT_FAILURE);
            }
            break;

            default:
            {
                osstring.str("");

                osstring << "Unexpected error while parsing " << BLUETIT_RC_FILE << " file. Exiting.";

                logger->systemLog(osstring.str());

                cleanup_and_exit(EXIT_FAILURE);
            }
            break;
        }
    }

    RCParser::Directives rcInvalidDirective = rcParser->getInvalidDirectives();

    for(int i = 0; i < rcInvalidDirective.size(); i++)
    {
        osstring.str("");

        osstring << "ERROR: invalid RC directive '" << rcInvalidDirective[i]->name << "' - " << rcInvalidDirective[i]->error;

        logger->systemLog(osstring.str());
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_BOOTSERVER);

    if(rcDirective == nullptr)
    {
        osstring.str("");

        osstring << "WARNING: no AirVPN bootstrap server URL found in '" << BLUETIT_RC_FILE << "'. AirVPN services and connection are disabled.";

        logger->systemLog(osstring.str());

        airVpnEnabled = false;
    }
    else
        airVpnEnabled = true;

    if(airVpnEnabled == true)
    {
        airVpnManifest = new AirVPNManifest(BLUETIT_RESOURCE_DIRECTORY);

        if(airVpnManifest == nullptr)
        {
            syslog(LOG_DEBUG, "Cannot create AirVPNManifest object. Exiting.");

            cleanup_and_exit(EXIT_FAILURE);
        }

        airVpnManifest->resetBootServers();

        for(std::string server : rcDirective->value)
        {
            airVpnManifest->addBootServer(server);
        }
        
        RCParser::Directive *rsaModulus = rcParser->getDirective(RC_DIRECTIVE_RSAMODULUS);
        RCParser::Directive *rsaExponent = rcParser->getDirective(RC_DIRECTIVE_RSAEXPONENT);

        if(rsaModulus != nullptr && rsaExponent != nullptr)
        {
            airVpnManifest->setRsaPublicKeyModulus(rsaModulus->value[0]);
            airVpnManifest->setRsaPublicKeyExponent(rsaExponent->value[0]);
        }
        else
        {
            osstring.str("");

            osstring << "WARNING: AirVPN RSA parameters not found in '" << BLUETIT_RC_FILE << "'. AirVPN services and connection are disabled.";

            logger->systemLog(osstring.str());

            airVpnEnabled = false;
        }
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_COUNTRY);

    if(rcDirective != nullptr)
    {
        bluetitCountry = rcDirective->value[0];

        osstring.str("");

        osstring << "System country set to " << bluetitCountry << " by " << BLUETIT_SHORT_NAME << " policy.";

        logger->systemLog(osstring.str());
    }
    else
        bluetitCountry = "";

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRCONNECTATBOOT);

    if(rcDirective != nullptr)
    {
        if(rcDirective->value[0] != "off" && rcDirective->value[0] != "quick" && rcDirective->value[0] != "server" && rcDirective->value[0] != "country")
        {
            osstring.str("");

            osstring << "ERROR in " << BLUETIT_RC_FILE << ": invalid value \"" << rcDirective->value[0] << "\"";
            osstring << " for directive " << RC_DIRECTIVE_AIRCONNECTATBOOT;
            osstring << " (allowed values: off, quick, server, country)";

            logger->systemLog(osstring.str());
        
            airVpnConnectAtBoot = "off";
        }
        else
            airVpnConnectAtBoot = rcDirective->value[0];
    }
    else
        airVpnConnectAtBoot = "off";

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRUSERNAME);

    if(rcDirective != nullptr)
        airVpnBootConnectionUserName = rcDirective->value[0];
    else
        airVpnBootConnectionUserName = "";

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRPASSWORD);

    if(rcDirective != nullptr)
        airVpnBootConnectionPassword = rcDirective->value[0];
    else
        airVpnBootConnectionPassword = "";

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRKEY);

    if(rcDirective != nullptr)
        airVpnBootConnectionKey = rcDirective->value[0];
    else
        airVpnBootConnectionKey = "";

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRSERVER);

    if(rcDirective != nullptr)
        airVpnBootConnectionServer = rcDirective->value[0];
    else
        airVpnBootConnectionServer = "";

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRCOUNTRY);

    if(rcDirective != nullptr)
        airVpnBootConnectionCountry = rcDirective->value[0];
    else
        airVpnBootConnectionCountry = "";

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRPROTO);

    if(rcDirective != nullptr)
    {
        if(rcDirective->value[0] != "udp" && rcDirective->value[0] != "tcp")
        {
            osstring.str("");

            osstring << "ERROR in " << BLUETIT_RC_FILE << ": invalid value \"" << rcDirective->value[0] << "\"";
            osstring << " for directive " << RC_DIRECTIVE_AIRPROTO;
            osstring << " (allowed values: udp, tcp)";

            logger->systemLog(osstring.str());
        
            airVpnBootConnectionProto = DEFAULT_PROTO;

            airVpnConnectAtBoot = "off";
        }
        else
            airVpnBootConnectionProto = rcDirective->value[0];

        if(airVpnConnectAtBoot == "quick")
        {
            osstring.str("");

            osstring << "WARNING: AirVPN boot connection mode is set to \"quick\".";
            osstring << " Directive \"" << RC_DIRECTIVE_AIRPROTO << "\" will override the default connection scheme.";

            logger->systemLog(osstring.str());
            
            airVpnOverrideQuickConnection = true;
        }
    }
    else
        airVpnBootConnectionProto = DEFAULT_PROTO;

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRPORT);

    if(rcDirective != nullptr)
    {
        try
        {
            if(std::stoi(rcDirective->value[0]) < 1 || std::stoi(rcDirective->value[0]) > 65535)
            {
                osstring.str("");

                osstring << "ERROR in " << BLUETIT_RC_FILE << ": invalid value \"" << rcDirective->value[0] << "\"";
                osstring << " for directive " << RC_DIRECTIVE_AIRPORT;
                osstring << " (must be from 1 to 65535)";

                logger->systemLog(osstring.str());
            
                airVpnBootConnectionPort = DEFAULT_PORT;
                airVpnConnectAtBoot = "off";
            }
            else
                airVpnBootConnectionPort = rcDirective->value[0];
        }
        catch(std::exception &e)
        {
            osstring.str("");

            osstring << "ERROR in " << BLUETIT_RC_FILE << ": illegal value \"" << rcDirective->value[0] << "\"";
            osstring << " for directive " << RC_DIRECTIVE_AIRPORT;

            logger->systemLog(osstring.str());

            airVpnBootConnectionPort = DEFAULT_PORT;
            airVpnConnectAtBoot = "off";
        }

        if(airVpnConnectAtBoot == "quick")
        {
            osstring.str("");

            osstring << "WARNING: AirVPN boot connection mode is set to \"quick\".";
            osstring << " Directive \"" << RC_DIRECTIVE_AIRPORT << "\" will override the default connection scheme.";

            logger->systemLog(osstring.str());

            airVpnOverrideQuickConnection = true;
        }
    }
    else
        airVpnBootConnectionPort = DEFAULT_PORT;

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRIPV6);

    if(rcDirective != nullptr)
    {
        if(rcParser->isDirectiveEnabled(rcDirective) == true)
        {
            if(iPv6Enabled == true)
                airVpnBootConnectionIPv6 = true;
            else
            {
                airVpnBootConnectionIPv6 = false;

                osstring.str("");
                
                osstring << "WARNING: directive " << RC_DIRECTIVE_AIRIPV6 << " in " << BLUETIT_RC_FILE;
                osstring << " is ignored: IPv6 is not available in this system.";

                logger->systemLog(osstring.str());
            }
        }
        else
            airVpnBootConnectionIPv6 = false;
    }
    else
        airVpnBootConnectionIPv6 = false;

    AirVPNTools::setBootServerIPv6Mode(airVpnBootConnectionIPv6);

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIR6TO4);

    if(rcDirective != nullptr)
    {
        if(rcParser->isDirectiveEnabled(rcDirective) == true)
        {
            if(iPv6Enabled == true)
                airVpnBootConnection6to4 = true;
            else
            {
                airVpnBootConnection6to4 = false;

                osstring.str("");

                osstring << "WARNING: directive " << RC_DIRECTIVE_AIR6TO4 << " in " << BLUETIT_RC_FILE;
                osstring << " is ignored: IPv6 is not available in this system.";

                logger->systemLog(osstring.str());
            }
        }
        else
            airVpnBootConnection6to4 = false;
    }
    else
        airVpnBootConnection6to4 = false;

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_FORBIDQUICKHOMECOUNTRY);

    if(rcDirective != nullptr)
    {
        osstring.str("");

        osstring << "Forbid quick connection to home country is ";

        if(rcParser->isDirectiveEnabled(rcDirective) == true)
        {
            forbidQuickHomeCountry = true;

            osstring << "enabled";
        }
        else
        {
            forbidQuickHomeCountry = false;

            osstring << "disabled";
        }

        osstring << " by " << BLUETIT_SHORT_NAME << " policy";

        OPENVPN_LOG(osstring.str());
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_CIPHER);

    if(rcDirective != nullptr)
    {
        for(std::string cipher : rcDirective->value)
        {
            if(VpnClient::isDataCipherSupported(cipher) == false)
            {
                osstring.str("");

                osstring << "WARNING: cipher " << cipher << " for directive " << RC_DIRECTIVE_CIPHER;
                osstring << " in " << BLUETIT_RC_FILE << " is not supported.";

                logger->systemLog(osstring.str());
            }
        }
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_AIRCIPHER);

    if(rcDirective != nullptr)
    {
        if(VpnClient::isDataCipherSupported(rcDirective->value[0]) == false)
        {
            osstring.str("");

            osstring << "WARNING: cipher " << rcDirective->value[0] << " for directive " << RC_DIRECTIVE_AIRCIPHER;
            osstring << " in " << BLUETIT_RC_FILE << " is not supported.";

            logger->systemLog(osstring.str());
            
            airVpnBootConnectionCipher = "";
        }
        else
            airVpnBootConnectionCipher = rcDirective->value[0];
    }
    else
        airVpnBootConnectionCipher = "";

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_NETWORKLOCKPERSIST);

    if(rcDirective != nullptr)
    {
        std::string value = AirVPNTools::normalizeBoolValue(rcDirective->value[0], "on", "off");

        if(value == "on")
            persistent_network_lock_mode = NetFilter::Mode::AUTO;
        else if(value == "iptables")
            persistent_network_lock_mode = NetFilter::Mode::IPTABLES;
        else if(value == "nftables")
            persistent_network_lock_mode = NetFilter::Mode::NFTABLES;
        else if(value == "pf")
            persistent_network_lock_mode = NetFilter::Mode::PF;
        else if(value == "off")
            persistent_network_lock_mode = NetFilter::Mode::OFF;
        else
        {
            osstring.str("");

            osstring << "ERROR: " << RC_DIRECTIVE_NETWORKLOCKPERSIST << " in " << BLUETIT_RC_FILE << " must be on, iptables, nftables, pf or off";

            logger->systemLog(osstring.str());

            persistent_network_lock_mode = NetFilter::Mode::OFF;
        }

        if(persistent_network_lock_mode != NetFilter::Mode::OFF)
            policy_network_lock_mode = persistent_network_lock_mode;
    }
    else
        persistent_network_lock_mode = NetFilter::Mode::OFF;


    rcDirective = rcParser->getDirective(RC_DIRECTIVE_NETWORKLOCK);

    if(rcDirective != nullptr)
    {
        if(persistent_network_lock_mode == NetFilter::Mode::OFF)
        {
            std::string value = AirVPNTools::normalizeBoolValue(rcDirective->value[0], "on", "off");

            if(value == "on")
                policy_network_lock_mode = NetFilter::Mode::AUTO;
            else if(value == "iptables")
                policy_network_lock_mode = NetFilter::Mode::IPTABLES;
            else if(value == "nftables")
                policy_network_lock_mode = NetFilter::Mode::NFTABLES;
            else if(value == "pf")
                policy_network_lock_mode = NetFilter::Mode::PF;
            else if(value == "off")
                policy_network_lock_mode = NetFilter::Mode::OFF;
            else
            {
                osstring.str("");

                osstring << "ERROR: " << RC_DIRECTIVE_NETWORKLOCK << " in " << BLUETIT_RC_FILE << " must be on, iptables, nftables, pf or off";

                logger->systemLog(osstring.str());

                policy_network_lock_mode = NetFilter::Mode::UNKNOWN;
            }
        }
        else
        {
            osstring.str("");

            osstring << "WARNING: " << RC_DIRECTIVE_NETWORKLOCKPERSIST << " directive found in " << BLUETIT_RC_FILE << ". " << RC_DIRECTIVE_NETWORKLOCK << " directive is ignored.";

            logger->systemLog(osstring.str());

            policy_network_lock_mode = persistent_network_lock_mode;
        }
    }

    rcDirective = rcParser->getDirective(RC_DIRECTIVE_MAXCONNECTIONRETRIES);

    if(rcDirective != nullptr)
    {
        try
        {
            maxConnectionRetries = std::stoi(rcDirective->value[0]);
        }
        catch(std::exception &e)
        {
            maxConnectionRetries = DEFAULT_MAX_CONNECTION_RETRIES;
        }
    }
    else
        maxConnectionRetries = DEFAULT_MAX_CONNECTION_RETRIES;

    osstring.str("");

    osstring << BLUETIT_SHORT_NAME << " successfully initialized and ready";

    logger->systemLog(osstring.str());

    logger->enableDBusMode(dbusConnector);

    try
    {
        netFilter = new NetFilter(BLUETIT_RESOURCE_DIRECTORY, policy_network_lock_mode);

        if(netFilter == nullptr)
        {
            logger->systemLog("Cannot create NetFilter object. Exiting.");

            cleanup_and_exit(EXIT_FAILURE);
        }
    }
    catch(NetFilterException &e)
    {
        osstring.str("");

        osstring << "NetFilterException: " << e.what();

        logger->systemLog(osstring.str());

        cleanup_and_exit(EXIT_FAILURE);
    }

    manifestSemaphore = new Semaphore();

    if(manifestSemaphore == nullptr)
    {
        syslog(LOG_DEBUG, "Cannot create Semaphore for AirVPNManifest thread. Exiting.");

        cleanup_and_exit(EXIT_FAILURE);
    }

    manifestSemaphore->permit();

    bluetit_status();

    if(persistent_network_lock_mode != NetFilter::Mode::OFF)
    {
        if(status == BLUETIT_STATUS_READY)
        {
            logger->systemLog("Enabling persistent network filter and lock");
            
            manifestSemaphore->forbid("Persistent network filter and lock enabled");

            enable_network_lock(NETLOCKMODE_PERSISTENT);
        }
        else
        {
            osstring.str("");

            osstring << "ERROR: Cannot enable persistent network filter and lock. " << bluetit_status_description();

            logger->systemLog(osstring.str());
        }

        logger->flushLog();
    }

    if(airVpnEnabled == true)
    {
        if(bluetitCountry == "")
        {
            if(status == BLUETIT_STATUS_READY)
            {
                if(netFilter->isNetworkLockEnabled() == false)
                {
                    std::map<std::string, std::string>::iterator it;
                
                    logger->systemLog("Requesting network IP and country to AirVPN ipleak.net via secure connection");

                    bluetitLocation = AirVPNTools::detectLocation();

                    if(bluetitLocation.size() > 0)
                    {
                        it = bluetitLocation.find("status");

                        if(it->second == AirVPNTools::LOCATION_STATUS_OK)
                        {
                            it = bluetitLocation.find("ip");

                            osstring.str("");
                            
                            osstring << "Network IP: " << it->second;

                            logger->systemLog(osstring.str());

                            it = bluetitLocation.find("country");

                            osstring.str("");

                            osstring << "System country: " << it->second;

                            logger->systemLog(osstring.str());
                            
                            bluetitCountry = it->second;
                        }
                        else
                        {
                            osstring.str("");

                            osstring << "ERROR: Cannot detect system location: " << it->second;

                            logger->systemLog(osstring.str());

                            bluetitCountry = "";
                        }
                    }
                    else
                    {
                        logger->systemLog("ERROR: ipleak.net returned an empty document");
                        
                        bluetitCountry = "";
                    }
                }
                else
                {
                    osstring.str("");
                    
                    osstring << "WARNING: Cannot determine system country via AirVPN's ipleak.net: Persistent Network Lock is enabled. AirVPN quick connection will use world best server. Please use \"country\" directive in " << BLUETIT_RC_FILE << " for better performance.";
                    
                    logger->systemLog(osstring.str());
                }
            }
            else
            {
                osstring.str("");
                
                osstring << "ERROR: Connection to AirVPN's ipleak.net cancelled. " << bluetit_status_description();

                logger->systemLog(osstring.str());
            }
        }

        if(airVpnConnectAtBoot != "off")
            manifestSemaphore->forbid("AirVPN boot connection initialization in progress");

        rcDirective = rcParser->getDirective(RC_DIRECTIVE_MANIFESTUPDATEINTERVAL);

        if(rcDirective != nullptr)
        {
            try
            {
                airVpnManifestUpdateInterval = std::stoi(rcDirective->value[0]);

                if(airVpnManifestUpdateInterval <= 0)
                {
                    osstring.str("");

                    osstring << "WARNING: Invalid value '" << rcDirective->value[0] << "' for ";
                    osstring << RC_DIRECTIVE_MANIFESTUPDATEINTERVAL << " in " << BLUETIT_RC_FILE;

                    logger->systemLog(osstring.str());

                    logger->systemLog("Using default update interval value");

                    airVpnManifestUpdateInterval = DEFAULT_MANIFEST_UPDATE_INTERVAL;
                }
            }
            catch(std::exception &e)
            {
                osstring.str("");

                osstring << "WARNING: Illegal value '" << rcDirective->value[0] << "' for ";
                osstring << RC_DIRECTIVE_MANIFESTUPDATEINTERVAL << " in " << BLUETIT_RC_FILE;

                logger->systemLog(osstring.str());

                logger->systemLog("Using default update interval value");

                airVpnManifestUpdateInterval = DEFAULT_MANIFEST_UPDATE_INTERVAL;
            }
        }
        else
            airVpnManifestUpdateInterval = DEFAULT_MANIFEST_UPDATE_INTERVAL;

        if(status == BLUETIT_STATUS_DIRTY_EXIT)
            manifestSemaphore->forbid("System needs network recovery");

        manifestUpdaterFutureObject = manifestUpdaterSignalExit.get_future();

        airVpnManifestUpdaterThread = new std::thread(airvpn_manifest_updater, airVpnManifestUpdateInterval, std::move(manifestUpdaterFutureObject));

        if(airVpnManifestUpdaterThread == nullptr)
        {
            osstring.str("");
            
            osstring << "ERROR: Cannot create AirVPN Manifest updater thread. Terminating " << BLUETIT_SHORT_NAME;
            
            logger->systemLog(osstring.str());
            
            cleanup_and_exit(EXIT_FAILURE);
        }
    }

    // main loop

    try
    {
        if(airVpnConnectAtBoot != "off")
            start_boot_airvpn_connection();

        while(dbusConnector->readWriteDispatch())
        {
            while((dbusMessage = dbusConnector->popMessage()) != NULL)
            {
                if(dbusConnector->isMethod(dbusMessage, BT_METHOD_VERSION))
                {
                    logger->logMethod(BT_METHOD_VERSION);

                    dbusResponse.clear();

                    osstring.str("");

                    osstring << BLUETIT_FULL_NAME << " - " << BLUETIT_RELEASE_DATE;

                    dbusResponse.setResponse(osstring.str());

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_OPENVPN_INFO))
                {
                    logger->logMethod(BT_METHOD_OPENVPN_INFO);

                    dbusResponse.clear();

                    dbusResponse.setResponse(VpnClient::openVPNInfo());

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_OPENVPN_COPYRIGHT))
                {
                    logger->logMethod(BT_METHOD_OPENVPN_COPYRIGHT);

                    dbusResponse.clear();

                    dbusResponse.setResponse(VpnClient::openVPNCopyright());

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_SSL_LIBRARY_VERSION))
                {
                    logger->logMethod(BT_METHOD_SSL_LIBRARY_VERSION);

                    dbusResponse.clear();

                    dbusResponse.setResponse(VpnClient::sslLibraryVersion());

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_BLUETIT_STATUS))
                {
                    osstring.str("");

                    osstring << BT_METHOD_BLUETIT_STATUS << " -> ";

                    switch(status)
                    {
                        case BLUETIT_STATUS_READY:
                        {
                            osstring << BLUETIT_SHORT_NAME << " is ready";
                        }
                        break;

                        case BLUETIT_STATUS_CONNECTED:
                        {
                            osstring << BLUETIT_SHORT_NAME << " is connected to VPN";
                        }
                        break;

                        case BLUETIT_STATUS_DIRTY_EXIT:
                        {
                            osstring << BLUETIT_SHORT_NAME << " did not exit properly and needs recovery";
                        }
                        break;

                        case BLUETIT_STATUS_RESOURCE_DIRECTORY_ERROR:
                        {
                            osstring << "Resource directory " << BLUETIT_RESOURCE_DIRECTORY << " is not accessible";
                        }
                        break;

                        case BLUETIT_STATUS_INIT_ERROR:
                        {
                            osstring << BLUETIT_SHORT_NAME << " initialization error";
                        }
                        break;

                        case BLUETIT_STATUS_LOCK_ERROR:
                        {
                            osstring << "Lock file has been tampered";
                        }
                        break;

                        case BLUETIT_STATUS_UNKNOWN:
                        {
                            osstring << "Unknown " << BLUETIT_SHORT_NAME << " status";
                        }
                        break;

                        default:
                        {
                            osstring << BLUETIT_SHORT_NAME << " status cannot be determined";
                        }
                        break;
                    }

                    logger->logMethod(osstring.str());

                    dbusConnector->replyToMessage(dbusMessage, status);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_RESET_BLUETIT_OPTIONS))
                {
                    osstring.str("");

                    if(status == BLUETIT_STATUS_READY)
                    {
                        reset_settings();
                        
                        logger->resetQueue();

                        osstring << BLUETIT_SHORT_NAME << " options successfully reset";
                    }
                    else
                    {
                        osstring << "Operation refused. " << BLUETIT_SHORT_NAME << " is ";

                        if(status == BLUETIT_STATUS_CONNECTED)
                            osstring << "connected to VPN.";
                        else
                            osstring << "not accepting reset now.";
                    }

                    message = BT_METHOD_RESET_BLUETIT_OPTIONS;
                    message += " -> ";
                    message += osstring.str();

                    logger->logMethod(message);

                    dbusResponse.clear();

                    dbusResponse.setResponse(osstring.str());

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_LIST_DATA_CIPHERS))
                {
                    std::vector<std::string> supportedDataCipher = VpnClient::getSupportedDataCiphers();

                    logger->logMethod(BT_METHOD_LIST_DATA_CIPHERS);

                    dbusResponse.clear();

                    if(supportedDataCipher.empty() == false)
                    {
                        dbusResponse.setResponse(BT_OK);

                        for(std::string cipher : supportedDataCipher)
                        {
                            DBusResponse::Item item;

                            dbusResponse.addToItem(item, "cipher", cipher);

                            dbusResponse.add(item);
                        }
                    }
                    else
                        dbusResponse.setResponse("ERROR: No data cipher supported");

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_LIST_PUSHED_DNS))
                {
                    std::vector<VpnClient::IPEntry> pushedDns;

                    logger->logMethod(BT_METHOD_LIST_PUSHED_DNS);

                    dbusResponse.clear();

                    if(vpnClient != nullptr)
                    {
                        pushedDns = vpnClient->getPushedDns();

                        dbusResponse.setResponse(BT_OK);

                        for(VpnClient::IPEntry dns : pushedDns)
                        {
                            DBusResponse::Item item;

                            dbusResponse.addToItem(item, "address", dns.address);

                            if(dns.ipv6)
                                dbusResponse.addToItem(item, "type", "IPv6");
                            else
                                dbusResponse.addToItem(item, "type", "IPv4");

                            dbusResponse.add(item);
                        }
                    }
                    else
                        dbusResponse.setResponse("ERROR: VPN is not connected");

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_RECOVER_NETWORK))
                {
                    std::string result = "";

                    osstring.str("");

                    osstring << BT_METHOD_RECOVER_NETWORK " -> " << result;

                    logger->logMethod(osstring.str());

                    if(status == BLUETIT_STATUS_DIRTY_EXIT)
                    {
                        result = recover_network();
                        
                        dbusResponse.clear();

                        dbusResponse.setResponse(result);

                        dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                    }
                    else if(status != BLUETIT_STATUS_READY)
                    {
                        osstring.str("");

                        osstring << "Operation refused. " << bluetit_status_description();

                        logger->logMethod(osstring.str());

                        dbusResponse.clear();

                        dbusResponse.setResponse(osstring.str());

                        dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                    }
                    else
                    {
                        osstring.str("");

                        osstring << BLUETIT_SHORT_NAME << " does not need a network recovery.";

                        logger->logMethod(osstring.str());

                        dbusResponse.clear();

                        dbusResponse.setResponse(osstring.str());

                        dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                    }

                    send_event(BT_EVENT_END_OF_SESSION, "");
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_SET_OPTIONS))
                {
                    dbusItems = dbusConnector->getVector(dbusMessage);

                    if(dbusItems.empty() == false)
                    {
                        osstring.str("");

                        if(status == BLUETIT_STATUS_READY)
                            message = set_bluetit_options(dbusItems);
                        else
                        {
                            message = "Operation refused. ";
                            message += BLUETIT_SHORT_NAME;
                            message += " is ";

                            if(status == BLUETIT_STATUS_CONNECTED)
                                message += "connected to VPN.";
                            else
                                message += "not accepting options now.";
                        }

                        dbusResponse.clear();

                        if(message != BT_OK)
                            dbusResponse.setResponse(message);
                        else if(airVpnInfo == true)
                        {
                            if(airVpnEnabled == true)
                            {
                                if(airVpnServerPattern.empty() == false)
                                {
                                    osstring.str("");

                                    osstring << BT_DATASET_AIRVPN_SERVER_INFO << " -> " << airVpnServerPattern;

                                    airvpn_server_info(airVpnServerPattern, dbusResponse);

                                    if(dbusResponse.getResponse() == BT_OK)
                                        dbusResponse.setResponse(BT_DATASET_AIRVPN_SERVER_INFO);
                                    else
                                    {
                                        dbusResponse.setResponse(MESSAGE_AIRVPN_SERVER_NOT_FOUND);

                                        osstring << ": not found";
                                    }

                                    logger->logMethod(osstring.str());
                                }
                                else if(airVpnCountryPattern.empty() == false)
                                {
                                    osstring.str("");

                                    osstring << BT_DATASET_AIRVPN_COUNTRY_INFO << " -> " << airVpnCountryPattern;

                                    airvpn_country_info(airVpnCountryPattern, dbusResponse);

                                    if(dbusResponse.getResponse() == BT_OK)
                                        dbusResponse.setResponse(BT_DATASET_AIRVPN_COUNTRY_INFO);
                                    else
                                    {
                                        dbusResponse.setResponse(MESSAGE_AIRVPN_COUNTRY_NOT_FOUND);

                                        osstring << ": not found";
                                    }

                                    logger->logMethod(osstring.str());
                                }
                                else
                                    dbusResponse.setResponse("ERROR: provide either --air-server or --air-country");
                            }
                            else
                                dbusResponse.setResponse(MESSAGE_AIRVPN_NOT_AVAILABLE);
                            
                            airVpnInfo = false;
                        }
                        else if(airVpnList == true)
                        {
                            if(airVpnEnabled == true)
                            {
                                if(airVpnServerPattern.empty() == false)
                                {
                                    osstring.str("");

                                    osstring << BT_DATASET_AIRVPN_SERVER_LIST << " -> " << airVpnServerPattern;

                                    airvpn_server_list(airVpnServerPattern, dbusResponse);

                                    if(dbusResponse.getResponse() == BT_OK)
                                        dbusResponse.setResponse(BT_DATASET_AIRVPN_SERVER_LIST);
                                    else
                                    {
                                        dbusResponse.setResponse(MESSAGE_AIRVPN_SERVER_NOT_FOUND);

                                        osstring << ": not found";
                                    }

                                    logger->logMethod(osstring.str());
                                }
                                else if(airVpnCountryPattern.empty() == false)
                                {
                                    osstring.str("");

                                    osstring << BT_DATASET_AIRVPN_COUNTRY_LIST << " -> " << airVpnCountryPattern;

                                    airvpn_country_list(airVpnCountryPattern, dbusResponse);

                                    if(dbusResponse.getResponse() == BT_OK)
                                        dbusResponse.setResponse(BT_DATASET_AIRVPN_COUNTRY_LIST);
                                    else
                                    {
                                        dbusResponse.setResponse(MESSAGE_AIRVPN_COUNTRY_NOT_FOUND);

                                        osstring << ": not found";
                                    }

                                    logger->logMethod(osstring.str());
                                }
                                else
                                    dbusResponse.setResponse("ERROR: provide either --air-server or --air-country");
                            }
                            else
                                dbusResponse.setResponse(MESSAGE_AIRVPN_NOT_AVAILABLE);
                            
                            airVpnList = false;
                        }
                        else if(airVpnKeyList == true)
                        {
                            if(airVpnEnabled == true)
                            {
                                osstring.str("");

                                osstring << BT_DATASET_AIRVPN_KEY_LIST;

                                airvpn_key_list(airVpnUsername, airVpnPassword, dbusResponse);

                                if(dbusResponse.getResponse() == BT_OK)
                                    dbusResponse.setResponse(BT_DATASET_AIRVPN_KEY_LIST);
                                else
                                {
                                    osstring << ": not found";
                                }

                                logger->logMethod(osstring.str());
                            }
                            else
                                dbusResponse.setResponse(MESSAGE_AIRVPN_NOT_AVAILABLE);
                            
                            airVpnKeyList = false;
                        }
                        else if(airVpnSave == true)
                        {
                            if(airVpnEnabled == true)
                            {
                                osstring.str("");

                                osstring << BT_DATASET_AIRVPN_SAVE;

                                if(!airVpnServerPattern.empty())
                                {
                                    airvpn_server_save(airVpnUsername, airVpnPassword, false, dbusResponse);

                                    airVpnServerPattern = "";
                                }
                                else if(!airVpnCountryPattern.empty())
                                {
                                    airvpn_server_save(airVpnUsername, airVpnPassword, true, dbusResponse);

                                    airVpnCountryPattern = "";
                                }
                                else if(!airVpnKeyName.empty())
                                {
                                    airvpn_key_save(airVpnUsername, airVpnPassword, dbusResponse);

                                    airVpnKeyName = "";
                                }
                                else
                                {
                                    dbusResponse.setResponse("--air-save needs --air-server, --air-country or --air-key to be defined");
                                }

                                if(dbusResponse.getResponse() == BT_OK)
                                    dbusResponse.setResponse(BT_DATASET_AIRVPN_SAVE);
                                else
                                {
                                    osstring << ": not found";
                                }

                                logger->logMethod(osstring.str());
                            }
                            else
                            {
                                dbusResponse.setResponse(MESSAGE_AIRVPN_NOT_AVAILABLE);
                            
                                airVpnSave = false;
                            }
                        }
                        else
                            dbusResponse.setResponse(message);

                        dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                    }
                    else
                        logger->systemLog("DBusConnector: Error getting message");
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_AIRVPN_SET_KEY))
                {
                    std::string result = BT_ERROR;

                    osstring.str("");

                    osstring << BT_METHOD_AIRVPN_SET_KEY << " -> ";

                    dbusItems = dbusConnector->getVector(dbusMessage);

                    if(dbusItems.empty() == false)
                    {
                        if(dbusItems.size() == 1)
                        {
                            customAirVpnKey = dbusItems[0];
                            
                            result = BT_OK;
                        }
                    }
                    else
                    {
                        logger->systemLog("DBusConnector: Error getting message");
                        
                        result = BT_ERROR;
                    }

                    dbusResponse.clear();

                    dbusResponse.setResponse(result);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    osstring << result;

                    logger->logMethod(osstring.str());
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_SET_OPENVPN_PROFILE))
                {
                    std::string result = BT_ERROR;

                    osstring.str("");

                    osstring << BT_METHOD_SET_OPENVPN_PROFILE << " -> ";

                    dbusItems = dbusConnector->getVector(dbusMessage);

                    if(dbusItems.empty() == false)
                    {
                        if(dbusItems.size() == 1)
                            result = set_openvpn_profile(dbusItems[0]);

                        dbusResponse.clear();

                        dbusResponse.setResponse(result);

                        dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                    }
                    else
                        logger->systemLog("DBusConnector: Error getting message");

                    osstring << result;

                    logger->logMethod(osstring.str());
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_AIRVPN_START_CONNECTION))
                {
                    logger->logMethod(BT_METHOD_AIRVPN_START_CONNECTION);

                    connectionThread = new std::thread(start_airvpn_connection);

                    dbusResponse.clear();

                    osstring.str("");

                    if(connectionThread != nullptr)
                    {
                        message = BT_OK;

                        osstring << "OpenVPN3 connection successfully started";
                    }
                    else
                    {
                        message = BT_ERROR;

                        osstring << "ERROR: Cannot create the connection thread. Terminating " << BLUETIT_SHORT_NAME;
                    }

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    if(connectionThread == nullptr)
                    {
                        terminate_client_session();
                        
                        logger->systemLog(osstring.str());
                    
                        cleanup_and_exit(EXIT_FAILURE);
                    }

                    logger->systemLog(osstring.str());
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_START_CONNECTION))
                {
                    logger->logMethod(BT_METHOD_START_CONNECTION);

                    if(persistent_network_lock_mode == NetFilter::Mode::OFF)
                    {
                        try
                        {
                            netFilter->setMode(vpn_network_lock_mode);
                            
                            message = BT_OK;
                        }
                        catch(NetFilterException &e)
                        {
                            osstring.str("");

                            osstring << BT_ERROR << " - Network Lock Error: " << e.what();

                            message = osstring.str();
                        }

                        if(message == BT_OK)
                        {
                            if(netFilter->init())
                            {
                                logger->queueLog("Network filter successfully initialized");
                            
                                netFilter->setup(localNetwork->getLoopbackInterface());
                            }
                            else
                            {
                                osstring.str("");

                                osstring << BT_ERROR << " - Cannot initialize network filter";

                                logger->queueLog(osstring.str());

                                message = osstring.str();
                            }
                        }
                    }
                    else
                        message = BT_OK;

                    if(message == BT_OK)
                    {
                        connectionThread = new std::thread(start_openvpn_connection, maxConnectionRetries);

                        dbusResponse.clear();

                        osstring.str("");

                        if(connectionThread != nullptr)
                        {
                            message = BT_OK;

                            osstring << "OpenVPN3 connection successfully started";
                        }
                        else
                        {
                            message = BT_ERROR;

                            osstring << "ERROR: Cannot create the connection thread. Terminating " << BLUETIT_SHORT_NAME;
                        }
                    }

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    if(connectionThread == nullptr)
                    {
                        terminate_client_session();
                        
                        logger->systemLog(osstring.str());
                    
                        cleanup_and_exit(EXIT_FAILURE);
                    }

                    logger->systemLog(osstring.str());
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_STOP_CONNECTION))
                {
                    logger->logMethod(BT_METHOD_STOP_CONNECTION);

                    message = stop_openvpn_connection();

                    dbusResponse.clear();

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    osstring.str("");

                    if(message == BT_OK)
                        osstring << "OpenVPN3 connection thread successfully terminated";
                    else
                    {
                        osstring << "Failed to stop OpenVPN3 connection thread: " << message;

                        if(vpnClient != nullptr)
                        {
                            delete vpnClient;

                            vpnClient = nullptr;
                        }

                        reset_settings();
                    }

                    terminate_client_session();

                    logger->systemLog(osstring.str());
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_PAUSE_CONNECTION) || dbusConnector->isMethod(dbusMessage, BT_METHOD_SESSION_PAUSE))
                {
                    logger->logMethod(BT_METHOD_PAUSE_CONNECTION);

                    message = pause_openvpn_connection();

                    dbusResponse.clear();

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    if(message == BT_OK)
                        OPENVPN_LOG("OpenVPN3 connection paused");

                    if(dbusConnector->isMethod(dbusMessage, BT_METHOD_SESSION_PAUSE))
                    {
                        logger->flushLog();

                        send_event(BT_EVENT_END_OF_SESSION, "");
                    }
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_RESUME_CONNECTION) || dbusConnector->isMethod(dbusMessage, BT_METHOD_SESSION_RESUME))
                {
                    logger->logMethod(BT_METHOD_RESUME_CONNECTION);

                    message = resume_openvpn_connection();

                    dbusResponse.clear();

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    if(message == BT_OK)
                        OPENVPN_LOG("OpenVPN3 connection resumed");

                    if(dbusConnector->isMethod(dbusMessage, BT_METHOD_SESSION_RESUME))
                    {
                        logger->flushLog();

                        send_event(BT_EVENT_END_OF_SESSION, "");
                    }
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_RECONNECT_CONNECTION) || dbusConnector->isMethod(dbusMessage, BT_METHOD_SESSION_RECONNECT))
                {
                    logger->logMethod(BT_METHOD_RECONNECT_CONNECTION);

                    message = reconnect_openvpn();

                    dbusResponse.clear();

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    if(message == BT_OK)
                        OPENVPN_LOG("Reconnecting OpenVPN3");

                    if(dbusConnector->isMethod(dbusMessage, BT_METHOD_SESSION_RECONNECT))
                    {
                        logger->flushLog();

                        send_event(BT_EVENT_END_OF_SESSION, "");
                    }
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_CONNECTION_STATS))
                {
                    connection_stats(dbusResponse);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_ENABLE_NETWORK_LOCK))
                {
                    logger->logMethod(BT_METHOD_ENABLE_NETWORK_LOCK);

                    dbusResponse.clear();

                    if(persistent_network_lock_mode != NetFilter::Mode::OFF)
                    {
                        message = "ERROR: Operation ignored. Persistent Network Lock and Filter is enabled.";
                    }
                    else if(netFilter->isNetworkLockAvailable())
                    {
                        if(netFilter->isNetworkLockEnabled() == false)
                        {
                            if(enable_network_lock(NETLOCKMODE_SESSION) == true)
                                message = BT_OK;
                            else
                                message = "ERROR: Cannot enable network filter and lock";
                        }
                        else
                            message = "ERROR: Network filter and lock is already enabled";
                    }
                    else
                        message = "ERROR: Network filter and lock is not available";

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    if(message != BT_OK)
                        logger->systemLog(message);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_DISABLE_NETWORK_LOCK))
                {
                    logger->logMethod(BT_METHOD_DISABLE_NETWORK_LOCK);

                    dbusResponse.clear();

                    if(persistent_network_lock_mode != NetFilter::Mode::OFF)
                    {
                        message = "ERROR: Operation ignored. Persistent Network Lock and Filter is enabled.";
                    }
                    else if(netFilter->isNetworkLockAvailable())
                    {
                        if(netFilter->isNetworkLockEnabled() == true)
                        {
                            if(disable_network_lock() == true)
                                message = BT_OK;
                            else
                                message = "ERROR: Cannot disable network filter and lock";
                        }
                        else
                            message = "ERROR: Network filter and lock is already disabled";
                    }
                    else
                        message = "ERROR: Network filter and lock is not available";

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);

                    if(message != BT_OK)
                        logger->systemLog(message);
                }
                else if(dbusConnector->isMethod(dbusMessage, BT_METHOD_NETWORK_LOCK_STATUS))
                {
                    std::string netLockModeDescription = "";

                    osstring.str("");

                    osstring << BT_METHOD_NETWORK_LOCK_STATUS << " -> ";

                    dbusResponse.clear();

                    if(netFilter != nullptr)
                    {
                        netLockModeDescription = " (using ";

                        switch(netFilter->getMode())
                        {
                            case NetFilter::Mode::NFTABLES:
                            {
                                netLockModeDescription += "nftables";
                            }
                            break;

                            case NetFilter::Mode::IPTABLES:
                            {
                                netLockModeDescription += "iptables";
                            }
                            break;

                            case NetFilter::Mode::PF:
                            {
                                netLockModeDescription += "pf";
                            }
                            break;

                            case NetFilter::Mode::OFF:
                            {
                                netLockModeDescription = " (off";
                            }
                            break;

                            default:
                            {
                                netLockModeDescription = " (unknown";
                            }
                            break;
                        }
                        
                        netLockModeDescription += ")";
                    }
                    else
                        netLockModeDescription = "";

                    if(persistent_network_lock_mode != NetFilter::Mode::OFF)
                    {
                        message = "Persistent Network Lock and Filter is enabled." + netLockModeDescription;
                    }
                    else if(netFilter->isNetworkLockAvailable())
                    {
                        if(netFilter->isNetworkLockEnabled() == true)
                        {
                            message = "Network filter and lock is enabled" + netLockModeDescription;

                            if(persistent_network_lock_mode != NetFilter::Mode::OFF)
                                message += " (persistent mode)";
                        }
                        else
                            message = "Network filter and lock is disabled";
                    }
                    else
                        message = "Network filter and lock is not available";

                    osstring << message;

                    logger->logMethod(osstring.str());

                    dbusResponse.setResponse(message);

                    dbusConnector->replyToMessage(dbusMessage, dbusResponse);
                }

                dbusConnector->unreferenceMessage(dbusMessage);
            }
        }
    }
    catch(DBusConnectorException &e)
    {
        osstring.str("");

        osstring << "DBusConnectorException: " << e.what();

        logger->systemLog(osstring.str());

        cleanup_and_exit(EXIT_FAILURE);
    }

    return 0;
}
