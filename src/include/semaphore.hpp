/*
 * semaphore.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SEMAPHORE_CLASS_HPP
#define SEMAPHORE_CLASS_HPP

#include <thread>
#include <mutex>
#include <chrono>
#include <condition_variable>
#include <string>

class Semaphore
{
    public:
    
    enum Status
    {
        FORBIDDEN,
        PERMITTED,
        PENDING_JOB_ABORTED
    };

    Semaphore(Status s = PERMITTED);
    ~Semaphore();

    void forbid();
    void forbid(const std::string &reason);
    void abortPendingJob();
    void abortPendingJob(const std::string &reason);
    void permit();
    void wait();
    void wait_for(const std::chrono::seconds &s);

    Status status();

    bool isPermitted();
    bool isForbidden();
    bool isPendingJobAborted();
    std::string getForbidReason();
    std::string getAbortReason();

    private:

    std::mutex sem_lock;
    std::condition_variable cond_var;
    Status sem_status;
    std::string forbid_reason;
    std::string abort_reason;
};

#endif
