/*
 * airvpntools.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef AIRVPNTOOLS_CLASS_HPP
#define AIRVPNTOOLS_CLASS_HPP

#include <string>
#include <vector>
#include <map>

#include <cryptopp/cryptlib.h>
#include <libxml/tree.h>

class AirVPNTools
{
    public:

    enum InitSystemType
    {
        SystemVinit,
        Systemd,
        Launchd,
        Unknown
    };

    enum AirVPNServerError
    {
        OK,
        NO_BOOTSERVER,
        NO_RSA_MODULUS,
        NO_RSA_EXPONENT,
        RSA_ERROR,
        AES_ENCRYPTION_ERROR,
        AES_DECRYPTION_ERROR,
        NO_PARAMETERS,
        NO_ACTION,
        CANNOT_RESOLVE_HOST,
        CONNECTION_ERROR,
        HTTP_ERROR,
        EMPTY_DOCUMENT,
        SEND_ERROR,
        RECEIVE_ERROR,
        TIMEOUT,
        END_OF_BOOTSERVER_LIST,
        UNKNOWN
    };

    struct BootServer
    {
        std::string url;
        int entry;
        bool ipv6;
    
        bool operator > (const BootServer &s) const
        {
            return (ipv6 > s.ipv6 && entry > s.entry);
        }
    };

    const static long ONE_DECIMAL_KILO = 1000;
    const static long ONE_DECIMAL_MEGA = 1000000;
    const static long ONE_DECIMAL_GIGA = 1000000000;
    const static long ONE_KILOBYTE = 1024; // Real KB 2^10
    const static long ONE_MEGABYTE = 1048576; // Real MB 2^20
    const static long ONE_GIGABYTE = 1073741824; // Real GB 2^30

    const static char *LOCATION_STATUS_OK;

    private:

    static size_t curlWriteCallback(void *data, size_t size, size_t nmemb, void *userp);

    const int SECRETKEY_SIZE = 32;    // 32 bytes == 256 bits
    const static long SERVER_CONNECTION_TIMEOUT = 15L;
    const static long SERVER_READ_TIMEOUT = 30L;

    int bootServerEntryIndex;
    static bool bootServerIPv6Mode;
    std::vector<BootServer> bootServerList;
    std::map<std::string, std::string> requestDocumentParameter;
    std::string curlReadBuffer, documentData;
    std::string rsaModulus, rsaExponent;
    std::string documentRequestError;

    std::string mapToAirVPNParameters(const std::map<std::string, std::string> &map);

    public:

    AirVPNTools();
    ~AirVPNTools();

    static InitSystemType getInitSystemType();
    static std::string getInitSystemName(InitSystemType type);
    static int getLoad(long byteBandWidth, long maxMBitBandWidth);
    static std::string formatTransferRate(long long rate, bool decimal = false);
    static std::string formatDataVolume(long long bytes, bool decimal = false);
    static std::string formatTime(long s);

    void resetBootServers();
    void addBootServer(const std::string &server);
    static void setBootServerIPv6Mode(bool enable);

    void setRSAModulus(const std::string &modulus);
    void setRSAExponent(const std::string &exponent);

    void resetAirVPNDocumentRequest();
    void setAirVPNDocumentParameter(const std::string &key, const std::string &value);
    AirVPNServerError requestAirVPNDocument();
    std::string getRequestedDocument();
    std::string getDocumentRequestErrorDescription();

    static std::string convertXmlEntities(std::string xml);
    static std::vector<std::string> split(const std::string &s, const std::string &delimiter);
    static std::string trim(std::string s);
    static bool iequals(const std::string &str1, const std::string &str2);
    static std::string unquote(const std::string &s);
    static std::string toLower(std::string s);
    static std::string toUpper(std::string s);
    static std::string pad(const std::string &str, const size_t size, const char padChar = ' ');
    static std::string padRight(const std::string &str, const size_t size, const char padChar = ' ');
    static std::string normalizeBoolValue(const std::string &value, const std::string &trueValue, const std::string &falseValue);

    static xmlNode *findXmlNode(const xmlChar *name, const xmlNode *startNode);
    static std::string getXmlAttribute(const xmlNode *node, const std::string &name, const std::string &defaultValue = "");

    static std::map<std::string, std::string> detectLocation();
};

#endif
