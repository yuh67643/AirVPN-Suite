/*
 * airvpnserver.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef AIRVPNSERVER_CLASS_HPP
#define AIRVPNSERVER_CLASS_HPP

#include "countrycontinent.hpp"

#include <string>
#include <vector>
#include <map>

class AirVPNServer
{
    public:

    const static int WORST_SCORE = 99999999;

    AirVPNServer(const std::string &resourceDirectory);
    ~AirVPNServer();

    std::string getName();
    void setName(const std::string &n);
    std::string getContinent();
    void setContinent(const std::string &c);
    std::string getCountryCode();
    std::string getCountryName();
    void setCountryCode(const std::string &c);
    std::string getRegion();
    void setRegion(const std::string &r);
    std::string getLocation();
    void setLocation(const std::string &l);
    long long getBandWidth();
    void setBandWidth(long long b);
    long long getMaxBandWidth();
    void setMaxBandWidth(long long b);
    long long getEffectiveBandWidth();
    int getUsers();
    void setUsers(int u);
    int getMaxUsers();
    void setMaxUsers(int u);
    double getUserLoad();
    bool getSupportIPv4();
    void setSupportIPv4(bool s);
    bool getSupportIPv6();
    void setSupportIPv6(bool s);
    std::map<int, std::string> getEntryIPv4();
    void setEntryIPv4(const std::map<int, std::string> &l);
    void setEntryIPv4(int entry, const std::string &IPv4);
    std::map<int, std::string> getEntryIPv6();
    void setEntryIPv6(const std::map<int, std::string> &l);
    void setEntryIPv6(int entry, const std::string &IPv6);
    std::string getExitIPv4();
    void setExitIPv4(const std::string &e);
    std::string getExitIPv6();
    void setExitIPv6(const std::string &e);
    std::string getWarningOpen();
    void setWarningOpen(const std::string &w);
    std::string getWarningClosed();
    void setWarningClosed(const std::string &w);
    int getScoreBase();
    void setScoreBase(int s);
    bool getSupportCheck();
    void setSupportCheck(bool s);
    int getLoad();
    std::vector<int> getTlsCiphers();
    std::string getTlsCipherNames();
    void setTlsCiphers(const std::vector<int> &list);
    bool hasTlsCipher(int c);
    bool hasTlsCipher(const std::vector<int> &cipherList);
    std::vector<int> getTlsSuiteCiphers();
    std::string getTlsSuiteCipherNames();
    void setTlsSuiteCiphers(const std::vector<int> &list);
    bool hasTlsSuiteCipher(int c);
    bool hasTlsSuiteCipher(const std::vector<int> &cipherList);
    std::vector<int> getDataCiphers();
    std::string getDataCipherNames();
    void setDataCiphers(std::vector<int> list);
    bool hasDataCipher(int c);
    bool hasDataCipher(const std::vector<int> &cipherList);
    void computeServerScore();
    int getScore();
    void setScore(int s);
    bool isAvailable();
    bool isAvailable(const std::vector<int> &cipherList);

    private:

    CountryContinent *countryContinent;

    std::string name;
    std::string continent;
    std::string countryCode;
    std::string region;
    std::string location;
    long long bandWidth;
    long long maxBandWidth;
    int users;
    int maxUsers;
    bool supportIPv4;
    bool supportIPv6;
    std::map<int, std::string> entryIPv4;
    std::map<int, std::string> entryIPv6;
    std::string exitIPv4;
    std::string exitIPv6;
    std::string warningOpen;
    std::string warningClosed;
    int scoreBase;
    int score;
    bool supportCheck;
    int group;
    std::vector<int> tlsCiphers;
    std::vector<int> tlsSuiteCiphers;
    std::vector<int> dataCiphers;

    void init();
};

#endif
