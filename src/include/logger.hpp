/*
 * logger.hpp
 *
 * This file is part of AirVPN's hummingbird Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <syslog.h>
#include <string>
#include <vector>

#include "semaphore.hpp"

#if defined(OPENVPN_PLATFORM_LINUX)

#include "dbusconnector.hpp"

#endif

#define OPENVPN_LOG_GLOBAL // use global rather than thread-local  object pointer

#include <openvpn/log/logbasesimple.hpp>

using namespace openvpn;

class Logger : public LogBaseSimple
{
    public:

    Logger(const std::string &logId);
    ~Logger();

    void log(const std::string &str) override;
    void log(const char *msg);

    void resetQueue();
    void queueLog(const std::string &str);
    void queueLog(const char *msg);
    void flushLog();

    void systemLog(const std::string &str);
    void systemLog(const char *msg);
    void logMethod(const std::string &str);
    void logMethod(const char *msg);

#if defined(OPENVPN_PLATFORM_LINUX)

    void enableDBusMode(DBusConnector *dbusConn);

#endif

    private:

    std::string logIdentifier;
    std::vector<std::string> logMessage;
    Semaphore *logSemaphore = nullptr;

#if defined(OPENVPN_PLATFORM_LINUX)

    DBusConnector *dbusConnector = nullptr;

    std::vector<std::string> dbusItems;

#endif
};

Logger::Logger(const std::string &logId)
{
    logIdentifier = logId;

    openlog(logIdentifier.c_str(), LOG_NDELAY, LOG_DAEMON);

    dbusConnector = nullptr;

    logSemaphore = new Semaphore();

    logMessage.clear();
}

Logger::~Logger()
{
    flushLog();

    closelog();

    if(logSemaphore->isForbidden())
          logSemaphore->abortPendingJob();

    if(logSemaphore != nullptr)
        delete logSemaphore;
}

#if defined(OPENVPN_PLATFORM_LINUX)

void Logger::enableDBusMode(DBusConnector *dbusConn)
{
    dbusConnector = dbusConn;
}

#endif

void Logger::log(const std::string &str)
{
    if(logSemaphore->isForbidden())
        logSemaphore->wait();

    if(str != "")
        logMessage.push_back(str);

    flushLog();
}

void Logger::log(const char *msg)
{
    std::string str = msg;

    log(str);
}

void Logger::resetQueue()
{
    if(logSemaphore->isForbidden())
        logSemaphore->wait();

    logMessage.clear();
}

void Logger::queueLog(const std::string &str)
{
    if(logSemaphore->isForbidden())
        logSemaphore->wait();

    logMessage.push_back(str);
}

void Logger::queueLog(const char *msg)
{
    std::string str = msg;

    queueLog(str);
}

void Logger::flushLog()
{
    if(logSemaphore->isForbidden())
        logSemaphore->wait();

    logSemaphore->forbid();

    for(std::string s : logMessage)
    {
#if defined(OPENVPN_PLATFORM_LINUX)

        if(dbusConnector)
        {
            dbusItems.clear();

            dbusItems.push_back(s.c_str());

            dbusConnector->callMethod(BT_CLIENT_BUS_NAME, BT_CLIENT_OBJECT_PATH_NAME, BT_METHOD_LOG, dbusItems);
        }
        else
            syslog(LOG_DEBUG, "Logger::log() Cannot send log to client: dbusConnection is null.");

#endif

        syslog(LOG_DEBUG, s.c_str());
    }
    
    logMessage.clear();

    logSemaphore->permit();
}

void Logger::systemLog(const std::string &str)
{
    systemLog(str.c_str());
}

void Logger::systemLog(const char *msg)
{
    if(msg == NULL || strlen(msg) == 0)
        return;

    syslog(LOG_DEBUG, msg);
}

void Logger::logMethod(const std::string &str)
{
    std::string logMsg = "Requested method \"" + str + "\"";

    syslog(LOG_DEBUG, logMsg.c_str());
}

void Logger::logMethod(const char *msg)
{
    std::string string = msg;

    logMethod(string);
}
