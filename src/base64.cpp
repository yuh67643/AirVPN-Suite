/*
 * base64.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/base64.hpp"

static const BYTE from_base64[] = {
                                    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
                                    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
                                    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,  62, 255,  62, 255,  63,
                                     52,  53,  54,  55,  56,  57,  58,  59,  60,  61, 255, 255, 255, 255, 255, 255,
                                    255,   0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
                                     15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25, 255, 255, 255, 255,  63,
                                    255,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,
                                     41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51, 255, 255, 255, 255, 255
                                  };

static const char to_base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

std::string Base64::encode(const std::vector<BYTE> &byteVector)
{
    if(byteVector.empty())
        return "";

    return encode(&byteVector[0], (unsigned int)byteVector.size());
}

std::string Base64::encode(const BYTE *byteVector, unsigned int vectorLength)
{
    std::string output;
    BYTE b3[3], b4[4];
    size_t padding = 0, index;
    size_t output_size;

    output_size= vectorLength;

    while((output_size % 3) != 0)
    {
        output_size++;
        padding++;
    }

    output_size = 4 * output_size / 3;

    output.reserve(output_size);

    for(unsigned int i = 0; i < output_size / 4; i++)
    {
        index = i * 3;

        b3[0] = (index < vectorLength) ? byteVector[index] : 0;
        b3[1] = (index + 1 < vectorLength) ? byteVector[index + 1] : 0;
        b3[2] = (index + 2 < vectorLength) ? byteVector[index + 2] : 0;

        b4[0] = ((b3[0] & 0xfc) >> 2);
        b4[1] = ((b3[0] & 0x03) << 4) + ((b3[1] & 0xf0) >> 4);
        b4[2] = ((b3[1] & 0x0f) << 2) + ((b3[2] & 0xc0) >> 6);
        b4[3] = ((b3[2] & 0x3f) << 0);

        output += to_base64[b4[0]];
        output += to_base64[b4[1]];
        output += to_base64[b4[2]];
        output += to_base64[b4[3]];
    }

    for(size_t i = 0; i < padding; ++i)
        output[output_size - i - 1] = '=';

    return output;
}

std::vector<BYTE> Base64::decodeToByte(std::string encodedString)
{
    std::vector<BYTE> output;
    BYTE b3[3], b4[4];
    size_t encoded_size;

    while((encodedString.size() % 4) != 0)
        encodedString += '=';

    encoded_size = encodedString.size();

    output.reserve(3 * encoded_size / 4);

    for(size_t i = 0; i < encoded_size; i += 4)
    {
        b4[0] = (encodedString[i] <= 'z') ? from_base64[(int)encodedString[i]] : 0xff;
        b4[1] = (encodedString[i + 1] <= 'z') ? from_base64[(int)encodedString[i + 1]] : 0xff;
        b4[2] = (encodedString[i + 2] <= 'z') ? from_base64[(int)encodedString[i + 2]] : 0xff;
        b4[3] = (encodedString[i + 3] <= 'z') ? from_base64[(int)encodedString[i + 3]] : 0xff;

        b3[0] = ((b4[0] & 0x3f) << 2) + ((b4[1] & 0x30) >> 4);
        b3[1] = ((b4[1] & 0x0f) << 4) + ((b4[2] & 0x3c) >> 2);
        b3[2] = ((b4[2] & 0x03) << 6) + ((b4[3] & 0x3f) >> 0);

        if(b4[1] != 0xff)
            output.push_back(b3[0]);

        if(b4[2] != 0xff)
            output.push_back(b3[1]);

        if(b4[3] != 0xff)
            output.push_back(b3[2]);
    }

    return output;
}


std::string Base64::decodeToString(const std::string &encodedString)
{
    std::vector<BYTE> decodedVector;
    std::string ret;

    decodedVector = decodeToByte(encodedString);

    for(BYTE byte : decodedVector)
        ret += byte;

    return ret;
}
