/*
 * semaphore.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/semaphore.hpp"

Semaphore::Semaphore(Status s)
{
    sem_status = s;
    forbid_reason = "";
    abort_reason = "";
}

Semaphore::~Semaphore()
{
}

void Semaphore::forbid()
{
    std::unique_lock<std::mutex> lock(sem_lock);

    sem_status = FORBIDDEN;

    cond_var.notify_one();
}

void Semaphore::forbid(const std::string &reason)
{
    forbid_reason = reason;
    
    forbid();
}

void Semaphore::abortPendingJob()
{
    std::unique_lock<std::mutex> lock(sem_lock);

    sem_status = PENDING_JOB_ABORTED;

    cond_var.notify_one();
}

void Semaphore::abortPendingJob(const std::string &reason)
{
    abort_reason = reason;
    
    abort();
}

void Semaphore::permit()
{
    std::unique_lock<std::mutex> lock(sem_lock);

    sem_status = PERMITTED;

    cond_var.notify_one();
    
    forbid_reason = "";
    abort_reason = "";
}

void Semaphore::wait()
{
    std::unique_lock<std::mutex> lock(sem_lock);

    if(sem_status != FORBIDDEN)
        return;

    cond_var.wait(lock);
}

void Semaphore::wait_for(const std::chrono::seconds &s)
{
    std::unique_lock<std::mutex> lock(sem_lock);

    if(sem_status != FORBIDDEN)
        return;

    if(cond_var.wait_for(lock, std::chrono::seconds(s)) == std::cv_status::timeout)
        sem_status = PERMITTED;
}

Semaphore::Status Semaphore::status()
{
    return sem_status;
}

bool Semaphore::isPermitted()
{
    return (sem_status == PERMITTED);
}

bool Semaphore::isForbidden()
{
    return (sem_status == FORBIDDEN);
}

bool Semaphore::isPendingJobAborted()
{
    return (sem_status == PENDING_JOB_ABORTED);
}

std::string Semaphore::getForbidReason()
{
    return forbid_reason;
}
