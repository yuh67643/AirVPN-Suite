/*
 * airvpntools.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <sstream>
#include <regex>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/integer.h>
#include <cryptopp/modes.h>
#include <cryptopp/rsa.h>
#include <cryptopp/aes.h>
#include <cryptopp/filters.h>
#include <cryptopp/base64.h>
#include <cryptopp/osrng.h>
#include <curl/curl.h>
#include "include/airvpntools.hpp"
#include "include/base64.hpp"
#include "include/execproc.h"

#if defined(MAKING_BLUETIT)

extern void global_syslog(const std::string &s);

#endif

const char *AirVPNTools::LOCATION_STATUS_OK = "OK";
bool AirVPNTools::bootServerIPv6Mode = false;

AirVPNTools::AirVPNTools()
{
    rsaModulus = "";
    rsaExponent = "";

    bootServerEntryIndex = 0;

    bootServerList.clear();
    requestDocumentParameter.clear();
}

AirVPNTools::~AirVPNTools()
{
}

AirVPNTools::InitSystemType AirVPNTools::getInitSystemType()
{
    InitSystemType initType = InitSystemType::Unknown;
    char binpath[64], sm_name[32];
    const char *sm_proc_file = "/proc/1/comm";
    int last_char;
    FILE *sm_comm_file;

    if(access(sm_proc_file, F_OK) == 0)
    {
        sm_comm_file = fopen(sm_proc_file, "r");

        if(sm_comm_file != NULL)
        {
            if(fgets(sm_name, sizeof(sm_name) / sizeof(sm_name[0]), sm_comm_file) == NULL)
            {
                fclose(sm_comm_file);

                return InitSystemType::Unknown;
            }

            fclose(sm_comm_file);
        }
        else
            return InitSystemType::Unknown;

    }
    else
    {
        get_exec_path("ps", binpath);

        if(strcmp(binpath, "") != 0)
        {
            if(execute_process(NULL, sm_name, binpath, "-o", "comm=", "1", NULL) != 0)
                strcpy(sm_name ,"");
        }
        else
            strcpy(sm_name, "");
    }

    if(strcmp(sm_name, "") != 0)
    {
        last_char = strlen(sm_name) - 1;

        if(sm_name[last_char] == '\n')
            sm_name[last_char] = '\0';

        if(strcmp(sm_name, "init") == 0)
            initType = SystemVinit;
        else if(strcmp(sm_name, "systemd") == 0)
            initType = Systemd;
        else if(strcmp(sm_name, "/sbin/launchd") == 0)
            initType = Launchd;
        else
            initType = Unknown;
    }
    else
        initType = Unknown;

    return initType;
}

std::string  AirVPNTools::getInitSystemName(InitSystemType type)
{
    std::string name = "";

    switch(type)
    {
        case SystemVinit:
        {
            name = "System V style init";
        }
        break;

        case Systemd:
        {
            name = "systemd";
        }
        break;

        case Launchd:
        {
            name = "launchd";
        }
        break;

        default:
        {
            name = "unknown";
        }
        break;
    }

    return name;
}

void AirVPNTools::resetBootServers()
{
    bootServerEntryIndex = 0;

    bootServerList.clear();
}

void AirVPNTools::setBootServerIPv6Mode(bool enable)
{
    bootServerIPv6Mode = enable;
}

void AirVPNTools::addBootServer(const std::string &server)
{
    std::string path;
    std::size_t schemePosition;
    BootServer bootServer;
    
    bootServer.url = server;
    bootServer.entry = bootServerEntryIndex++;

    schemePosition = server.find("://");
    
    if(schemePosition != std::string::npos)
        path = server.substr(schemePosition + 3);
    else
        path = server;

    if(path.find(":") != std::string::npos)
        bootServer.ipv6 = true;
    else
        bootServer.ipv6 = false;

    bootServerList.push_back(bootServer);
}

void AirVPNTools::setRSAModulus(const std::string &modulus)
{
    rsaModulus = modulus;
}

void AirVPNTools::setRSAExponent(const std::string &exponent)
{
    rsaExponent = exponent;
}

void AirVPNTools::resetAirVPNDocumentRequest()
{
    requestDocumentParameter.clear();
}

void AirVPNTools::setAirVPNDocumentParameter(const std::string &key, const std::string &value)
{
    if(key.empty() || value.empty())
        return;

    requestDocumentParameter.insert(std::make_pair(key, value));
}

AirVPNTools::AirVPNServerError AirVPNTools::requestAirVPNDocument()
{
    AirVPNTools::AirVPNServerError result = UNKNOWN;

    std::string assocParamS, bytesParamS, aesDataIn, bytesParamD, urlParameters, manifest;

#if defined(CRYPTOPP_NO_GLOBAL_BYTE)
    CryptoPP::byte secretKey[SECRETKEY_SIZE];
    CryptoPP::byte iv[CryptoPP::AES::BLOCKSIZE];
#else
    byte secretKey[SECRETKEY_SIZE];
    byte iv[CryptoPP::AES::BLOCKSIZE];
#endif

    CryptoPP::AutoSeededRandomPool secureRandom;
    CryptoPP::Integer rsaModInt, rsaExpInt;
    CryptoPP::RSA::PublicKey rsaPublicKey;
    CryptoPP::RSAES_PKCS1v15_Encryptor rsaCipher;
    CryptoPP::CBC_Mode<CryptoPP::AES>::Encryption aesEncryptor;
    CryptoPP::CBC_Mode<CryptoPP::AES>::Decryption aesDecryptor;

    std::vector<BootServer> bootList;
    CURL *curl;
    CURLcode curlResult;
    struct curl_slist *httpHeader;
    std::ostringstream httpLength;
    int serverNdx;

    documentRequestError = "";
    curlReadBuffer = "";
    documentData = "";

    if(bootServerList.size() == 0)
    {
        documentRequestError = "No AirVPN bootserver provided";

        return NO_BOOTSERVER;
    }

    if(rsaModulus.empty())
    {
        documentRequestError = "No RSA modulus provided";

        return NO_RSA_MODULUS;
    }

    if(rsaExponent.empty())
    {
        documentRequestError = "No RSA exponent provided";

        return NO_RSA_EXPONENT;
    }

    if(requestDocumentParameter.size() == 0)
    {
        documentRequestError = "No document request parameters provided";

        return NO_PARAMETERS;
    }

    if(requestDocumentParameter.find("act") == requestDocumentParameter.end())
    {
        documentRequestError = "No action provided in document request parameters";

        return NO_ACTION;
    }

    CryptoPP::StringSource rsaMod64(rsaModulus, true, new CryptoPP::Base64Decoder());
    CryptoPP::StringSource rsaExp64(rsaExponent, true, new CryptoPP::Base64Decoder());

    rsaModInt = CryptoPP::Integer(rsaMod64, rsaMod64.MaxRetrievable());
    rsaExpInt = CryptoPP::Integer(rsaExp64, rsaExp64.MaxRetrievable());

    secureRandom.GenerateBlock(secretKey, sizeof(secretKey));
    secureRandom.GenerateBlock(iv, sizeof(iv));

    assocParamS = Base64::encode((BYTE *)"key", 3);
    assocParamS += ":";
    assocParamS += Base64::encode((BYTE *)secretKey, sizeof(secretKey));
    assocParamS += "\n";

    assocParamS += Base64::encode((BYTE *)"iv", 2);
    assocParamS += ":";
    assocParamS += Base64::encode((BYTE *)iv, sizeof(iv));
    assocParamS += "\n";

    rsaPublicKey.Initialize(rsaModInt, rsaExpInt);

    rsaCipher = CryptoPP::RSAES_PKCS1v15_Encryptor(rsaPublicKey);

    try
    {
        CryptoPP::StringSource(assocParamS, true, new CryptoPP::PK_EncryptorFilter(secureRandom, rsaCipher, new   CryptoPP::StringSink(bytesParamS)));
    }
    catch(const CryptoPP::Exception& e)
    {
        documentRequestError = "RSA error: ";
        documentRequestError += e.what();

        return RSA_ERROR;
    }

    aesDataIn = mapToAirVPNParameters(requestDocumentParameter);

    aesEncryptor.SetKeyWithIV(secretKey, sizeof(secretKey), iv);

    try
    {
        CryptoPP::StringSource(aesDataIn, true, new CryptoPP::StreamTransformationFilter(aesEncryptor, new CryptoPP::StringSink(bytesParamD), CryptoPP::StreamTransformationFilter::PKCS_PADDING));
    }
    catch(const CryptoPP::Exception& e)
    {
        documentRequestError = "AES encryption error: ";
        documentRequestError += e.what();

        return AES_ENCRYPTION_ERROR;
    }

    curl = curl_easy_init();

    if(curl)
    {
        urlParameters = "s=";
        urlParameters += Base64::encode((BYTE *)bytesParamS.c_str(), bytesParamS.length());
        urlParameters += "&d=";
        urlParameters += Base64::encode((BYTE *)bytesParamD.c_str(), bytesParamD.length());

        curlResult = (CURLcode)-1;

        bootList.clear();

        for(serverNdx = 0; serverNdx < bootServerList.size(); serverNdx++)
        {
            if(bootServerList[serverNdx].ipv6 == false)
                bootList.push_back(bootServerList[serverNdx]);
            else if(bootServerIPv6Mode == true)
                bootList.push_back(bootServerList[serverNdx]);
        }

        std::sort(bootList.begin(), bootList.end(), std::greater<BootServer>());

        for(serverNdx = 0; serverNdx < bootList.size() and curlResult != CURLE_OK; serverNdx++)
        {
#if defined(MAKING_BLUETIT)

            global_syslog("Trying connection to AirVPN bootstrap server at " + bootList[serverNdx].url);

#endif

            curl_easy_setopt(curl, CURLOPT_URL, bootList[serverNdx].url.c_str());

            httpHeader = NULL;

            httpHeader = curl_slist_append(httpHeader, "Accept:");
            httpHeader = curl_slist_append(httpHeader, "Content-Type: application/x-www-form-urlencoded");

            httpLength.str("");

            httpLength << "Content-Length: " << urlParameters.length();

            httpHeader = curl_slist_append(httpHeader, httpLength.str().c_str());

            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, httpHeader);
            curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, SERVER_CONNECTION_TIMEOUT);
            curl_easy_setopt(curl, CURLOPT_TIMEOUT, SERVER_READ_TIMEOUT);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, urlParameters.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteCallback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &curlReadBuffer);

            curlResult = curl_easy_perform(curl);

            switch(curlResult)
            {
                case CURLE_OK:
                {
                    documentRequestError = "Ok";

                    result = OK;
                }
                break;

                case CURLE_COULDNT_RESOLVE_HOST:
                {
                    documentRequestError = "Cannot resolve host: ";
                    documentRequestError += curl_easy_strerror(curlResult);

                    result = CANNOT_RESOLVE_HOST;
                }
                break;

                case CURLE_COULDNT_CONNECT:
                {
                    documentRequestError = "Cannot connect host: ";
                    documentRequestError += curl_easy_strerror(curlResult);

                    result = CONNECTION_ERROR;
                }
                break;

                case CURLE_HTTP_RETURNED_ERROR:
                {
                    documentRequestError = "HTTP error: ";
                    documentRequestError += curl_easy_strerror(curlResult);

                    result = HTTP_ERROR;
                }
                break;

                case CURLE_GOT_NOTHING:
                {
                    documentRequestError = "Host returned an empty document: ";
                    documentRequestError += curl_easy_strerror(curlResult);

                    result = EMPTY_DOCUMENT;
                }
                break;

                case CURLE_SEND_ERROR:
                {
                    documentRequestError = "Send error: ";
                    documentRequestError += curl_easy_strerror(curlResult);

                    result = SEND_ERROR;
                }
                break;

                case CURLE_RECV_ERROR:
                {
                    documentRequestError = "Receive error: ";
                    documentRequestError += curl_easy_strerror(curlResult);

                    result = RECEIVE_ERROR;
                }
                break;

                case CURLE_OPERATION_TIMEDOUT:
                {
                    documentRequestError = "Connection timeout: ";
                    documentRequestError += curl_easy_strerror(curlResult);

                    result = TIMEOUT;
                }
                break;

                default:
                {
                    documentRequestError = "Unknown error: ";
                    documentRequestError += curl_easy_strerror(curlResult);

                    result = UNKNOWN;
                }
            }

#if defined(MAKING_BLUETIT)

            if(result != OK)
                global_syslog(documentRequestError);

#endif
        }

        if(serverNdx == bootList.size())
            result = END_OF_BOOTSERVER_LIST;

        curl_easy_cleanup(curl);
        curl_slist_free_all(httpHeader);

        if(result == OK)
        {
            aesDecryptor.SetKeyWithIV(secretKey, sizeof(secretKey), iv);

            try
            {
                CryptoPP::StringSource(curlReadBuffer, true, new CryptoPP::StreamTransformationFilter(aesDecryptor, new CryptoPP::StringSink(documentData), CryptoPP::StreamTransformationFilter::PKCS_PADDING));
            }
            catch(const CryptoPP::Exception& e)
            {
                documentRequestError = "AES decryption error: ";
                documentRequestError += e.what();

#if defined(MAKING_BLUETIT)

                global_syslog(documentRequestError);

#endif

                return AES_DECRYPTION_ERROR;
            }
        }
    }

    return result;
}

std::string AirVPNTools::getRequestedDocument()
{
    return documentData;
}

std::string AirVPNTools::getDocumentRequestErrorDescription()
{
    return documentRequestError;
}

std::string AirVPNTools::mapToAirVPNParameters(const std::map<std::string, std::string> &map)
{
    std::string output;

    if(map.size() > 0)
    {
        for(auto const &parameter : map)
        {
            output += Base64::encode((BYTE *)parameter.first.c_str(), parameter.first.length());
            output += ":";
            output += Base64::encode((BYTE *)parameter.second.c_str(), parameter.second.length());
            output += "\n";
        }
    }

    return output;
}

size_t AirVPNTools::curlWriteCallback(void *data, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)data, size * nmemb);

    return size * nmemb;
}

std::string AirVPNTools::convertXmlEntities(std::string xml)
{
    std::vector<std::string> xmlEntity = { "&#10;", "&#13;", "&#38;", "&gt;", "&lt;", "&amp;", "&quot;", "&apos;", "\\'", "\\\"" };
    std::vector<std::string> character = { "\n", "\r", "&", ">", "<", "&", "\"", "'", "'", "\"" };

    for(int i = 0; i < xmlEntity.size(); i++)
        xml = std::regex_replace(xml, std::regex(xmlEntity[i]), character[i]);

    return xml;
}

int AirVPNTools::getLoad(long byteBandWidth, long maxMBitBandWidth)
{
    long bwCur = 2 * (byteBandWidth * 8) / ONE_DECIMAL_MEGA; // to Mbit/s
    int load = 0;

    if(maxMBitBandWidth > 0)
        load = (int)((bwCur * 100) / maxMBitBandWidth);
    else
        load = 0;

    return load;
}

std::string AirVPNTools::formatTransferRate(long long rate, bool decimal)
{
    char txt[64], doubleFormat[16];
    double r = (double)rate;

    if(decimal)
        strcpy(doubleFormat, "%.2f %s");
    else
        strcpy(doubleFormat, "%.0f %s");

    if(rate >= ONE_DECIMAL_GIGA)
        sprintf(txt, doubleFormat, r / ONE_DECIMAL_GIGA, "Gbit/s");
    else if(rate >= ONE_DECIMAL_MEGA)
        sprintf(txt, doubleFormat, r / ONE_DECIMAL_MEGA, "Mbit/s");
    else if(rate > ONE_DECIMAL_KILO)
        sprintf(txt, doubleFormat, r / ONE_DECIMAL_KILO, "Kbit/s");
    else
        sprintf(txt, "%d bit/s", (int)r);

    return std::string(txt);
}

std::string AirVPNTools::formatDataVolume(long long bytes, bool decimal)
{
    char txt[64], doubleFormat[16];
    double fBytes = (double)bytes;

    if(decimal)
        strcpy(doubleFormat, "%.2f %s");
    else
        strcpy(doubleFormat, "%.0f %s");

    if(bytes >= ONE_GIGABYTE)
        sprintf(txt, doubleFormat, fBytes / ONE_GIGABYTE, "GB");
    else if(bytes >= ONE_MEGABYTE)
        sprintf(txt, doubleFormat, fBytes / ONE_MEGABYTE, "MB");
    else if(bytes >= ONE_KILOBYTE)
        sprintf(txt, doubleFormat, fBytes / ONE_KILOBYTE, "KB");
    else
        sprintf(txt, "%d bytes", (int)bytes);

    return std::string(txt);
}

std::string AirVPNTools::formatTime(long s)
{
    int hours, minutes, seconds;
    char txt[64];

    hours = s / 3600;
    minutes = (s % 3600) / 60;
    seconds = s % 60;

    sprintf(txt, "%02d:%02d:%02d", hours, minutes, seconds);
    
    return std::string(txt);
}

std::vector<std::string> AirVPNTools::split(const std::string &s, const std::string &delimiter)
{
    std::vector<std::string> item;
    std::string value;
    size_t beg, pos = 0;

    item.clear();

    if(s.find_first_not_of(delimiter, pos) == std::string::npos)
        item.push_back(trim(s));
    else
    {
        while((beg = s.find_first_not_of(delimiter, pos)) != std::string::npos)
        {
            pos = s.find_first_of(delimiter, beg + 1);

            value = trim(s.substr(beg, pos - beg));

            if(value.empty() == false)
                item.push_back(value);
        }
    }

    return item;
}

std::string AirVPNTools::trim(std::string s)
{
    size_t first, last;
    std::string TRIM_CHARACTERS = " \n\r\t\f\v";

    first = s.find_first_not_of(TRIM_CHARACTERS);

    if(first == std::string::npos)
        return "";

    last = s.find_last_not_of(TRIM_CHARACTERS);

    s = s.substr(first, (last - first + 1));

    return s;
}

bool AirVPNTools::iequals(const std::string &str1, const std::string &str2)
{
    if(str1.size() != str2.size())
        return false;

    return std::equal(str1.begin(), str1.end(), str2.begin(), str2.end(), [](char c1, char c2)
    {
        return tolower(c1) == tolower(c2);
    });
}

std::string AirVPNTools::unquote(const std::string &s)
{
    return std::regex_replace(s, std::regex("^\"|\"$"), "");
}

std::string AirVPNTools::toLower(std::string s)
{
    std::for_each(s.begin(), s.end(), [](char &c)
    {
        c = ::tolower(c);
    });

    return s;
}

std::string AirVPNTools::toUpper(std::string s)
{
    std::for_each(s.begin(), s.end(), [](char &c)
    {
        c = ::toupper(c);
    });

    return s;
}

std::string AirVPNTools::pad(const std::string &str, const size_t size, const char padChar)
{
    std::string s = str;;

    for(int i = str.size(); i < size; i++)
        s += padChar;

    return s;
}

std::string AirVPNTools::padRight(const std::string &str, const size_t size, const char padChar)
{
    std::string s = str;;

    for(int i = str.size(); i < size; i++)
        s = padChar + s;

    return s;
}

std::string AirVPNTools::normalizeBoolValue(const std::string &value, const std::string &trueValue, const std::string &falseValue)
{
    if(value == "1" || value == "on" || value == "yes" || value == "true")
        return trueValue;
    else if(value == "0" || value == "off" || value == "no" || value == "false")
        return falseValue;
    else
        return value;
}

xmlNode *AirVPNTools::findXmlNode(const xmlChar *name, const xmlNode *startNode)
{
    xmlNode *node = nullptr;

    if(startNode == NULL)
        return NULL;

    node = (xmlNode *)startNode;

    while(node != NULL)
    {
        if(!xmlStrcmp(node->name, name))
        {
            return node;
        }
        else if(node->children != NULL)
        {
            xmlNode *searchNode = findXmlNode(name, node->children);

            if(searchNode != NULL)
                return searchNode;
        }

        node = node->next;
    }

    return NULL;
}

std::string AirVPNTools::getXmlAttribute(const xmlNode *node, const std::string &name, const std::string &defaultValue)
{
    std::string value;
    xmlChar *attribute = nullptr;

    attribute = xmlGetProp(node, (const xmlChar *)name.c_str());

    if(attribute != NULL)
        value = (char *)attribute;
    else
        value = defaultValue;

    xmlFree(attribute);

    return value;
}

std::map<std::string, std::string> AirVPNTools::detectLocation()
{
    CURL *curl;
    CURLcode curlResult;
    xmlDoc *locationDocument = nullptr;;
    xmlNode *rootNode = nullptr, *node = nullptr;
    std::map<std::string, std::string> result;
    std::string status, curlBuffer;

    result.clear();

    curlBuffer = "";

    curl = curl_easy_init();

    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, "https://ipleak.net/xml/");

        curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, SERVER_CONNECTION_TIMEOUT);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, SERVER_READ_TIMEOUT);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &curlBuffer);

        curlResult = curl_easy_perform(curl);

        switch(curlResult)
        {
            case CURLE_OK:
            {
                status = LOCATION_STATUS_OK;
            }
            break;

            case CURLE_COULDNT_RESOLVE_HOST:
            {
                status = "Cannot resolve ipleak.net";
            }
            break;

            case CURLE_COULDNT_CONNECT:
            {
                status = "Cannot connect to ipleak.net";
            }
            break;

            case CURLE_HTTP_RETURNED_ERROR:
            {
                status = "HTTP error: ";
                status += curl_easy_strerror(curlResult);
            }
            break;

            case CURLE_GOT_NOTHING:
            {
                status = "ipleak.net returned an empty document: ";
                status += curl_easy_strerror(curlResult);
            }
            break;

            case CURLE_SEND_ERROR:
            {
                status = "Send error: ";
                status += curl_easy_strerror(curlResult);
            }
            break;

            case CURLE_RECV_ERROR:
            {
                status = "Receive error: ";
                status += curl_easy_strerror(curlResult);
            }
            break;

            case CURLE_OPERATION_TIMEDOUT:
            {
                status = "Connection timeout: ";
                status += curl_easy_strerror(curlResult);
            }
            break;

            default:
            {
                status = "Unknown error: ";
                status += curl_easy_strerror(curlResult);
            }
        }

        curl_easy_cleanup(curl);

        if(curlResult == CURLE_OK)
        {
            locationDocument = xmlReadMemory(curlBuffer.c_str(), curlBuffer.size(), "noname.xml", NULL, 0);

            if(locationDocument != nullptr)
            {
                result.insert(std::make_pair("status", status));

                rootNode = xmlDocGetRootElement(locationDocument);

                node = AirVPNTools::findXmlNode((xmlChar *)"ip", rootNode);

                if(node != NULL)
                    result.insert(std::make_pair("ip", (char *)xmlNodeGetContent(node)));
                else
                    result.insert(std::make_pair("ip", ""));

                node = AirVPNTools::findXmlNode((xmlChar *)"country_code", rootNode);

                if(node != NULL)
                    result.insert(std::make_pair("country", (char *)xmlNodeGetContent(node)));
                else
                    result.insert(std::make_pair("country", ""));

                node = AirVPNTools::findXmlNode((xmlChar *)"latitude", rootNode);

                if(node != NULL)
                    result.insert(std::make_pair("latitude", (char *)xmlNodeGetContent(node)));
                else
                    result.insert(std::make_pair("latitude", ""));

                node = AirVPNTools::findXmlNode((xmlChar *)"longitude", rootNode);

                if(node != NULL)
                    result.insert(std::make_pair("longitude", (char *)xmlNodeGetContent(node)));
                else
                    result.insert(std::make_pair("longitude", ""));
            }
        }
        else
        {
            result.insert(std::make_pair("status", status));
            result.insert(std::make_pair("ip", ""));
            result.insert(std::make_pair("country", ""));
            result.insert(std::make_pair("latitude", "0.0"));
            result.insert(std::make_pair("longitude", "0.0"));
        }
    }
    else
    {
        result.insert(std::make_pair("status", "Cannot initialize curl"));
        result.insert(std::make_pair("ip", ""));
        result.insert(std::make_pair("country", ""));
        result.insert(std::make_pair("latitude", "0.0"));
        result.insert(std::make_pair("longitude", "0.0"));
    }

    return result;
}
