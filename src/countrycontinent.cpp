/*
 * countrycontinent.cpp
 *
 * This file is part of the AirVPN Suite for Linux and macOS.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the AirVPN Suite. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <algorithm>
#include "include/countrycontinent.hpp"
#include "include/airvpntools.hpp"

extern void global_log(const std::string &s);

std::map<std::string, std::string> *CountryContinent::countryContinent = nullptr;
std::map<std::string, std::string> *CountryContinent::countryNames = nullptr;
int CountryContinent::instanceCounter = 0;

CountryContinent::CountryContinent(const std::string &resourceDirectory)
{
    std::string line, fileName;
    std::vector<std::string> item;

    if(instanceCounter == 0)
    {
        if(countryContinent != nullptr)
            delete countryContinent;

        countryContinent = new std::map<std::string, std::string>;

        countryContinent->clear();

        fileName = resourceDirectory;
        fileName += "/";
        fileName += COUNTRY_CONTINENT_FILE_NAME;

        std::ifstream countryContinentFile(fileName);

        if(countryContinentFile.is_open())
        {
            while(std::getline(countryContinentFile, line))
            {
                item = AirVPNTools::split(line, SPLIT_DELIMITER);

                if(item.size() == 2)
                    countryContinent->insert(std::make_pair(AirVPNTools::toUpper(item[0]), AirVPNTools::toUpper(item[1])));
            }

            countryContinentFile.close();
        }
        else
            global_log("CountryContinent::CountryContinent() - Cannot open " + fileName);

        if(countryNames != nullptr)
            delete countryNames;

        countryNames = new std::map<std::string, std::string>;

        countryNames->clear();

        fileName = resourceDirectory;
        fileName += "/";
        fileName += COUNTRY_NAMES_FILE_NAME;

        std::ifstream countryNamesFile(fileName);

        if(countryNamesFile.is_open())
        {
            while(std::getline(countryNamesFile, line))
            {
                item = AirVPNTools::split(line, SPLIT_DELIMITER);

                if(item.size() == 2)
                    countryNames->insert(std::make_pair(AirVPNTools::toUpper(item[0]), item[1]));
            }

            countryNamesFile.close();
        }
        else
            global_log("CountryContinent::CountryContinent() - Cannot open " + fileName);
    }

    instanceCounter++;
}

CountryContinent::~CountryContinent()
{
    instanceCounter--;

    if(instanceCounter == 0)
    {
        if(countryContinent != nullptr)
        {
            delete countryContinent;

            countryContinent = nullptr;
        }

        if(countryNames != nullptr)
        {
            delete countryNames;

            countryNames = nullptr;
        }
    }
}

std::string CountryContinent::getCountryContinent(std::string countryCode)
{
    if(countryContinent->empty())
        return "";

    countryCode = AirVPNTools::toUpper(countryCode);

    std::string continent = "";
    std::map<std::string, std::string>::iterator it = countryContinent->find(countryCode);

    if(it != countryContinent->end())
        continent = it->second;

    return continent;
}

std::string CountryContinent::getCountryName(std::string countryCode)
{
    if(countryNames->empty())
        return "";

    std::string name = "";

    countryCode = AirVPNTools::toUpper(countryCode);

    std::map<std::string, std::string>::iterator it = countryNames->find(countryCode);

    if(it != countryNames->end())
        name = countryNames->find(countryCode)->second;

    return name;
}

std::string CountryContinent::getCountryCode(std::string countryName)
{
    if(countryNames->empty())
        return "";

    std::string code = "";
    bool found = false;

    countryName = AirVPNTools::toUpper(countryName);

    for(std::map<std::string, std::string>::iterator it = countryNames->begin(); it != countryNames->end() && found == false; it++)
    {
        if(AirVPNTools::toUpper(it->second) == countryName)
        {
            code = AirVPNTools::toUpper(it->first);

            found = true;
        }
    }

    return code;
}

std::vector<std::string> CountryContinent::searchCountry(std::string pattern)
{
    std::vector<std::string> result;

    if(countryNames->empty())
        return result;

    pattern = AirVPNTools::toUpper(pattern);

    for(std::map<std::string, std::string>::iterator it = countryNames->begin(); it != countryNames->end(); it++)
    {
        if(AirVPNTools::toUpper(it->first).find(pattern) != std::string::npos || AirVPNTools::toUpper(it->second).find(pattern) != std::string::npos || pattern == "ALL")
            result.push_back(it->first);
    }

    return result;
}

int CountryContinent::getCountries()
{
    return countryNames->size();
}

int CountryContinent::getContinents()
{
    return countryContinent->size();
}
